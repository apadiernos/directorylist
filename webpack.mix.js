const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/sass/app.scss', 'public/css');
mix.sass('resources/assets/scss/carbalen.scss', 'public/css')


mix.combine([
	'public/css/app.css',
	'public/css/carbalen.css',	
], 'public/css/all.css').version(); 
   
mix.combine([
    'resources/assets/js/jquery.js',
    'resources/assets/js/tether.min.js',
	'resources/assets/js/materialist.js',	
    'public/assets/js/libs/misc/jquery-block-ui/jqueryblockui.min.js',
    'public/assets/js/libs/bower/jquery-slimscroll/jquery.slimscroll.js',
    'resources/assets/js/bootstrap.min.js', 
	'resources/assets/js/library.js',
    'resources/assets/js/plugins.js',
    'resources/assets/js/main.js'
], 'public/js/all.js').version(); 