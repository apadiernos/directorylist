<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/logout', function () {
  Auth::logout();
  return redirect('/');
});


Route::get('/googleredirect', 'Auth\SocialAuthController@redirect_google');
Route::get('/googlecallback', 'Auth\SocialAuthController@callback_google');


Route::get('/facebookredirect', 'Auth\SocialAuthController@redirect_facebook');
Route::get('/facebookcallback', 'Auth\SocialAuthController@callback_facebook');

//home routes
Route::get('/', 'Portal\HomeController@showHomePage');
Route::post('homesearch', 'Portal\HomeController@homeSearch');
Route::post('listsearch', 'Portal\HomeController@listsearch');
Route::get('/verifyadvertiser/{encodeddata}', 'Portal\HomeController@verifyUserAdvertiser');

Route::get('/get_captcha/{config?}', function (\Mews\Captcha\Captcha $captcha, $config = 'default') {
    return $captcha->src($config);
});

//single routes
Route::post('commentssearch', 'Portal\PostSingleController@commentssearch');
Route::post('/listinquiry', 'Portal\PostSingleController@emaillist');
Route::post('/listreact', 'Portal\PostSingleController@saveListReaction');
Route::get('lists/{category}/{title}', 'Portal\PostSingleController@showListingSingle');
Route::get('lists/{category}/{title}/{post}', 'Portal\PostSingleController@showListingSingle');

Auth::routes();
Route::post('/updateprofile', 'Auth\ProfileController@saveRecords');

//profile routes
Route::get('/profile/{term}', 'Portal\ProfileController@showProfilePage');
Route::post('/getprofilecontent', 'Portal\ProfileController@showProfilePageContents');
Route::post('/registerasadvertiser', 'Portal\ProfileController@registerUserAdAdvertiser');

//ads routes
Route::get('/ads/{term}', 'Portal\ManageAdvertiserController@showManageAdvertiserPage');
Route::get('/ads/single/{id}', 'Portal\Ads\AdSingleController@showSingleAdPage');
Route::get('/ads/single/{id}/sections', 'Portal\Ads\AdSingleController@showSingleAdSectionsPage');

//admin auth
Route::get('admin/login', function () {
    return view('admin/auth/login');
});
Route::post('login/admin', 'Auth\LoginController@adminLogin');

//admin
Route::get('admin/dashboard', 'Admin\Dashboard@showAdminDashboard');
//listing category
Route::get('admin/listings/categories', 'Admin\ListingCategoryController@showListingCategory');
Route::get('admin/listings/categories/table', 'Admin\ListingCategoryController@getTableRecords');
Route::post('admin/listings/categories', 'Admin\ListingCategoryController@saveRecords');
Route::delete('listings/categories/{id}', 'Admin\ListingCategoryController@deleteRecords');

Route::get('listings/categories', 'Listings\ListingCategoryController@getRecords');
Route::get('listings/categories/{id}', 'Listings\ListingCategoryController@getRecord');
Route::post('listings/categories', 'Listings\ListingCategoryController@saveRecords');
Route::put('listings/categories/{id}', 'Listings\ListingCategoryController@gsaveRecord');

Route::get('admin/listings', 'Admin\ListingController@showListing');
Route::get('admin/listings/table', 'Admin\ListingController@getTableRecords');
Route::get('admin/listings/add', 'Admin\ListingController@showAddListing');
Route::get('admin/listings/{id}', 'Admin\ListingController@showEditListing');
Route::post('admin/listings', 'Admin\ListingController@saveRecords');
Route::delete('listings/{id}', 'Admin\ListingController@deleteRecords');

Route::get('listings', 'Listings\ListingController@getRecords');
Route::post('listings/ajax', 'Listings\ListingController@getRecordsByAjax');
Route::post('listings/getlocationajax', 'Listings\ListingController@getLocationAjax');
Route::get('listings/{id}', 'Listings\ListingController@getRecord');
Route::post('listings', 'Listings\ListingController@saveRecords');
Route::put('listings/{id}', 'Listings\ListingController@gsaveRecord');

Route::post('attachments', 'Listings\AttachmentController@saveRecords');
Route::post('attachments/get', 'Listings\AttachmentController@getRecords');
Route::post('attachments/delete', 'Listings\AttachmentController@deleteRecords');
Route::delete('attachments/delete/{id}', 'Listings\AttachmentController@deleteRecord');

Route::post('reviews', 'Listings\ReviewController@saveRecords');
Route::get('reviews/{id}', 'Listings\ReviewController@getRecord');
Route::get('reviews', 'Listings\ReviewController@getRecords');
Route::post('reviews/delete', 'Listings\ReviewController@deleteRecords');
Route::delete('reviews/delete/{id}', 'Listings\ReviewController@deleteRecord');

Route::post('reviewreactions', 'Listings\ReviewReactionController@saveRecords');
Route::post('reviewreactions/{id}', 'Listings\ReviewReactionController@getRecord');
Route::get('reviewreactions', 'Listings\ReviewReactionController@getRecords');
Route::post('reviewreactions/delete', 'Listings\ReviewReactionController@deleteRecords');
Route::delete('reviewreactions/delete/{id}', 'Listings\ReviewReactionController@deleteRecord');

Route::post('tags', 'Listings\TagController@saveRecords');
Route::post('tags/get', 'Listings\TagController@getRecords');
Route::post('tags/getbyterm', 'Listings\TagController@getRecordsByTerm');
Route::post('tags/delete', 'Listings\TagController@deleteRecords');
Route::delete('tags/delete/{id}', 'Listings\TagController@deleteRecord');

Route::post('carmakes/getbyterm', 'Listings\CarMakeController@getRecordsByTerm');

Route::get('listingcarmakes', 'Listings\ListingCarMakeController@getRecords');
Route::get('listingcarmakes/{id}', 'Listings\ListingCarMakeController@getRecord');
Route::post('listingcarmakes', 'Listings\ListingCarMakeController@saveRecords');
Route::put('listingcarmakes/{id}', 'Listings\ListingCarMakeController@gsaveRecord');

Route::get('listingposts', 'Listings\ListingPostController@getRecords');
Route::get('listingposts/{id}', 'Listings\ListingPostController@getRecord');
Route::post('listingposts', 'Listings\ListingPostController@saveRecords');
Route::put('listingposts/{id}', 'Listings\ListingPostController@gsaveRecord');

Route::get('listingsubposts', 'Listings\ListingSubPostController@getRecords');
Route::get('listingsubposts/{id}', 'Listings\ListingSubPostController@getRecord');
Route::post('listingsubposts', 'Listings\ListingSubPostController@saveRecords');
Route::put('listingsubposts/{id}', 'Listings\ListingSubPostController@gsaveRecord');

Route::post('userlistlikes', 'Listings\UserListLikeController@saveRecords');
Route::post('userlistdislikes', 'Listings\UserListLikeController@saveRecords');


Route::post('cities', 'Misc\MiscController@getCities');
Route::post('spellcheck', 'Misc\MiscController@checkSpelling');

Route::get('lists', 'Portal\HomeController@showListingLists');
Route::get('lists/{category}', 'Portal\HomeController@showListingListByCategory');