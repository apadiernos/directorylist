/*!
 @package Facebook-Reactions-JS - A jQuery Plugin to generate Facebook Style Reactions. 
 @version version: 1.0
 @developer github: https://github.com/99points/Facebook-Reactions-JS
 
 @developed by: Zeeshan Rasool. [Founder @ http://www.99points.info & http://wallscriptclone.com]
 
 @license Licensed under the MIT licenses: http://www.opensource.org/licenses/mit-license.php
*/

(function($) {

	$.fn.facebookReactions = function(options) {
		var instance = new Date().valueOf();
		$(this).addClass('FB_reactions FB_reactions'+instance).attr('data-instance',instance).append('<div class="bar_container"></div>');
			
		var settings = $.extend( {
            
			ajax: {url: false, data: {}}, // once user will select an emoji, lets save this selection via ajax to DB.
			defaultText: "like" // default text for button
				
		}, options);

		var bg = $('.FB_reactions').css('background-image');
		bg = bg.replace('url(','').replace(')','').replace(/\"/gi, "");
		bg = bg.substring(0, bg.lastIndexOf('/'))+'/';
			
		var last_emoji_value;
		var emoji_value;
		var _bar;
		var _inner;
		var _this = $(this);
		var _spn_cnt = options.reactions && options.reactions.total ? parseInt(options.reactions.total) : 0;
		if( _spn_cnt != 0 ){
			var all_react_html = '<div class="FB_all_reactions">';
			$.each(options.reactions.reacts,function(key,value){
				all_react_html += '<img src="'+bg+value+'.svg" class="all-emoji" />';
			});
			all_react_html += '<span>'+options.reactions.total+'</span> <br clear="all" /></div>';
			$(this).before(all_react_html);
		}
		
		// on click emotions
		$('body').on("click",".emoji",function (e) {
			if(e.target !== e.currentTarget) return;	
			var instance_emoji = $(this).attr('data-instance');

			var base = $(this).parents('.FB_reactions');
			var control_id = parseInt(base.attr("data-unique-id"));
			var _spn_cnt = 0;
			if( base.prev('.FB_all_reactions') && base.prev('.FB_all_reactions').find('span').html() ){
				var _spn_cnt = parseInt(base.prev('.FB_all_reactions').find('span').html());
			}else	
				var _spn_cnt = 0;

			var move_emoji = $(this);
			// on complete reaction
			emoji_value = move_emoji.data('emoji-value');
			
			if (move_emoji) {
				var cloneemoji = move_emoji.clone().offset({
					top: move_emoji.offset().top,
					left: move_emoji.offset().left
				}).css({
					'height': '40px',
					'width': '40px',
					'opacity': '0.9',
					'position': 'absolute',
					'z-index': '99'
				}).appendTo($('body')).animate({
						'top': base.offset().top+5,
						'left': base.offset().left + 10,
						'width': 30,
						'height': 30
				}, 300, 'easeInBack');
				
				cloneemoji.animate({
					'width': 27,
					'height': 27
				},100, function () {
					
					var _imgicon = $(this);
					
					_imgicon.fadeOut(100, function(){ _imgicon.detach(); 
						 // add icon class
						base.attr("data-emoji-class", emoji_value);
						// change text
						base.find('span').html(emoji_value);
						
						if ( settings.ajax.url && instance_emoji == instance ) {
								
							if( _spn_cnt == 0 ){
								base.before('<div class="FB_all_reactions"><img src="'+bg+emoji_value+'.svg" class="all-emoji" /><span>1</span></div>');
								_spn_cnt++;
							}else{
								if( !base.prev('.FB_all_reactions').find('img[src="'+bg+emoji_value+'.svg"]').length ){
									if( control_id == 0  ){
										base.prev('.FB_all_reactions').prepend('<img src="'+bg+emoji_value+'.svg" class="all-emoji" />');
										_spn_cnt++;
										base.prev('.FB_all_reactions').find('span').html('<span>'+_spn_cnt+'</span>');
									}else{
										base.prev('.FB_all_reactions').find('img[src="'+bg+emoji_value+'.svg"]').attr('src',bg+emoji_value+'.svg')
									}	
								}else{
									if( control_id != 0  )
										base.prev('.FB_all_reactions').find('img[src="'+bg+emoji_value+'.svg"]').attr('src',bg+emoji_value+'.svg')
								}
								
								
									
							}							
							__ajax(base, emoji_value);
						}
					});
				});
				last_emoji_value = emoji_value;
			}
			
			return false;
		});
		
		// ajax
		function __ajax(base, value){
			
			// here we have control id and value. We need to save them into db. You can change it according to yours requirements. 
			var control_id = base.attr("data-unique-id");
			var formData = {'control_id':control_id,'value':value};
			if( settings.ajax.data )
				$.extend(formData, settings.ajax.data);				
			
			$.ajax({
				type	:	'POST', // define the type of HTTP verb we want to use (POST for our form)
				url		:	settings.ajax.url, // the url where we want to POST
				data	:	formData, // our data object
				success	:	function(data){
					try{
						var dataRes = $.parseJSON(data);
						
						if( dataRes.control_id )
							base.attr('data-unique-id',dataRes.control_id);
						if( dataRes.encoded_data )
							settings.ajax.data.encoded_data = dataRes.encoded_data;				
					}catch(err){
	
					}
					
					// nothing requires here but you can add something here. 
					//console.log(data);
				},
				error: function(err) {
					 
					//console.log(err);
					console.log('An error has occurred');
				}
			});
		}
		
		return this.each(function() {
			
			var _this = $(this);
			window.tmr;
			window.selector = _this.get(0).className;
			var open_instance;
			var _react_html = '<div style="position:absolute; z-index: 1;" class="_bar" data-status="hidden"><div class="_inner">'; 
			
			var faces 	= '<img src="'+bg+'like.svg" class="emoji" data-emoji-value="like" style="" data-instance="'+instance+'"/>';
			
			faces = faces + '<img src="'+bg+'love.svg" class="emoji" data-emoji-value="love" style="" data-instance="'+instance+'"/>';
			
			faces = faces + '<img src="'+bg+'haha.svg" class="emoji" data-emoji-value="haha" style="" data-instance="'+instance+'"/>';
			
			faces = faces + '<img src="'+bg+'wow.svg" class="emoji" data-emoji-value="wow" style="" data-instance="'+instance+'"/>';
			
			faces = faces + '<img src="'+bg+'sad.svg" class="emoji" data-emoji-value="sad" style="" data-instance="'+instance+'"/>';
			
			faces = faces + '<img src="'+bg+'angry.svg" class="emoji" data-emoji-value="angry" style="" data-instance="'+instance+'"/>';
			  
			_react_html = _react_html + faces;
			
			_react_html = _react_html + '<br clear="all" /></div></div>';
			
			setTimeout(function(){
				_this.find('.bar_container').html(_react_html);
				_bar = _this.find('.bar_container').find('._bar');
				_inner = _this.find('.bar_container').find('._inner');
				if( _this.prev('.FB_all_reactions') )
					_spn_cnt = parseInt(_this.prev('.FB_all_reactions').find('span').html());
			},500)
		
			$('body').on('click','.FB_reactions span',function(e) {
				if(e.target !== e.currentTarget) return;	
				var isLiked = $(this).parent().attr("data-emoji-class");
				var control_id = $(this).parent().attr("data-unique-id");
				
				$(this).html(settings.defaultText);	
				
				if(!isLiked)
				{					
			
					$(this).parent().attr("data-emoji-class", "like");
					
					if ( settings.ajax.url )
						__ajax($(this).parents('.FB_reactions'), "like");
				}
			});
		var open_bar;
		$("body").on('mouseover','.FB_reactions',function () {
				var _bar = $(_this).find('._bar');
				if( open_instance && open_instance != $(this).attr('data-instance') ){
					__hide(open_instance); 
				}
				open_instance =  $(this).attr('data-instance');
				
				if ( $(this).hasClass("emoji") ){
					return false;
				}
				
				if($(this).hasClass("open") === true)
				{
					clearTimeout(window.tmr);
					return false;
				}
					
				$('.'+window.selector).each(function() {
						 
					__hide(open_instance);
				});
				
				if( _bar.attr('data-status') != 'active' ) {
					
					__show(this);
				}
			},

		);		
		$('body').on('mouseleave','._bar', function () {
			var _bar = $(this);
			window.tmr = setTimeout( function(){
				__hide(open_instance); 
			}, 200);		
		});	

			// functions
			function __hide(_this) {
				var _bar = $('.FB_reactions[data-instance="'+open_instance+'"] ._bar');
				var _inner = _bar.find('._inner');
				
				_bar.attr('data-status', 'hidden');
				
				_bar.hide();
				
				$('.open').removeClass("open");
				
				_inner.removeClass('ov_visi');
				
				clearTimeout(window.tmr);
			}
			
			function __show(_this) {
				
				var _bar = $(_this).find('._bar');
				var _inner = $(_this).find('._inner');
				clearTimeout(window.tmr);

				_bar.fadeIn();
				
				_bar.attr('data-status', 'active');

				_inner.addClass('ov_visi');
				
				$(_this).addClass("open");
				
				// vertical or horizontal
				var position = $(_this).data('reactions-type');
				
				if( position == 'horizontal' )
				{
					_inner.css('width', '241px');
					// Set main bar position top: -50px; left:0px;
					_bar.css({'top': '-50px', 'left': '0px', 'right': 'auto' });
				}
				else
				{
					_inner.css('width', '41px');
					_bar.css({'top': '-6px', 'right': '-48px', 'left': 'auto' });
				}					

			}
		});
	};

})(jQuery);

