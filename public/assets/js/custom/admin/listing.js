$(function(){
	var template = $('div.section-template').html();
	var row_template = $('div.section-template div.section-row-template').html();
	var idx = $('fieldset.sections').length ? parseInt($('fieldset.sections').length)-1 : -1;

	$('button.btn-add-section').click(function(){
		var dt = new Date().valueOf();
		$('div.sections').append('<fieldset id="'+dt+'" class="sections" style="background-color: rgba(0, 0, 0, 0.10); position: relative">'+template+'</fieldset>');	
		idx++;
		setTimeout(function(){
			var objTxt = $('fieldset#'+dt).find('button.btn-add-row').attr('obj');
			$('fieldset#'+dt).find('button.btn-add-row').attr('data-idx',idx);
			$('fieldset#'+dt).find('select,input').each(function(){
				var propTxt = $(this).attr('prop');
				$(this).attr('name',objTxt+'['+idx+']['+propTxt+'][]');
			});
		},600);			
		setTimeout(function(){
			$('div.sections fieldset#'+dt+' select').iniSelect2Fa();
		},300);
		return false;
	});
	
	$('body').on('click','button.btn-remove-section',function(){
		if( $('fieldset.sections').length > 1 )
			$(this).parents('fieldset').remove();
		return false;
	});	
	$('body').on('click','button.btn-add-row',function(){
		var dt = new Date().valueOf();
		var objTxt = $(this).attr('obj');
		var no_idx = $(this).attr('data-idx') ? $(this).attr('data-idx') : 0;
		
		$(this).parents('fieldset').append('<div id="'+dt+'" class="row section-row-template">'+row_template+'</div>');
		setTimeout(function(){
			$('div#'+dt).find('select,input').each(function(){
				var propTxt = $(this).attr('prop');
				$(this).attr('name',objTxt+'['+no_idx+']['+propTxt+'][]');
			});
			$('div#'+dt).find('button.btn-add-row').attr('data-idx',no_idx);
		},600);		
		setTimeout(function(){
			$('div#'+dt+' select').iniSelect2Fa();
		},300);		
		return false;
	});
	$('body').on('click','button.btn-remove-row',function(){
		if( $(this).parents('fieldset').find('div.row').length > 1 )
			$(this).parents('div.row').remove();
		return false;
	});
	setTimeout(function(){
		$('select.fa-init').iniSelect2Fa();
	},300);	
})
var faJson = JSON.parse($('div.admin-box').attr('data-fa'));
function iformat(icon) {
	return '<i class="' + icon.id + '"></i> ' + icon.text;
}
$.fn.iniSelect2Fa = function(){
	var obj = [];
	$.each(faJson, function (key,val) {
	  var res = key.replace("fas fa-", "");		
	  res = res.replace("fab fa-", "");		
	  res = res.replace("-", " ");		
	  obj.push({text:res,id:key});
	});
	var val = JSON.parse($(this).attr('data-options'));
	$(this).attr('data-plugin','select2')
				.plugins()
				.select2({
					data: obj,
					theme: "material",
					width: "100%",
					templateSelection: iformat,
					escapeMarkup : function (markup) { return markup },
					templateResult: iformat
				});
}