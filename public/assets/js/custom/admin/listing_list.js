$(function(){	
	$('body').on('click','a.btn-delete',function(){
		try{
			var data = $.parseJSON($(this).attr('data-options'));
			var url =  base_url+'/listings/'+data.id;
			var results = $.xResponse(url,{},'delete');	
			if(table['listings']){
				var options = table['listings'].options;
				var oTableUrl = options.ajax.url;
				var api = table['listings'].api;
				api.ajax.url(oTableUrl).load();	
			}			
		}catch(err){
			console.log(err);
			console.log('Error deleting listing category');
		}		
	});	
})	