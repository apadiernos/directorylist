$(function(){
	$('body').on('click','button.new-category',function(){
		$('#modal').modal('show');
	});
	$('button.save').click(function(){
		try{
			var url =  base_url+'/admin/listings/categories';
			var request = $('form#ajax-form').serialize();
			var results = $.xResponse(url,request);
			if( results ){
				if(table['category_listing']){
					var options = table['category_listing'].options;
					var oTableUrl = options.ajax.url;
					var api = table['category_listing'].api;
					api.ajax.url(oTableUrl).load();	
					$('#modal').modal('hide');	
				}
			}		
		}catch(err){
			console.log(err);
			console.log('Error saving listing category');
		}		
	});
	$('body').on('click','a.btn-update',function(){
		$('#modal').modal('show');
		try{
			var data = $.parseJSON($(this).attr('data-options'));
			$('#modal input.category').val(data.category);
			$('#modal select.icon option[value="'+data.icon+'"]').val(data.icon);
			$('#modal input.id').val(data.id);	
		}catch(err){
			console.log(err);
			console.log('Error updating listing category');
		}		
	});	
	$('body').on('click','a.btn-delete',function(){
		try{
			var data = $.parseJSON($(this).attr('data-options'));
			var url =  base_url+'/listings/categories/'+data.id;
			var results = $.xResponse(url,{},'delete');	
			if(table['category_listing']){
				var options = table['category_listing'].options;
				var oTableUrl = options.ajax.url;
				var api = table['category_listing'].api;
				api.ajax.url(oTableUrl).load();	
			}			
		}catch(err){
			console.log(err);
			console.log('Error deleting listing category');
		}		
	});	
	setTimeout(function(){
		$('select.fa-init').iniSelect2Fa();
	},300);		
})
function iformat(icon) {
    var originalOption = icon.element;
	if( $(originalOption).data('icon') )
		return '<i class="' + $(originalOption).data('icon') + '"></i> ' + icon.text;
	else
		return icon.text;
}
$.fn.iniSelect2Fa = function(){
	$(this).attr('data-plugin','select2')
				.plugins()
				.select2({
					theme: "material",
					width: "100%",
					templateSelection: iformat,
					escapeMarkup : function (markup) { return markup },
					templateResult: iformat
				});
}