setTimeout(function(){
	var marker_cluster = L.markerClusterGroup();	
	map.on("moveend", function () {
		var params = map.getCenter();
		try{
			var url =  map.customoptions.url;
			var distance = $('form#homemapform select#distance option:selected').val();
			var results = $.xResponse(url,{params:{'lat':params.lat,'long':params.lng,'distance':distance}});
			if( results ){
				var data = $.parseJSON(results);
				$.each(data,function(index,value){
					var icon = L.divIcon({
						html: value.icon,
						iconSize:     [36, 36],
						iconAnchor:   [36, 36],
						popupAnchor:  [-20, -42]
					});

					var marker = L.marker(value.center, {
						icon: icon
					}).addTo(map);		
					var bg = "background-image: url('" + value.image + "')";
					marker.bindPopup(
						'<div class="listing-window-image-wrapper">' +
							'<a href="'+value.url+'/'+'">' +
								'<div class="listing-window-image" style="'+bg+'"></div>' +
								'<div class="listing-window-content">' +
									'<div class="info">' +
										'<h2>' + value.title + '</h2>' +
										'<h3>' + value.category + '</h3>' +
									'</div>' +
								'</div>' +
							'</a>' +
						'</div>'
					);

					marker_cluster.addLayer(marker);					
				})
			}		
		}catch(err){
			console.log(err);
			console.log('Error getting lists on map');
		}		
	});
},2000)

$(function(){
	var circle;
	$('button.search-home').click(function(){
		if( !$('form#homemapform').valid() )
			return false;		
		try{
			var distance = $('form#homemapform select#distance option:selected').val();
			var url =  base_url+'/homesearch';
			var request = $('form#homemapform').serialize();
			var results = $.xResponse(url,request);
			if( results ){
				var data = $.parseJSON(results);
				if(circle)
					map.removeLayer(circle)				
				circle = L.circle([data.lat, data.long], {radius: distance*1000}).addTo(map);
				map.flyTo(new L.LatLng(data.lat, data.long));
				map.fitBounds(circle.getBounds());
				
			}		
		}catch(err){
			console.log(err);
			console.log('Error searching in map home');
		}	
		return false;
	});
	var page = 2;
	$('a.load-more').click(function(){
		var this_element = $('a.btn-list');
		if( $(this).hasClass('reset') ){
			page = 1;
		}	
		var request = $('form#homemapform').serialize()+'&page='+page;
		var distance = $('form#homemapform select#distance option:selected').val();
		
		$('div.listings div.card').html('').addClass('card-loader');
		try{
			var url =  base_url+'/listsearch';
			var results = $.xResponse(url,request);
			if( results ){
				setTimeout(function(){
					$('div#list-container').html(results);
					$('input[data-plugin="rating"]').plugins();	

					if( $('div#list-container h4.no-more').length ){
						page = 1;
						this_element.text('Click to Reload');
					}else{
						if(circle)
							map.removeLayer(circle)		
						var data = $.parseJSON($('div#list-container div.card:first').attr('data-location'));	
						circle = L.circle([data.lat, data.long], {radius: distance*1000}).addTo(map);
						map.flyTo(new L.LatLng(data.lat, data.long));
						map.fitBounds(circle.getBounds());							
						this_element.text('Load More');	
					}				
				},500);
				page++;
				
			}		
		}catch(err){
			console.log(err);
			console.log('Error searching in list pages');
		}
		return false;	
	});
})
