var reply_el;
var reply_box;
var rep_msg;
var span_rep_el;
$(function(){
	$('div.comments-container').scrollableBlock();		
	var no_more_comments = 0;	 
	var comments_ctr = 1;
	$('div.comments-container').bind('slimscroll', function(e, pos){
			if( pos == 'bottom' && !no_more_comments ){
				var recent_comments = parseInt($('span.recent-count').html());	
				comments_ctr++;
				var url = base_url+'/commentssearch';
				var data_encrypted = $(this).attr('data-params');
				$(this).waitingBlock('Getting Comments...');		
				setTimeout(function(){	
					try{
						var results = $.xResponse(url,{'data_params':data_encrypted,'offset':comments_ctr});
						if( results ){
							//console.log(results);
							$('ul#comments').append(results);
							setTimeout(function(){	
								$('span.recent-count').html(recent_comments + parseInt($('li.new-comments-'+comments_ctr).attr('data-count')));
								$('li.new-comments-'+comments_ctr).find('input[data-plugin="rating"]').plugins();
								$('li.new-comments-'+comments_ctr).find('a[data-plugin="facebookReactions"]').plugins();
								$('li.new-comments-'+comments_ctr).find("div.comment-box textarea").emojioneInit();							
							},500);
							no_more_comments = 0;
						}else{
							no_more_comments = 1;
							$('ul#comments').append('<p class="text-center fw-600">No more comments found!</p>');
						}	
					}catch(err){
						console.log(err);
						show_error_modal();	
						console.log('Error Getting Comments List');
					}				
					$('div.comments-container').unblock();	
				},1500);				
			}
	 });	
	var react_param;
	var is_dislike = 0;
	$('div.owl-report').click(function(e){
		e.preventDefault();
		react_param = $(this).attr('data-params');
		$('div#report-post-modal').modal('show');
		is_dislike = 1;
	});	
	$('div.owl-like').click(function(e){
		react_param = $(this).attr('data-params');
		e.preventDefault();
		is_dislike = 0;	
		react_list();
	});
	$('button.submit-report').click(function(e){
		e.preventDefault();	
		if( react_param ){
			react_list();
		}
	});	
	$('body').on('click','a.report-post',function(e){
		e.preventDefault();	
		var manage_post = $(this).parents('.comment-meta-date').find('a.manage-post');
		react_param = manage_post.find('input[type="hidden"]').val();
		$('div#report-post-modal').modal('show');		
		is_dislike = 1;	
	});		

	function react_list(){
		if( !react_param ){
			if( is_dislike )
				$('div.alert-danger-report').show();
			else	
				show_error_modal();
		}
		var url = base_url+'/listreact';
		var request = {'encoded_data':react_param,'is_ajax':1};	
		var msg = $('textarea[name="report_message"]').data("emojioneArea").getText();	
		if( msg && is_dislike ){
			$('div.alert-danger-report').hide();
			$('div.alert-success-report').hide();			
			request.message = msg;
		}
		try{
			var results = $.xResponse(url,request);
			if( results ){
				var data = $.parseJSON(results);
				if( !data.success ){
					if( is_dislike )
						$('div.alert-danger-report').show();
					else
						show_error_modal();					
				}else{
					if( is_dislike )
						$('div.alert-success-report').show();	
					else
						$(this).addClass('icon-hover');
				}
				
			}else{
				if( is_dislike )
					$('div.alert-danger-report').show();
				else	
					show_error_modal();
			}	
		}catch(err){
			if( is_dislike )
				$('div.alert-danger-report').show();
			else	
				show_error_modal();
		}
		is_dislike = 0;	
		react_param = 0;	
	}	
	$('button.submit-register').click(function(e){
		e.preventDefault();
		$('#register-modal').modal('show');
		$('.focus-modal').css('z-index',1);
		return false;
	});
	
	$('button.send-email').click(function(e){
		$('div.alert-danger-inquiry').hide();
		$('div.alert-success-inquiry').hide();
		e.preventDefault();
		if( !$('form#inquiry').valid() )
			return false;
		var url = base_url+'/listinquiry';
		var request = $('form#inquiry').serialize();		
		try{
			var results = $.xResponse(url,request);
			if( results ){
				var data = $.parseJSON(results);
				if( !data.success ){
					$('div.alert-danger-inquiry').show();
					var validator = $('form#inquiry').validate();
					var errors = {};
					$.each(data.errors,function(key,val){
						errors[key] = val[0];			
					});
					if( Object.keys(errors).length ){
						refresh_captcha();
						validator.showErrors(errors);
						return;	
					}					
				}else{
					$('div.alert-success-inquiry').show();
				}
				
			}else{
				$('div.alert-danger-inquiry').show();
			}	
		}catch(err){
			$('div.alert-danger-inquiry').show();
			show_error_modal();	

		}			
	});
	$('body').on('click','div.comment-meta-date a.manage-post',function(e){
		$("div.comment-meta-date div.dropdown-menu").removeClass("show");	
		$(this).parents('div.comment-meta-date').find("div.dropdown-menu").addClass("show");		
	});
	var to_edit_post;
	var to_edit_form;
	var to_delete_data;
	$('body').on('click','a.edit-post',function(event){
		var manage_post = $(this).parents('.comment-meta-date').find('a.manage-post');
		var parent_comment = manage_post.parents('div.comment');
		var encoded = manage_post.find('input[type="hidden"]').val();
		if( encoded ){
			var msg = parent_comment.find('.comment-message').html();
			msg = msg.replace(/<img.*?class="emojione".*?alt="(.*?)".*?>/g, '$1').replace(/<br\s*[\/]?>/gi, "\n");			
			to_edit_post = encoded;
			to_edit_form = 'form#edit-review';
			$('div#edit-post-modal').find('form#edit-review').find('textarea').data("emojioneArea").setText(msg);			
			$('div#edit-post-modal').modal('show');
		}		
		return false;
	});	
	$('body').on('click','a.delete-post',function(event){
		$('#delete-modal .modal-body div.error').hide();
		var manage_post = $(this).parents('.comment-meta-date').find('a.manage-post');
		var parent_comment = manage_post.parents('div.comment');
		var encoded = manage_post.find('input[type="hidden"]').val();
		if( encoded ){
			to_delete_data = encoded;
			$('#delete-modal .modal-body span').html('Post');
			$('#delete-modal').modal('show');
		}		
		return false;
	});
	$(document).mouseup(function(e) 
	{
		var container = $("div.comment-meta-date div.dropdown-menu.show");
		if (!container.is(e.target) && container.has(e.target).length === 0) 
		{
			container.removeClass("show");
		}
		var container = $("div.comment-body div.dropdown-menu.show");
		if (!container.is(e.target) && container.has(e.target).length === 0) 
		{
			container.removeClass("show");
		}	
		var container = $('.focus-modal');
		if (!container.is(e.target) && container.has(e.target).length === 0) 
		{
			container.css('z-index',0);
			$(".modal-backdrop-custom").hide();
			$('div.popover-validate-error').remove();			
		}
		var container = $("div#feedback");
		if(!container.is(e.target) && container.has(e.target).length === 0){
			if( !container.hasClass('trim') )
				container.addClass('trim');
		}	
	})	
	$('body').on('click','a.manage-reply',function(event){
		$("div.comment-body div.dropdown-menu").removeClass("show");	
		var tooltipX = parseInt($(this).parents('span.comment-text').css('width')) - 8;
		$(this).parents('div.comment-body').find("div.dropdown-menu").css({'left':tooltipX+'px',right:'auto'}).addClass("show");		
	});
	$('body').on('click','a.edit-reply',function(event){
		var manage_rep = $(this).parents('div.comment-body').find('a.manage-reply')
		manage_rep.addClass('hide');
		var encoded = manage_rep.find('input[type="hidden"]').val();
		if( encoded ){
			span_rep_el = $(this).parents('div.comment-body').find('span.comment-text span.comment-message');
			rep_msg = span_rep_el.html();
			var msg = rep_msg.replace(/<img.*?class="emojione".*?alt="(.*?)".*?>/g, '$1').replace(/<br\s*[\/]?>/gi, "\n");
			var comment_parent_el = $(this).parents('li.parent-comment');	
			var cb = comment_parent_el.find('div.comment-box').html();
			span_rep_el.html(cb+'<small class="show" style="margin-top:-9px">Press esc to <a class="cancel-edit-reply" href="#">cancel</a></small>');
			setTimeout(function(){
				span_rep_el.find('.emojionearea').remove(); 
				span_rep_el.find('input[type="hidden"]').val(encoded);
				var emoji_el = span_rep_el.find('textarea');
				emoji_el.emojioneInit();
				emoji_el[0].emojioneArea.setText(msg);
			},300);	
			setTimeout(function(){
				span_rep_el.find('small').html('<a class="cancel-edit-reply" href="#">Cancel</a></small>'); 
			},3500);
		}		
		return false;
	});	
	$('body').on('click','a.delete-reply',function(event){
		$('#delete-modal .modal-body div.error').hide();
		var manage_rep = $(this).parents('div.comment-body').find('a.manage-reply')
		var encoded = manage_rep.find('input[type="hidden"]').val();
		if( encoded ){
			to_delete_data = encoded;
			$('#delete-modal .modal-body span').html('Comment');
			$('#delete-modal').modal('show');
		}		
		return false;
	});
	$('button.verify-delete-comment').click(function(event){
		if( to_delete_data ){
			try{
				var url = base_url+'/reviews/delete';
				var results = $.xResponse(url,{'encoded_data':to_delete_data});
				var data = $.parseJSON(results);
				if( data.id_encr ){
					$('ul#comments').find('li[data-id="'+data.id_encr+'"]').remove();
					$('#delete-modal').modal('hide');
				}else{
					if( !$('#delete-modal .modal-body .error').length )
						$('#delete-modal .modal-body').append('<div class="error">Error Deleting. Please reload page.</div>');
					else
						$('#delete-modal .modal-body .error').show();
				}
				
			}catch(err){
				if( !$('#delete-modal .modal-body .error').length )
					$('#delete-modal .modal-body').append('<div class="error">Error Deleting. Please reload page.</div>');
				else
					$('#delete-modal .modal-body .error').show();
			}			
		}
		return false;
	});
	
	$('body').on('click','a.cancel-edit-reply',function(event){
		if( span_rep_el ){
			span_rep_el.html(rep_msg);
			span_rep_el.next('a.manage-reply').removeClass('hide');
		}	
		return false;
	});	
	$('div#feedback').click(function() {
		$(this).removeClass('trim');
	});	
	$('div.focus-modal').click(function() {
		$(".modal-backdrop-custom").show();
		$('.focus-modal').css('z-index',0);
		$(this).css('z-index',1049);
	});

	setTimeout(function(){
		try {
			$("div.comment-box textarea").emojioneInit();
		} catch(err) {
			setTimeout(function(){
				$("div.comment-box textarea").emojioneInit();
			},2000);
		}
	 },1000); 
	  
	$('button.verify-comment').click(function(e){
		e.preventDefault();
		if( !$('form#review-reply').valid() )
			return false;
		var request = $('form#review-reply').serialize()+'&message='+reply_el.data("emojioneArea").getText()+'&encoded_data='+reply_el.prev('input[type="hidden"]').val();	
		var data =  $.post_reply($('form#review-reply'),request);
		if( data.id_encr ){
			$('div.comments-container').attr('data-verified',1); 
			$('div#comment-modal').modal('hide');
			if( reply_box ){
				reply_box.hide();
				$('div.popover-validate-error').remove();
			}	
		}	
	})
	$('body').on('click','a.comment-reply',function(){
		var comment_footer_el = $(this).parents('div.comment-footer');
		var comment_parent_el = $(this).parents('li.parent-comment');
		if( !comment_footer_el.find('.comment-box').length	){
			var cb = comment_parent_el.find('div.comment-box').html();
			comment_footer_el.append('<div class="comment-box">'+cb+'</div>');
		}else{
			comment_footer_el.find('.comment-box').show();
			return;
		}
		setTimeout(function(){
			comment_footer_el.find('.emojionearea').remove(); 
			comment_footer_el.find('textarea').val(comment_parent_el.find('b.reply-name').html()+' ').emojioneInit();
			reply_box = comment_footer_el.find('.comment-box');
		},300);
		return false;
	});
	
	$('button.submit-review').click(function(e){
		var form = to_edit_form ? to_edit_form : 'form#review';
		e.preventDefault();
		if( !$(form).valid() )
			return false;
		var val = $(form+' textarea').data("emojioneArea").getText();
		if( !val ){
			$(form+' .emojionearea').attr('data-toggle','popover').attr('data-placement','bottom').attr('data-content','This field is required.').popover({
						placement : 'bottom',
						template: '<div class="popover popover-validate-error bg-danger b-0"><div class="popover-arrow"></div><div class="popover-inner"><div class="popover-content p-xs"><p></p></div></div></div>'
				}).popover('show');
			return false;	
		}else{
			$(form+' .emojionearea').popover('dispose');	
		}
		var request = $(form).serialize()+'&message='+val;	
		if( to_edit_post )
			request += '&encoded_data='+to_edit_post;
		var data = $.post_reply($(form),request);
		if( data == 'error' )
			show_error_modal();	
		$('div#edit-post-modal').modal('hide');
		return false;
	});
	$('body').on('click','a.review-replay',function(){
		$('html, body').animate({
			scrollTop: $("div#feedback").offset().top - 10
		}, 1000);
		$(".modal-backdrop-custom").show();
		$('.focus-modal').css('z-index',1);
		$('div#feedback').css('z-index',1049);				
		 return false;
	});
	$('form#review').submit(function(){
		return false;
	});	
})

$.fn.emojioneInit = function(){
	$(this).emojioneArea({
		  search: false,
		  tonesStyle: 'checkbox',
		  pickerPosition: 'bottom',
		  filtersPosition: 'bottom',
		  events: {
			keypress: function (editor, event) {
				reply_el = $(this.source[0]);
				var msg = this.getText();
				if( event.ctrlKey && event.which === 13 ){
					return false;
				}		
				if( event.shiftKey && event.which === 13 ){
					return false;
				}		
				if( event.shiftKey && event.ctrlKey && event.which === 13 ){
					return false;
				}				
				if (event.which == 27) {
					$('a.cancel-edit-reply').click();
					return false;
				}	
				if(event.which === 13) {
					event.preventDefault();
					if( !msg ){
						if( !reply_el.parents('div.comment-box').find('small.error-comment-reply').length )
							reply_el.parents('div.comment-box').append('<small class="show error-comment-reply" style="margin-top:-9px">Please put a comment</small>');
						return false;
					}else if( reply_el.parents('div.comment-box').find('small.error-comment-reply') )
						reply_el.parents('div.comment-box').find('small.error-comment-reply').remove();	
					if( $('div.comments-container').attr('data-verified') != 1 ){			
						$('div#comment-modal').modal('show');
					}else{
						var params = 'message='+msg+'&encoded_data='+reply_el.prev('input[type="hidden"]').val()+'&verified=1';	
						if( $('div.comments-container').attr('data-auth') )
							var request = params;	
						else
							var request = $('form#review-reply').serialize()+'&'+params;	
						var data =  $.post_reply($('form#review-reply'),request);
						try{
							if( data.id_encr ){
								reply_el.val('');
								$('div.comments-container').attr('data-verified',1); 				
								if( reply_box ){
									reply_box.hide();
									$('div.popover-validate-error').remove();
								}
								this.setText('');	
							}	
						}catch(err){
							show_error_modal();	
						}
					}
					return false;
				}
			},
		  }
	});
};	


$.extend({
	post_reply: function(form,request)
	{
		var url =  base_url+'/reviews';
		var results = $.xResponse(url,request,'POST');
		var theResponse = null;
		try{
			var data = $.parseJSON(results);
			if( data ){
				if( data.errors ){			
					var validator = form.validate();
					var errors = {};
					$.each(data.errors,function(key,val){
						errors[key] = val[0];			
					});
					if( Object.keys(errors).length ){
						refresh_captcha();
						validator.showErrors(errors);
						return;	
					}
					return;	
				}
				refresh_captcha();
				theResponse = data;
				put_comment(data);
			}	
		}catch(err){
			return 'error';		
		}
		return theResponse;		
	}
});	
function put_comment(data)
{
	var dt = new Date().valueOf();
	if( data.parent_id ){
		var comment_template = $('div.comment-template-reply').html();
		
	}else
		var comment_template = $('div.comment-template').html();
	if( data.parent_id ){
		if( data.edited ){
			var li_edited = $('ul#comments').find('li[data-id="'+data.edited+'"]');
			if( li_edited.length > 0 ){
				span_rep_el = li_edited.find('span.comment-message');
				span_rep_el.html(data.message);
				span_rep_el.next('a.manage-reply').removeClass('hide');
				return;
			}else{
				$('a.cancel-edit-reply').click();
			}	
		}else{
			if( $('ul#comments').find('li[data-id="'+data.parent_id_encr+'"] ul li').length > 0 )
				$('ul#comments').find('li[data-id="'+data.parent_id_encr+'"] ul').prepend('<li class="comments-reply" data-id="'+data.id_encr+'" id="'+dt+'">'+comment_template+'</li>');
			else
				$('ul#comments').find('li[data-id="'+data.parent_id_encr+'"]').append('<ul><li class="comments-reply" data-id="'+data.id_encr+'" id="'+dt+'">'+comment_template+'</li></ul>');
		}
	}else{
		if( data.edited && $('ul#comments').find('li[data-id="'+data.id_encr+'"]').length > 0 )
			$('ul#comments').find('li[data-id="'+data.id_encr+'"]').remove();
		
		if( $('ul#comments').length )
			$('ul#comments').prepend('<li class="parent-comment" id="'+dt+'" data-id="'+data.id_encr+'">'+comment_template+'</li>');
		else{
			$('div.comments-container').html('<div class="scrollInner"><ul id="comments" class="comments"><li class="parent-comment" data-id="'+data.id_encr+'" id="'+dt+'">'+comment_template+'</li></ul></div>');
		}	
			
		var parentLi = $('ul#comments').find('li#'+dt);
		setTimeout(function(){
			parentLi.find('a.review-replay').attr('data-id',data.id_encr);
		},300);	
	}		
	setTimeout(function(){
		var parentLi = $('ul#comments').find('li#'+dt);
		if( data.name )parentLi.find('.comment-name').html(data.name);
		if( data.timecreated )parentLi.find('.comment-timecreated').html(data.timecreated);
		if( data.rating ){
			parentLi.find('div.comment-rating input').attr('data-plugin','rating').val(data.rating).plugins();
		}
		if( data.message )
			parentLi.find('.comment-message').html(data.message);
		if( data.edit_reply )
			parentLi.find('.comment-text').append(data.edit_reply);
		if( data.encoded_data && parentLi.find('div.comment-box input[type="hidden"]').length )
			parentLi.find('div.comment-box input[type="hidden"]').val(data.encoded_data);
		if( data.encoded_post_data && parentLi.find('a.manage-reply input[type="hidden"]').length )
			parentLi.find('a.manage-reply input[type="hidden"]').val(data.encoded_post_data);
		
		if( data.pp )
			parentLi.find('div.comment-author a').css('background-image','url('+data.pp+')');
		
		parentLi.find("div.comment-box").append('<textarea name="message" placeholder="Write a Comment..." rows="5"  class="m-h-xs"></textarea>')
		parentLi.find("div.comment-box textarea").emojioneInit();	
		
		if( !data.parent_id ){
			$('html, body').animate({
				scrollTop: parentLi.offset().top - 10
			}, 300);
		}		
		setTimeout(function(){
			$(".modal-backdrop-custom").hide();
		},600);	
	},300);	
}