var profile_data_encrypted;
$(document).ready(function() {
	profile_data_encrypted = $('li.nav-item-hovered a.nav-link-ajax').attr('param-data');
	$('div.profile-nav ul li a.nav-link-ajax').click(function(){
		$('div.profile-nav ul li').removeClass('nav-item-hovered');
		$(this).parents('li').attr('class','nav-item p-l-xs nav-item-hovered');
		$('div.profile-container-main').waitingBlock('Getting Records...');
		profile_data_encrypted = $(this).attr('param-data');		
		load_profile_content({'encoded_data':profile_data_encrypted});		
	});

	$('body').on('click','form#profile-info button',function(e){
		e.preventDefault();
		$('div.alert').hide();
		var url =  '/updateprofile';
		var form = $('form#profile-info');
		
		var request = new FormData();
		var file_data = $('input[type="file"]')[0].files[0]; // for multiple files
		var other_data = form.serializeArray();
		$.each(other_data,function(key,input){
			request.append(input.name,input.value);
		});
 		request.append('file',file_data);
		request.append('is_ajax', 1);
		
		if( !form.valid() )
			return false;
		try{
			$.ajax({
				url: url,
				data: request,
				contentType: false,
				processData: false,
				type: 'POST',
				beforeSend: function (xhr, settings) {
					xhr.setRequestHeader("X-CSRF-Token", csrf);
				},					
				success: function ( results ) {
					console.log(results);
					if( results ){
						var data = $.parseJSON(results);
						if( !data.success ){
							$('div.alert-danger-inquiry').show();
							var validator = form.validate();
							var errors = {};
							$.each(data.errors,function(key,val){
								errors[key] = val[0];			
							});
							if( Object.keys(errors).length ){
								refresh_captcha();
								validator.showErrors(errors);
								return;	
							}					
						}else{
							$('div.alert-success-inquiry').show();
						}
						
					}else{
						$('div.alert-danger-inquiry').show();
					}	
				}			
			});			
		}catch(err){
			console.log(err);
			$('div.alert-danger-inquiry').show();

		}
	});	
	setTimeout(function(){
		if( typeof map !== 'undefined' && !$('input[name="long"]').val() && !$('input[name="lat"]').val() ) getLocation();	
	},2000)	
	$('div#profilepic-select').click(function(){
		 $('input.filer').click();
	});
	$('input.filer').change(function(e){
		var tmppath = URL.createObjectURL(e.target.files[0]);
		$('div.profile-img').css('background','url("'+tmppath+'")');
	});	
})


var x = document.getElementById("autolocate");
function load_profile_content(request)
{
	var url = base_url+'/getprofilecontent';
	var results = $.xResponse(url,request);	
	try{
		if( results ){
			setTimeout(function(){
				$('div.profile-container-ajax').html(results);
				$('div.profile-container-ajax').find('[data-plugin]').plugins();	
				$('div.profile-container-main').unblock();

				if( typeof map !== 'undefined' && !$('input[name="long"]').val() && !$('input[name="lat"]').val() ){
					x = document.getElementById("autolocate");
					getLocation();
				}		
				
			},1500);	
			return results;
		}
		else
			show_error_modal();	
	}catch(err){
		show_error_modal();	
		//console.log(err);
		console.log('Error Getting Comments List');
	}
}	


function getLocation() {
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(
			// Success function
			showPosition, 
			// Error function
			showError, 
			// Options. See MDN for details.
			{
			   enableHighAccuracy: true,
			   timeout: 5000,
			   maximumAge: 0
			});
	}
}

function showError(error) {
	switch(error.code) {
		case error.PERMISSION_DENIED:
			x.innerHTML = "User denied the request for Geolocation."
			break;
		case error.POSITION_UNAVAILABLE:
			x.innerHTML = "Location information is unavailable."
			break;
		case error.TIMEOUT:
			x.innerHTML = "The request to get user location timed out."
			break;
		case error.UNKNOWN_ERROR:
			x.innerHTML = "An unknown error occurred."
			break;
	}
}

function showPosition(position) {
	$('input[name="long"]').val(position.coords.longitude);
	$('input[name="lat"]').val(position.coords.latitude);
	$('input[name="lat"]').blur();
}


