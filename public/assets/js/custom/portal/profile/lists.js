$(function(){	
	var page = 2;
	$('body').on('click','a.load-more',function(e){
		if( $('h4.no-more').length )
			page = 1;
		e.preventDefault();
		if( !profile_data_encrypted )
			show_error_modal();	
		var this_element = $('a.btn-list');
		$('div#list-container div.card').html('').addClass('card-loader');
		var results = load_profile_content({'encoded_data':profile_data_encrypted,'page':page});
		setTimeout(function(){
			$('input[data-plugin="rating"]').plugins();	
			page++;			
		},500);
		
		return false;	
	});
	$('body').on('click','a.join-as-advert',function(e){
		e.preventDefault();
		var url = base_url+'/registerasadvertiser';
		var results = $.xResponse(url,{});	
		try{
			if( results ){
				$('#advertiser-modal').modal('show');
			}
			else
				show_error_modal();	
		}catch(err){
			show_error_modal();	
			//console.log(err);
			console.log('Error Getting Comments List');
		}		
		return false;	
	});
})