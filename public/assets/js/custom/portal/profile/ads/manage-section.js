$(function(){
	var subpost_module_id;
	var encoded_data;
	var subpost;
	var subpost_el;
	var subpost_data;
	var subpost_title;
	var dz_options;
	var isdrop = 0;
	var dz;
	$('div.subposts').click(function(e){
		$('div.alert-post').hide();
		e.preventDefault();
		subpost_el = $(this);
		subpost_title = subpost_el.find('span.title');
		subpost = subpost_el.find('input[name="encoded_data_subpost"]');
		subpost_data = subpost_el.find('input[name="data"]');
		dz_options = subpost_el.find('input[name="dz_options"]');
		var modal_content = $('div#ad-post-modal').find('div.modal-content');
		var data_options = subpost_el.find('input[name="dz_options"]').val();
		try{
			var subpostdata = $.parseJSON(subpost_data.val());
			modal_content.find('input[name="title"]').val(subpostdata.title);
			if( subpostdata.description )
				modal_content.find('textarea[name="description"]').data("emojioneArea").setText(subpostdata.description);
		}catch(err){
			modal_content.find('input[name="title"]').val('');
			modal_content.find('textarea[name="description"]').html('');
		}		
		encoded_data = subpost.val();
		try{		
			var opt = $.parseJSON(data_options);
			subpost_module_id = opt.module_id+'-'+opt.module;
			$('div#ad-post-modal').modal('show');
			modal_content.waitingBlock('Getting Record...');
			modal_content.find('div.dropzone').attr('data-options',data_options).attr('data-plugin','dropzone').plugins();

			isdrop = 1;
			setTimeout(function(){
				modal_content.unblock();
				if( myDropzone[subpost_module_id] )
					dz = myDropzone[subpost_module_id];
			},1000);
		}catch(error){
			console.log(error);
			show_error_modal();
		}	
		return false;
	});
	$('button.ad-post-modal-close').click(function (e) {
		$('div#ad-post-modal').modal('hide');
		subposts_dz_destroy();
	});
	$('div#ad-post-modal').on('hidden.bs.modal',function (e) {
		var modal_content = $('div#ad-post-modal').find('div.modal-content');
		modal_content.find('div.dropzone')
		subposts_dz_destroy();
	});	
	$('button.ad-post-modal-save').click(function(e){
		e.preventDefault();
		var modal_content = $('div#ad-post-modal').find('div.modal-content');
		var form = modal_content.find('form');
		$('div.alert-post').hide();
		if( !form.valid() )
			return false;
		if( !encoded_data ){
			$('div.alert-danger-post').show();
			return false;
		}

		modal_content.waitingBlock('Saving...');
		var request = form.serialize()+'&encoded_data='+encoded_data;
		var url = base_url+'/listingsubposts';		
		try{
			var results = $.xResponse(url,request);
			if( results ){
				var data = $.parseJSON(results);
				if( !data.success ){
					setTimeout(function(){
						modal_content.unblock();
						$('div.alert-danger-post').show();
					},500);					
					var validator = form.validate();
					var errors = {};
					$.each(data.errors,function(key,val){
						errors[key] = val[0];			
					});
					if( Object.keys(errors).length ){
						refresh_captcha();
						validator.showErrors(errors);
						return;	
					}					
				}else{
					$('div.alert-success-post').show();
					encoded_data = data.data_encoded;
					subpost.val(encoded_data);
					subpost_data.val(data.record);
					if( data.dz_options ){
						dz_options.val(data.dz_options);
						subposts_dz_destroy();
						setTimeout(function(){	
							modal_content.find('div.dropzone').attr('data-options',data.dz_options).attr('data-plugin','dropzone').plugins();
							var opt = $.parseJSON(data.dz_options);
							subpost_module_id = opt.module_id+'-'+opt.module;
						},500);	
						setTimeout(function(){	
							if( myDropzone[subpost_module_id] )
								dz = myDropzone[subpost_module_id];						
						},1000);						
					}
					if( data.image ){
						subpost_el.css('background-image','url('+data.image+')');
					}
					subpost_title.html(data.record.title);
					setTimeout(function(){	
						modal_content.unblock();
					},1000);
				}
			}else{
				$('div.alert-danger-post').show();
				modal_content.unblock();
			}	
		}catch(err){
			$('div.alert-danger-post').show();
			modal_content.unblock();
		}			
		return false;

	});
	function subposts_dz_destroy(){
		if( myDropzone && myDropzone[subpost_module_id]){
			$('div#ad-post-modal').find('div.dropzone').find('.dz-preview').attr('destroy-called',1);
		  	myDropzone[subpost_module_id].destroy();
		  	delete myDropzone[subpost_module_id];
		  	$('div#ad-post-modal').find('div.dropzone').find('.dz-preview').remove();		  	
		  	$('div#ad-post-modal').find('div.dropzone').attr('data-plugin','').removeClass('dz-started');
		  	isdrop = 0;
	  	}		
	}
	$('div.sections-container').on('changed.owl.carousel', function(e) {
		$('.owl-item').find('input.title').attr('name','').attr('class','form-control title').popover('dispose');
		$('.owl-item').find('input.encoded_data').attr('name','').attr('class','encoded_data');
		setTimeout(function(){
			$('.owl-item.active').find('input.title').attr('name','title').attr('class','form-control title required');	
			$('.owl-item.active').find('input.encoded_data').attr('name','encoded_data').attr('class','encoded_data');	
		},500);	
  	});
})	


function callback_success(){
	$('div#ad-post-modal div.modal-footer button').show();
}	
function callback_processing(){
	$('div#ad-post-modal div.modal-footer button').hide();
}