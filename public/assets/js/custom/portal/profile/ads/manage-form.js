$(function(){
	setTimeout(function(){
		$('select.tags').select2({
		  tags: true,	
		  theme: 'material',	
		  minimumInputLength: 3,
		  escapeMarkup: function (markup) { return markup; }, 
		  ajax: {
			url: base_url+'/tags/getbyterm',
			method: 'post',
			data: function (params) {
			  var query = {
				_token: csrf,
				tag: params.term,
				page: params.page || 1,
				encoded_data: $('input[name="encoded_data"]').val()
			  }

			  // Query parameters will be ?search=[term]&type=public
			  return query;
			},
			processResults: function (dataRes, page) {
				try{
					var data = $.parseJSON(dataRes);	
					var more = (page * 10) < data.count; // whether or not there are more results available
					
					// notice we return the value of more so Select2 knows if more results can be loaded
					return { results: data.records, more: more };
				}catch(err){
					return { results: [], more: 0 };
				}
			}		
		  },  
		  createTag: function (params) {
			var term = $.trim(params.term);
			var id = params.id ? $.trim(params.id) : term;
			if (term === '') {
			  return null;
			}

			return {
			  id: id,
			  text: term,
			  newTag: true // add additional parameters
			}
		  }		  
		}).on("select2:select", function(e) {
			if(e.params.data.newTag){
				var newTag = e.params.data.text;
				var id = e.params.data.id;
				var url = base_url+'/spellcheck';
				var results = $.xResponse(url,{'text':newTag});		
				if( results ){
					$('select.tags option[value="'+id+'"]').remove().trigger('select2:change');
					$('div#ad-wrongtag-modal').modal('show');
				}
			}
		});	 
	},500);

})	