$(function(){
	var map_rz = 0;	
	if( $('div#ad-gallery-modal div.dropzone').length ){
		var opt = $.parseJSON($('div#ad-gallery-modal div.dropzone').attr('data-options'));
		var photo_module_id = opt.module_id+'-'+opt.module;
	}	
	$('a.location-modal').click(function(e){
		e.preventDefault();
		$('div#ad-location-modal').modal('show');
		$('div.alert-location').hide();
		if( map_rz )
			return false;
		setTimeout(function() {
		    map.invalidateSize();
		    map_rz = 1;
		 }, 500);	
		return false;	
	});
	$('div#ad-gallery-modal').on('hidden.bs.modal',function (e) {
		photo_dz_destroy();
	});	
	$('button.ad-gallery-modal-close').click(function () {
		$('div#ad-gallery-modal').modal('hide');
		photo_dz_destroy();
	});	
	function photo_dz_destroy(){
		if( myDropzone && myDropzone[photo_module_id] ){
			$('div#ad-gallery-modal').find('div.dropzone').find('.dz-preview').attr('destroy-called',1);
		  	myDropzone[photo_module_id].destroy()
		  	delete myDropzone[photo_module_id];
		  	$('div#ad-gallery-modal').find('div.dropzone').find('.dz-preview').remove();
		  	$('div#ad-gallery-modal').find('div.dropzone').attr('data-plugin','').removeClass('dz-started');
	  	}		
	}
	$('a.ad-gallery-modal-open').click(function () {
		$('div#ad-gallery-modal').modal('show');
		var modal_content = $('div#ad-gallery-modal').find('div.modal-content');
		modal_content.waitingBlock('Getting Images...');
		modal_content.find('div.dropzone').attr('data-plugin','dropzone').plugins();
		setTimeout(function(){
			modal_content.unblock();
		},500);	
		return false;				
	});		

	$('button.save-ad-location').click(function(e){
		e.preventDefault();
		var modal_content = $(this).parents('div.modal-content');
		var form = modal_content.find('form');
		$('div.alert-location').hide();
		if( !form.valid() )
			return false;
		var request = form.serialize();
		var url = base_url+'/listings';
		modal_content.waitingBlock('Saving...');
		try{
			var results = $.xResponse(url,request);
			if( results ){
				var data = $.parseJSON(results);
				if( !data.success ){
					setTimeout(function(){
						modal_content.unblock();
						$('div.alert-danger-location').show();
					},500);					
					var validator = form.validate();
					var errors = {};
					$.each(data.errors,function(key,val){
						errors[key] = val[0];			
					});
					if( Object.keys(errors).length ){
						refresh_captcha();
						validator.showErrors(errors);
						return;	
					}					
				}else{
					setTimeout(function(){
						modal_content.unblock();
						$('div.alert-success-location').show();
					},500);
				}
			}else{
				$('div.alert-danger-location').show();
				modal_content.unblock();
			}	
		}catch(err){
			$('div.alert-danger-location').show();
			modal_content.unblock();
		}			
		
		
		return false;				
	});

	setTimeout(function(){
		$('select.carmakes').select2({
		  tags: false,	
		  theme: 'material',	
		  minimumInputLength: 3,
		  escapeMarkup: function (markup) { return markup; }, 
		  ajax: {
			url: base_url+'/carmakes/getbyterm',
			method: 'post',
			data: function (params) {
			  var query = {
				_token: csrf,
				car_maker: params.term,
				page: params.page || 1,
				encoded_data: $('div#ad-carmakes-modal input[name="encoded_data"]').val()
			  }

			  // Query parameters will be ?search=[term]&type=public
			  return query;
			},
			processResults: function (dataRes, page) {
				try{
					var data = $.parseJSON(dataRes);	
					var more = (page * 10) < data.count; // whether or not there are more results available
					
					// notice we return the value of more so Select2 knows if more results can be loaded
					return { results: data.records, more: more };
				}catch(err){
					return { results: [], more: 0 };
				}
			}		
		  },  	  
		});
	},500);	

	$('button.save-ad-carmake').click(function(e){
		e.preventDefault();
		var modal_content = $(this).parents('div.modal-content');
		var form = modal_content.find('form');
		$('div.alert-carmake').hide();
		var request = form.serialize();
		var url = base_url+'/listingcarmakes';
		modal_content.waitingBlock('Saving...');
		try{
			var results = $.xResponse(url,request);
			if( results ){
				var data = $.parseJSON(results);
				if( !data.success ){
					setTimeout(function(){
						modal_content.unblock();
						$('div.alert-danger-carmake').show();
					},500);					
					var validator = form.validate();
					var errors = {};
					$.each(data.errors,function(key,val){
						errors[key] = val[0];			
					});
					if( Object.keys(errors).length ){
						refresh_captcha();
						validator.showErrors(errors);
						return;	
					}					
				}else{
					setTimeout(function(){
						modal_content.unblock();
						$('div.alert-success-carmake').show();
					},500);
				}
			}else{
				$('div.alert-danger-carmake').show();
				modal_content.unblock();
			}	
		}catch(err){
			$('div.alert-danger-carmake').show();
			modal_content.unblock();
		}			
		
		
		return false;				
	});	
})