$.fn.initmySelect2 = function(){
	var options = {};
	var options = eval('[' + $(this).attr('data-options') + ']');
	if ($.isPlainObject(options[0])) {
		options[0] = $.extend({}, options[0]);
	}else
		options[0] = {};
	if( options[0].manual_init )
		return;
	
	if( options[0].dropdownParent )	
		options[0].dropdownParent = $(options[0].dropdownParent);
	if( !options[0].theme	 )		
		options[0].theme = 'material';
	if( options[0].escapeMarkup	 ){
		options[0].escapeMarkup = function (markup) { return markup };
	}	
	$(this)[$(this).attr('data-plugin')].apply($(this), options);

}	
$('[data-plugin="select2"]').each(function(){
	$(this).initmySelect2();
});

$(".select2-selection__arrow")
	.addClass("material-icons")
	.html("arrow_drop_down");