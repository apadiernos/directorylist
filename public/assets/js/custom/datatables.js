var api = "";
var table = {};
/* $.extend( true, $.fn.dataTable.defaults, {
	"dom": sDom(),	
} ); */


$(document).ready(function() {	
	$('body').on( 'change','select.addedSearch', function () {
		var findDt = $(this).parents('div#responsive-datatable_wrapper').attr('data-unique');
		var options = table[findDt].options;
		if( options ){
			var oTableUrlSearch = "";
			var oTableUrl = options.ajax.url;
			var api = table[findDt].api;
/* 			$('select.addedSearch').each(function(){
				oTableUrlSearch += $(this).attr('name')+"="+$(this).val()+"&";
			}); */
			api.ajax.url(oTableUrl).load();
		}
	});
	$('body').on('dp.change','.dt-calendar', function(e) {
		var findDt = $(this).parents('div#responsive-datatable_wrapper').attr('data-unique');
		var options = table[findDt].options;
		if( options ){
			var oTableUrlSearch = "";
			var oTableUrl = options.ajax.url;
			var api = table[findDt].api;
/* 			$('select.addedSearch').each(function(){
				oTableUrlSearch += $(this).attr('name')+"="+$(this).val()+"&";
			}); */
			api.ajax.url(oTableUrl).load();
		}
	});		

});

/* Formating dom elements for search and pagination  */
function sDom(){
	//top search
	var sdom = "<'row dt-search-container'";
				
				sdom += "<'col-md-4'";
					sdom += "<'row'";
						sdom += "<'col-md-12 p-l-0'";
							
							sdom += "<'col-md-12 toolbar p-l-0'";
								sdom += "<'table-tools-actions clearfix col-md-12'>";
							sdom += "r>";
							
							
						sdom += "r>";
					sdom += "r>";
				sdom += "r>";
				sdom += "<'col-md-8'";
					sdom += "<'col-md-6'f>";
					sdom += "<'col-md-6'l>";
				sdom += "r>";	
			sdom += "r>";
	
	sdom += "t<'row'<'col-md-12'p i>>";
	return sdom;
}

/**
 *  @description get all the divisions based on the given attribute
 */		
$.fn.createAdditionalSearchElements = function(ajaxdata){
  try {
	 var additionalData = $(this).attr("additional_search"); 
	 try {
		 var addHtml = "";
		 var additionalDataJSON = JSON.parse($.parseJSON(additionalData));
		 var x = 0;
		 $.each(additionalDataJSON, function(i, elementData) {
			if( i == 0 ) addHtml += '<div class="row form-row m-b-xs">';
			else if( i%4 == 0 ) addHtml += '<div class="clearfix"></div></div><div class="row form-row m-b-xs">';
			
			var functionName = "create_"+elementData.element+"_element"; 
			if(functionName){
				addHtml += '<div class="col-md-3 dataTables_length">'+window[functionName](elementData,ajaxdata)+'</div>'; 
			}
		});
		if( addHtml ) $("div.toolbar .table-tools-actions").html('<div class="clearfix"></div></div>'+addHtml);
		$("div.toolbar .table-tools-actions").find('select').plugins();
	 }catch(err){
		console.log('Error in datatableevents.js No additonal search');	
	 };
  }catch(err){
		console.log('Error in datatableevents.js');	
  };		
	if( $('.dt-calendar').length ) $('div.dt-calendar').plugins();
	var findDt = $(this).attr('data-unique');
/* 	var options = table[findDt].options;	
	var api = table[findDt].api;			
	if( api.buttons() ){
		api.buttons().container().appendTo( $('div.toolbar .table-tools-actions', api.table().container() ) );		
	} */
};	

/*
*@param array data jsonObject data to create select tag based on the specified attributes
*@description create select tags
*/
function create_calendar_element(data){
	
	var dt_options = "{ viewMode: 'days', format: '"+date_format+"' }";
	var select_el = '<div class="input-group dt-calendar date date_from_date" id="date_from" data-plugin="datetimepicker" data-options="'+dt_options+'">';
	
	select_el += "<input type='text' ";
	//creating element attributes
	var colName = "";
	var classFound = 0;
	$.each(data.attributes,function(attrib,attribVal){
		if( attrib == "name" )
			colName = attribVal;
		if( attrib == "class" ){
			attribVal = "addedSearch "+attribVal;
			classFound = 1;
		}
		select_el += attrib+'="'+attribVal+'" data-plugin="DataTable" ';
	});
	if( classFound == 0 )
		select_el += ' class="addedSearch" ';
	select_el += "/>";
	
	select_el += '<span class="input-group-addon bg-info text-white">';
			select_el += '<span class="glyphicon glyphicon-calendar"></span>';
		select_el += '</span>';
	select_el += '</div>';
	
	//$('.table-tools-actions select.addedSearch').select2({minimumResultsForSearch: -1});
	return '<label> '+data.label+'</label>'+select_el;
}
	
/*
*@param array data jsonObject data to create select tag based on the specified attributes
*@description create select tags
*/
function create_select_element(data){
	var select_el = "<select ";
	//creating element attributes
	var colName = "";
	$.each(data.attributes,function(attrib,attribVal){
		if( attrib == "name" )
			colName = attribVal;
		if( attrib == "class" )
			attribVal = "addedSearch "+attribVal;
		select_el += attrib+'="'+attribVal+'" ';
	});
	select_el += ">";
	
	select_el += '\n<option value="">Please Choose</option>';
	
	//creating options
	$.each(data.value,function(i,opts){
		var optSel = "";
		var optVal = "";
		var optLabel = "";
		$.each(opts,function(column,colVal){
			//get the option value and selected
			if(data.optionAttributes.value == 'key'){
				optVal = i;
				if(data.selected == i)
					optSel = 'selected="selected"';				
			}else if(column == data.optionAttributes.value){
				optVal = colVal;
				if(data.selected == colVal)
					optSel = 'selected="selected"';
			}
			
			//get the option text
			if(String(column).trim() == String(data.optionAttributes.label).trim()){
				optLabel = colVal;
			}	
			
		});
		select_el += '\n<option value="'+optVal+'"  '+optSel+'>'+optLabel+'</option>';

	});

	select_el += "\n</select>";
	//$('.table-tools-actions select.addedSearch').select2({minimumResultsForSearch: -1});
	return '<label> '+data.label+'</label>'+select_el;
}

/*
*@param array data jsonObject data to create button based on the specified attributes
*@description create button tags
*/
function create_button_element(data){
	var button_el = "<button ";
	//creating element attributes
	var colName = "";
	$.each(data.attributes,function(attrib,attribVal){
		if( attrib == "name" )
			colName = attribVal;
		if( attrib == "class" )
			attribVal = "addedSearch "+attribVal;
		button_el += attrib+'="'+attribVal+'" ';
	});
	button_el += '>'+data.value+'</button>';
	return button_el;
}

/*
*@param array data jsonObject data to create link tag based on the specified attributes
*@description create link tags
*/
function create_link_element(data){
	var a_el = "<a ";
	//creating element attributes
	var colName = "";
	$.each(data.attributes,function(attrib,attribVal){
		if( attrib == "name" )
			colName = attribVal;
		if( attrib == "class" )
			attribVal = "addedSearch "+attribVal;
		a_el += attrib+'="'+attribVal+'" ';
	});
	a_el += '>'+data.value+'</a>';
	return a_el;
}

$.fn.initmyDataTable = function(){
	
	if( $(this).attr('data-unique').length > 0 && $(this).attr('data-options')  ){
		var options = {};
		var options = eval('[' + $(this).attr('data-options') + ']');
		options[0].language = {
			'paginate':{
				'previous':'<span aria-hidden="true">«</span>',
				'next':'<span aria-hidden="true">»</span>',
			}
		};
		console.log(options);
		if ($.isPlainObject(options[0])) {
			options[0] = $.extend({}, options[0]);
		}

		if( !options[0].dom ) options[0].dom = sDom();
		options[0].createdRow = function( row, data, dataIndex ) {
			$.each(data,function(key,val){
				if( isInt(key) == false ){
					$(row).attr(key,val);
				}	
			});			
		};
		if( options[0].buttons ){
			$.each(options[0].buttons.buttons,function(keyButton,buttons){
				if(buttons.action && buttons.text){
					options[0].buttons.buttons[keyButton].action = function ( e, dt, node, config ) {
						var data = dt.ajax.params();
						data.length = dt.settings()[0]._iRecordsTotal;
						if( config.url ){
							var urlParameters = $.xResponse(base_url+base_home+'/reports/get/urlparameters',data);
							window.location.href = config.url+'?'+urlParameters;
							//
							//console.log(results);
						}
					}	
				}
			});	
		}

		$(this)
			.on( 'init.dt', function () {
				var unique = $(this).attr('data-unique');
				$(this).parents('div#responsive-datatable_wrapper').attr('data-unique',unique);					
				$(this).createAdditionalSearchElements();
				$('#responsive-datatable_paginate').attr('class','pagination-wrapper');
			
			})
			.on( 'processing.dt', function ( e, settings, processing ) {
				if( processing ){
					$(this).waitingBlock('Processing...');
				}else{
					$(this).unblock();	
				}
			})			
			.on( 'preXhr.dt', function (e, settings, data) {
				//console.log(data);
				data['__token'] = csrf;
				$('.addedSearch').each(function(){
					if( $(this).attr('name') && $(this).val() ){
						var name = $(this).attr('name');
						var val = $(this).val();
						data[name] = val;
					}	
				});
				return data;
			})		
			.on('xhr.dt', function ( e, settings, json, xhr ) {
				if( !json ){
					$(this).waitingBlock( 'An error has occured: Please Reload Page!');
				}
				//console.log(xhr.responseText);
			})
			.on( 'preInit.dt', function ( e, settings ) {
				var api = new $.fn.dataTable.Api( settings );
				var unique = $(this).attr('data-unique');
				table[unique] = {};
				table[unique].element =  unique;
				table[unique].api =  api;
				try{
					var options = $(this).attr('data-options');
					table[unique].options = $.parseJSON(options);		
				}catch(err){
					console.log(err);
				}
			});
			$(this).attr('table-init',1);
			globalVars[$(this).attr('data-unique')] = $(this)[$(this).attr('data-plugin')].apply($(this), options);
			
	}	
}	
$('table[data-plugin="DataTable"]').each(function(){
	var attr = $(this).attr('table-init');
	if( !attr )
		$(this).initmyDataTable();
});

