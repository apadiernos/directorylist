$.fn.initmyVectorMap = function(){
	
	if( $(this).attr('data-options')  ){
		var options = {};
		var options = eval('[' + $(this).attr('data-options') + ']');
		if ($.isPlainObject(options[0])) {
			options[0] = $.extend({}, options[0]);
		}
		options[0].onMarkerTipShow = function( e, el, code ) {
			if(options[0].markers[code].content)
			 el.html(el.html()+options[0].markers[code].content);
		};
		options[0].onRegionTipShow = function( e, el, code ) {
			e.preventDefault();
		};		
		 $(this)[$(this).attr('data-plugin')].apply($(this), options);
			
	}	
}	
$('[data-plugin="vectorMap"]').each(function(){
		$(this).initmyVectorMap();
});
