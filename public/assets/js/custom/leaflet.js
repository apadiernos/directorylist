var map;
$.fn.initmyLeaflet = function(){
	var options = {};
	var options = eval('[' + $(this).attr('data-options') + ']');

	var center = ( options[0] && options[0].location ) ? options[0].location[0].center : [15.156311, 120.591774];
	var id = $(this).attr('id');
	map = L.map(id).setView(center, 12);
		
	//map.scrollWheelZoom.disable();
	var marker_cluster = L.markerClusterGroup();	
	
	L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {		
		scrollWheelZoom: false,		
	}).addTo(map);

	
	if( options[0] && options[0].location ){
		$.each(options[0].location, function(index, value) {
			var icon = L.divIcon({
				html: value.icon,
				iconSize:     [36, 36],
				iconAnchor:   [36, 36],
				popupAnchor:  [-20, -42]
			});

			var marker = L.marker(value.center, {
				icon: icon
			}).addTo(map);		

			marker.bindPopup(
				'<div class="listing-window-image-wrapper">' +
					'<a href="'+value.url+'/'+'">' +
						'<div class="listing-window-image" style="background-image: url(' + value.image + ');"></div>' +
						'<div class="listing-window-content">' +
							'<div class="info">' +
								'<h2>' + value.title + '</h2>' +
								'<h3>' + value.category + '</h3>' +
							'</div>' +
						'</div>' +
					'</a>' +
				'</div>'
			);

			marker_cluster.addLayer(marker);
		});

		map.addLayer(marker_cluster);
		if( options[0] ){
			map.customoptions = options[0];		
		}
	}	

}	
$('[data-plugin="leaflet"]').each(function(){
	$(this).initmyLeaflet();
});
