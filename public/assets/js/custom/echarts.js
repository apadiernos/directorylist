$.fn.initmyEcharts = function(){
	var options = {};
	var options = eval('[' + $(this).attr('data-options') + ']');
	if ($.isPlainObject(options[0])) {
		options[0] = $.extend({}, options[0]);
	}
	$.each(options[0].series,function(key,val){
		try{
			if( options[0].series[key].areaStyle.normal.gradient ){
				options[0].series[key].areaStyle.normal.color = new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
					offset: 0,
					color: 'rgb(255, 158, 68)'
				}, {
					offset: 1,
					color: 'rgb(255, 70, 131)'
				}]);				
			}	
		}catch(err){
			console.log(err);
			console.log('No Gradient');
		}			
		try{
			if( options[0].series[key].tooltip.reformat ){
				options[0].series[key].tooltip.formatter = function (params, ticket, callback) {
					var tooltip = "";
					
					$.each(params,function(key,param){
						var dataIndex = param.dataIndex;
						var append_val = "";
						try{
							if( param.series.tooltip.no_title != 1 ){
								tooltip += param.series.name+": ";
								if( param.series.append_data ) append_val = param.series.append_data;
								tooltip += param.value+append_val+"<br> ";
							}
						}catch(err){
							console.log('No Title');
						}			
						try{
							if(param.series.tooltip.added_data[dataIndex]){
								tooltip += param.series.tooltip.added_data[dataIndex]+"<br> ";
							}
						}catch(err){
							console.log('No Added Data');
						}								
					});		
					return tooltip;
				}
			}
		}catch(err){
			console.log('No Formatter');
		}
	});	
	$(this)[$(this).attr('data-plugin')].apply($(this), options);
}
$('div[data-plugin="chart"]').each(function(){
	$(this).initmyEcharts();
});