var marker;
$(document).ready(function() {	
	$('body').on('change','select.select-state',function(){
		var state = $('option:selected',this).val();
		var country = 'PHL';
		if( country && state ){
			try{
				var url =  base_url+'/listings/getlocationajax';
				var results = $.xResponse(url,{country:country,state:state});
				if( results ){
					if( map ){
						var data = $.parseJSON(results);
						map.panTo(new L.LatLng(data.lat, data.long));					
						$('input[name="long"]').val(data.long);	
						$('input[name="lat"]').val(data.lat);	
					}
				}		
			}catch(err){
				console.log(err);
				console.log('Error saving listing category');
			}
		}		
	});	
	setTimeout(function(){
		if (typeof map !== 'undefined'){
			map.on("click", function(e){
				if(marker)
					map.removeLayer(marker)
				var icon = L.divIcon({
					html: '<i class="fas fa-map-pin"></i>',
					iconSize:     [36, 36],
					iconAnchor:   [36, 36],
					popupAnchor:  [-20, -42]
				});

				marker = L.marker([e.latlng.lat, e.latlng.lng], {
					icon: icon
				}).addTo(map);
				$('input[name="long"]').val(e.latlng.lng);	
				$('input[name="lat"]').val(e.latlng.lat);	
			});
			if( $('input[name="long"]').val() && $('input[name="lat"]').val() ){
				map.setView(new L.LatLng($('input[name="lat"]').val(), $('input[name="long"]').val()), 20);
				var icon = L.divIcon({
					html: '<i class="fas fa-map-pin"></i>',
					iconSize:     [36, 36],
					iconAnchor:   [36, 36],
					popupAnchor:  [-20, -42]
				});

				marker = L.marker([$('input[name="lat"]').val(), $('input[name="long"]').val()], {
					icon: icon
				}).addTo(map);		
			}
		}
	},2000)	
	$('body').on('blur','input[name="lat"],input[name="long"]',function(){
		if( $('input[name="long"]').val() && $('input[name="lat"]').val() ){
			console.log('sadsa');
			if(marker)
				map.removeLayer(marker)			
			map.setView(new L.LatLng($('input[name="lat"]').val(), $('input[name="long"]').val()), 20);
			var icon = L.divIcon({
				html: '<i class="fas fa-map-pin"></i>',
				iconSize:     [36, 36],
				iconAnchor:   [36, 36],
				popupAnchor:  [-20, -42]
			});

			marker = L.marker([$('input[name="lat"]').val(), $('input[name="long"]').val()], {
				icon: icon
			}).addTo(map);	
			map.flyTo(new L.LatLng($('input[name="lat"]').val(), $('input[name="long"]').val()));	
		}
	});	
	$('body').on('blur','input[name="address"]',function(){
		paste_address();
	});		
	$('body').on('change','div.address-select select',function(){
		paste_address();
	});	
})
function paste_address()
{
	var text = "";
	setTimeout(function(){
		if( $('input[name="address"]').val() ) text += $('input[name="address"]').val();	
		if( $('select.select-brgy option:selected').val() ) text += ' '+$('select.select-brgy option:selected').text();	
		if( $('select.select-city option:selected').val() ) text += ' '+$('select.select-city option:selected').text()+',';	
		if( $('select.select-state option:selected').val() ) text += ' '+$('select.select-state option:selected').text();	
		$('label#label-str').html(text+' Philippines');
	},300);	
}