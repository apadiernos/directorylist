
Dropzone.autoDiscover = false;
var myDropzone = {};
var dzMaxfiles = 0;
//$(document).ready(function() {	
	$('[data-plugin="dropzone"]').each(function(){
		var self = $(this);
		var options = {};
		var options = eval('[' + self.attr('data-options') + ']');
		if ($.isPlainObject(options[0])) {
			options[0] = $.extend({}, options[0]);
		}
		var initdata = $.extend({
			'module' : options[0].module,
			'module_id' : options[0].module_id,
		},options[0].addeddata);
		var module_id = options[0].module_id+'-'+options[0].module;
		if( options[0].maxFiles )
			dzMaxfiles = parseInt(options[0].maxFiles);

		var myDz = new Dropzone(this,{
			url : options[0].url,
			addRemoveLinks : true,	
			dictRemoveFile: '',			
			acceptedFiles: "image/*",
			init: function(){
				$.ajax({
					type: 'POST',
					url: options[0].geturl,
					data: initdata,	
					beforeSend: function (xhr, settings) {
						xhr.setRequestHeader("X-CSRF-Token", csrf);
					},						
					success: function (attachments) {
						try{
							var jsonAttachments = $.parseJSON(attachments);
							var existingFileCount = 0;
							$.each(jsonAttachments,function(key,attachment){
								if(key != 'dir'){
									var fileDir = attachment.thumbnail;
									var mockFile = { name: attachment.file_name, size: attachment.file_size, type: attachment.file_type };
									myDz.emit("addedfile", mockFile);
									//myDz.options.addedfile.call(dropEl, mockFile)
									myDz.emit("thumbnail", mockFile, fileDir);
									$(mockFile.previewElement).attr('fileId', attachment.id);
									//dropEl.addFile.call(dropEl, mockFile);
									//myDz.options.thumbnail.call(dropEl, mockFile, fileDir);
									$(mockFile.previewElement).find('div.dz-progress').remove();
									$(mockFile.previewElement).append('<a href="'+attachment.full+'" target="_blank" style="font-size:14px;cursor:pointer" class="dz-url fas fa-eye m-t-xs  m-l-xs btn btn-outline btn-sm btn-primary"></a>');
									$(mockFile.previewElement).find('a.dz-remove').addClass('fas fa-trash m-l-md m-t-xs btn btn-outline btn-sm btn-danger').css({'display':'inline-block','border':'1px solid'}).attr('data-id',attachment.id).attr('data-id',attachment.id).attr('data-record',attachment.data_record);
																
									existingFileCount++;
								}
							});
							if( options[0].maxFiles ){
								myDz.options.maxFiles = dzMaxfiles - existingFileCount;
								dzMaxfiles = parseInt(myDz.options.maxFiles);
							}
						}catch(err){
							console.log(err);
							console.log('Error in form_element.js! Error Getting Dropzone file');
						}							
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 	
						console.log(XMLHttpRequest);
						console.log(errorThrown);
						//showErrorMessage(global_error_notif);
						console.log('Error in form_element.js');					
					}
				});	
				
			},				
		});
		myDropzone[module_id] = myDz;
		myDropzone[module_id].on('maxfilesexceeded', function(file, xhr, formData){
			console.log('Exceeds File Upload');
		});			
		myDropzone[module_id].on('sending', function(file, xhr, formData){
			formData.append('_token', csrf);
			formData.append('module', options[0].module);
			formData.append('module_id', options[0].module_id);
			$.each(options[0].addeddata,function(key,val){
				formData.append(key, val);
			});
			if( options[0].callbacks.sending )
				window[options[0].callbacks.sending]();			
		});
		myDropzone[module_id].on("success", function(file) {
			$(file.previewTemplate).append('<a href="'+file.fullname+'" target="_blank" style="font-size:14px;cursor:pointer" class="dz-url fas fa-eye m-t-xs m-l-xs btn btn-outline btn-sm btn-primary"></a>');
			$(file.previewTemplate).find('.dz-remove').addClass('fa fa-trash m-l-md m-t-xs btn btn-outline btn-sm btn-danger').css({'display':'inline-block','border':'1px solid'});
			if( options[0].callbacks.success )
				window[options[0].callbacks.success]();	
		});	
		myDropzone[module_id].on("removedfile", function(file) {
			if( $(file.previewElement).attr('destroy-called') )
				return;
			var data = {};
			$.each(options[0].addeddata,function(key,val){
				data[key] = val;
			});			
			var fileId = $(file.previewElement).find('.dz-remove').attr('data-id');	
			var results = $.xResponse(options[0].removeurl+'/'+fileId,data,'delete');
			if( dzMaxfiles )	
				myDropzone[module_id].options.maxFiles = dzMaxfiles--; 				
		});		
		myDropzone[module_id].on("complete", function(data) {
			if( data.status == "error" )
				$(data.previewTemplate).remove();
			else{
				var attachment;
				try{
					attachment = $.parseJSON(data.xhr.responseText);
					$(data.previewElement).find('.dz-url').attr('href',attachment.url);
					$(data.previewElement).find('.dz-remove').attr('data-id',attachment.id);				
				}catch(err){
					//showErrorMessage(global_error_notif);
					console.log('Error in form_element.js! Error Parsing JSON Result');
				}
			}			
		});	
		myDropzone[module_id].on("error", function(data) {
			console.log(data);		
		});			
	});
//});	