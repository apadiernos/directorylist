
$(document).on('change', '.bs-file', function(e) {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [numFiles, label]);

});

$(document).ready(function() {	
	//document.styleSheets[0].insertRule('div.popover-validate-error div.popover-arrow:after { border-bottom-color: rgb(255,0,0,0.5) !important; }', 0);
	
    $('body').on('fileselect','.bs-file', function(event, numFiles, label) {
		try{
			var tmppath = URL.createObjectURL(event.target.files[0]);
			var imgSrc = $(this).parents('.single-image-upload').find('.bs-file-img');
			if( imgSrc ) imgSrc.attr('src',tmppath);
		}catch(err){
			console.log('Error in form_validations.js! No image container');
		}
		var labelSrc = $(this).parents('.single-image-upload').find('.bs-file-input');		
        if( labelSrc ) labelSrc.val(label);
    });
	/**-------------------------------- Country and state --------------------------------------------------------------**/
	$('.select-state').change(function(){
		var state = $(this).val();		
		var select_city = $(this).parents('.address-select').find('.select-city');
		var select_region = $(this).parents('.address-select').find('.select-region');
		var select_region_code = $(this).parents('.address-select').find('.select-region-code');
		var select_brgy = $(this).parents('.address-select').find('.select-brgy');
		var choose = select_city.attr('choose');
		select_city.html('').hide();
		select_brgy.html('').hide();
		if( state != "" ){
			try{
				if( select_city.length > 0 ){
					try{
						var getCitiesUrl = base_url+'/cities';		
						var cities = $.xResponse(getCitiesUrl,{state : state});
						var dataRes = $.parseJSON(cities);	
						var stateHtm = '<option value="">'+choose+'</option>';
						$.each(dataRes,function(town,brgys){
							var brgyData = JSON.stringify(brgys.barangay_list);
							stateHtm += '<option value="'+town+'"';
							stateHtm +=  "data-brgy='"+brgyData+"'>"+town+"</option>";
						});
						select_city.show().html(stateHtm).attr('data-plugin','select2').plugins();
						select_region.val(dataRes.region);
						select_region_code.val(dataRes.region_code);
					}catch(err){
						console.log(err);
						//showErrorMessage(global_error_notif);
						console.log('Error in form_validations.js! No states');
					}					
				}
					//showErrorMessage(global_error_notif);
			}catch(err){
				//showErrorMessage(global_error_notif);
				console.log('Error in form_validations.js! No countries');
			}	

		}
	});
	$('.select-city').change(function(){
		var city = $(this).val();		
		var select_brgy = $(this).parents('.address-select').find('.select-brgy');
		var choose = select_brgy.attr('choose');
		var brgys = $('option:selected',this).attr('data-brgy');
		//console.log(brgys);
		select_brgy.html('').hide();
		if( city != "" ){
			try{
				if( select_brgy.length > 0 ){
					try{
						var dataRes = $.parseJSON(brgys);	
						var stateHtm = '<option value="">'+choose+'</option>';
						$.each(dataRes,function(key,brgy){
							stateHtm += '<option value="'+brgy+'" >'+brgy+'</option>';
						});
						select_brgy.show().html(stateHtm).attr('data-plugin','select2').plugins();
					}catch(err){
						console.log(err);
						//showErrorMessage(global_error_notif);
						console.log('Error in form_validations.js! No states');
					}					
				}
					//showErrorMessage(global_error_notif);
			}catch(err){
				//showErrorMessage(global_error_notif);
				console.log('Error in form_validations.js! No countries');
			}	

		}
	});	
	/**-------------------------------- Country and state --------------------------------------------------------------**/	
	
	try{
		/**-------------------------------- Icon Validator Methods --------------------------------------------------------------**/
		/**
		 *  @params label object element for error message
		 *  @description errorplacement validation content
		 */
		$.fn.errorPlacementIcon = function(label){
			$(this).addClass('validated validate-error').removeClass('validate-success');
			var icon = $(this).parent('.input-with-icon').children('i');
			if(label){
				if( $(this).attr('error-message') )
					label = $(this).attr('error-message');
				else 
					label = $(label).text();
				
				$(this).attr('data-toggle','popover').attr('data-placement','bottom').attr('data-content',label).popover({
						placement : 'bottom',
						template: '<div class="popover popover-validate-error bg-danger b-0"><div class="popover-arrow"></div><div class="popover-inner"><div class="popover-content p-xs"><p></p></div></div></div>'
				}).popover('show');
			}
			var parent = $(this).parent('.input-with-icon');
			$(this).parents('.form-group').addClass('has-error').removeClass('has-success');
			$(this).parent('.input-with-icon').find('span.validate-status').html('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span><span id="inputError2Status" class="sr-only">(error)</span>');			
		};
		/**
		 *  @description highlight validation content
		 */	
		$.fn.highlightIcon = function(){
			//$(this).parents('.form-group').removeClass('has-error').removeClass('has-success');			
		};
		/**
		 *  @description success validation content
		 */		
		$.fn.successIcon = function(){
			$(this).addClass('validated validate-success').removeClass('validate-error');
			$(this).parent('.input-with-icon').find('span.validate-status').html('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span><span id="inputSuccess2Status" class="sr-only">(success)</span>');
			var parent = $(this).parent('.input-with-icon');
			$(this).parents('.form-group').addClass('has-success').removeClass('has-error');
			$(this).popover('dispose');		
		};		
		/**-------------------------------- Icon Validator Methods --------------------------------------------------------------**/	
		
	   /**-------------------------------- Icon Validator Addt'l Methods --------------------------------------------------------------**/
		/**
		 *  @description check if record exists in database using Servervalidation Controller. Located at sadmin/Servervalidation.php
		 */	
		$.validator.addMethod("recordExists", function(value, el, param) {
			try{
				var options = $(el).attr('duplicate-options');
				var dataRes = $.parseJSON(options);	
				if( dataRes.default_value != value ){
					var dataParam = {};
					dataParam.url = dataRes.url;
					dataParam.params = {};	
					dataParam.params[dataRes.column] = value;
					if(dataRes.added_params){
						$.each(dataRes.added_params,function(key,val){
							dataParam.params[key] = val;
						})
					}
					var jsonStr = JSON.stringify(dataParam);
					$(el).attr('data-parameters',jsonStr);
					var duplicaterecord = $(el).getRecord();
				}else
					return true;
			}catch(err){
				console.log('error at form_validation.js cannot parse duplicate options');
			}		
			
			if( duplicaterecord && duplicaterecord.data_status == 1 )
				return false;
			else
				return true;
		},function(val,el){
			var dataRes = {};
			try{
				var options = $(el).attr('duplicate-options');
				var dataRes = $.parseJSON(options);	
			}catch(err){
				console.log('error at form_validation.js cannot parse duplicate options');
			}
			return dataRes.message ? dataRes.message : "Existing Database Record";
		});
		/**
		 *  @description check if value of .duplicate element has duplicates
		 */		
		$.validator.addMethod("valueExists", function(value, element, param) {
			var ctr = 0;
			var val = "";
			$(".duplicate").each(function(){
				if( val == $(this).val() ){
					ctr++;	
				}
				val = $(this).val();			
			});
			if( ctr >= 2 )
				return false;
			else
				return true;
		},function(val){
			return "Existing Form Record";
		});
		$.validator.addMethod("captchaError", function(value, element, param) {
			if( value  )
				return false;
			else
				return true;
		},function(val){
			return "Invalid Captcha";
		});		
		$.validator.addMethod("pwcheck", function(value, element) {
			if( value.length == 0 )
				return true;
			else{
			   return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these
				   && /[a-z]/.test(value) // has a lowercase letter
				   && /\d/.test(value) // has a digit
				   && value.length >= 8
			}
		},function(val,el){
			return $(el).attr('validation-message') ? $(el).attr('validation-message') : "Password minimmum requirements no reached";
		});
/* 		$.validator.addMethod("requiredField", function(value, element) {
			  return value.length > 0
		},function(val,el){
			return "This field is required";
		}); */

		/**-------------------------------- Icon Validator Addt'l Functions --------------------------------------------------------------**/    

		$('select[data-plugin="select2"]','.formvalidation').change(function () {
			$('form[data-plugin="validate"]').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
		});

	/* 	$('body').on('keyup','.duplicate',function () {
			$(this).rules('add', {
				recordExists: 1,
				valueExists: 1,
			});      
		}); */
		$('body').on('blur','.duplicate',function () {
			$(this).rules('add', {
				recordExists: 1,
				valueExists: 1,
			});      
		});
		$('body').on('blur','.password-check',function () {
			$(this).rules('add', {
				pwcheck: 1,
			});      
		});		
/* 		$('body').on('blur','.required',function () {
			$(this).rules('add', {
				requiredField: 1,
			});      
		});	 */		
	}catch(err){
		console.log('No validation');
	}	

});
var validator;
var globalValidator;

$.fn.initmyValidator = function(){
	//Iconic form validation sample	
	var validator = $(this).validate({
		errorElement: 'span', 
		errorClass: 'error', 
		focusInvalid: false, 

		ignore: "",
		invalidHandler: function (event, validator) {
			//console.log(validator);
			//display error alert on form submit    
		},

		errorPlacement: function (label, element) { // render error placement for each input type
			$(element).errorPlacementIcon(label);
		},

		highlight: function (element) { // hightlight error inputs
			$(element).highlightIcon(); 
		},

		unhighlight: function (element) { // revert the change done by hightlight
			
		},

		success: function (label, element) {
			$(element).successIcon();
		},
		submitHandler: function(form){
			//console.log(form);
			form.submit();
		}
	   
	});
	if( $(this).attr('data-unique') )
		globalValidator[$(this).attr('data-unique')] = validator;
}	

$('form[data-plugin="validate"]').each(function(){
	$(this).initmyValidator();
});
	
$.fn.getRecord = function(){
	var getRecordLibUrl = base_url+'servervalidation/servervalidation/getRecord';
	try{
		var params = $(this).attr('data-parameters');
		var getData = $.xResponse(getRecordLibUrl, {'data':params});
		var dataRes = $.parseJSON(getData);	
		return dataRes;
	}catch(err){
		console.log('error at form_validation.js');
	}
	//var callbackfunction= window[callbackFunctionName];
	
};
$.fn.updateRecord = function(){
	var updateRecordLibUrl = base_url+'servervalidation/servervalidation/updateRecord';
	try{
		var params = $(this).attr('data-parameters');
		var getData = $.xResponse(updateRecordLibUrl, {'data':params});
		var dataRes = $.parseJSON(getData);	
		return dataRes;
	}catch(err){
		console.log('error at form_validation.js');
	}
	//var callbackfunction= window[callbackFunctionName];
	
};