<?php

namespace App\model\auth;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
	protected $fillable = [
		'city','brgy','user_id','country','state','region','region_code','address','long','lat','updated_by','created_by'
	];
	public function user()
	{
		return $this->belongsTo('App\User', 'user_id', 'id');
	}		
}
