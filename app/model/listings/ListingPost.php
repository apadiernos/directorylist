<?php

namespace App\model\listings;

use Illuminate\Database\Eloquent\Model;

class ListingPost extends Model
{
	protected $fillable = [
		'user_id','listing_id','title','unique_id','slug_term'
	];
	public function listing()
	{
		return $this->belongsTo('App\model\listings\Listing', 'listing_id', 'id');
	}
	public function subposts() {
		$record = $this->hasMany('App\model\listings\ListingSubPost','listing_post_id','id');
		return $record;
	}	
	public function subpostsTemp() {
		$record = $this->hasMany('App\model\listings\ListingSubPost','unique_post_id','unique_id')->where('listing_post_id',0);
		return $record;
	}					
}
