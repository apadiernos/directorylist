<?php

namespace App\model\listings;

use Illuminate\Database\Eloquent\Model;

class UserListView extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','listing_id','updated_by','created_by'
    ];

	public function list() {
		$record = $this->belongsTo('App\model\listings\Listing','listing_id','id');
		return $record;
	}  
}
