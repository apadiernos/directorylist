<?php

namespace App\model\listings;

use Illuminate\Database\Eloquent\Model;

class ReviewReaction extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','reaction','review_id','listing_id','visitor','timecreated'
    ];
	public function user()
	{
		return $this->belongsTo('App\User', 'user_id', 'id');
	}				
}
