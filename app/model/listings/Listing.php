<?php

namespace App\model\listings;

use Illuminate\Database\Eloquent\Model;

class Listing extends Model
{
	protected $fillable = [
		'city','brgy','category_id','ratings','title','email','website','description','sections','socialmedia','country','state','region','region_code','address','long','lat','slug_term','updated_by','created_by','user_id','status'
	];
	public function category() {
		$record = $this->belongsTo('App\model\listings\ListingCategory','category_id','id');
		return $record;
	}		
	public function reviews() {
		return $this->hasMany('App\model\listings\Review','listing_id','id');
	}
	public function review_reactions() {
		$record = $this->hasMany('App\model\listings\ReviewReaction','listing_id','id');
		return $record;
	}

	public function rating() {
		$record = $this->reviews()->selectRaw('avg(rating) as aggregate, listing_id')->groupBy('listing_id');
		return $record;
	}	

	public function attachments() {
		$record = $this->hasMany('App\model\listings\Attachment','module_id','id')->where('module','listings');
		return $record;
	}
	public function reports() {
		$record = $this->hasMany('App\model\listings\UserListReport','listing_id','id');
		return $record;
	}	
	public function likes() {
		$record = $this->hasMany('App\model\listings\UserListLike','listing_id','id');
		return $record;
	}		
	public function tags() {
		$record = $this->hasMany('App\model\listings\ListingTag','listing_id','id');
		return $record;
	}
	public function carmakes() {
		$record = $this->hasMany('App\model\listings\ListingCarMake','listing_id','id');
		return $record;
	}	
	public function posts() {
		$record = $this->hasMany('App\model\listings\ListingPost','listing_id','id');
		return $record;
	}	
	public function subposts() {
		$record = $this->hasMany('App\model\listings\ListingSubPost','listing_id','id');
		return $record;
	}		
	protected static function boot() 
    {
      parent::boot();
      static::deleting(function($listings) {
         $listings->reviews()->delete();
         $listings->review_reactions()->delete();
         $listings->attachments()->delete();		 
      });
    }	
}
