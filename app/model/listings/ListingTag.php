<?php

namespace App\model\listings;

use Illuminate\Database\Eloquent\Model;

class ListingTag extends Model
{
	protected $fillable = [
		'user_id','listing_id','listing_post_id','listing_subpost_id','tag_id'
	];
	public function tag()
	{
		return $this->belongsTo('App\model\listings\Tag', 'tag_id', 'id');
	}	
}
