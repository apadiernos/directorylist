<?php

namespace App\model\listings;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email','name','user_id','rating','message','parent_id','listing_id','visitor','timecreated','status','module'
    ];
	public function user()
	{
		return $this->belongsTo('App\User', 'user_id', 'id');
	}	
	public function replies() {
		$record = $this->hasMany('App\model\listings\Review','parent_id','id');
		return $record;
	}
	public function review_reactions() {
		$record = $this->hasMany('App\model\listings\ReviewReaction','review_id','id');
		return $record;
	}
	public function reports() {
		$record = $this->hasMany('App\model\listings\UserReviewReport','review_id','id');
		return $record;
	}	
	protected static function boot() 
    {
      parent::boot();
      static::deleting(function($reviews) {
         $reviews->replies()->delete();
         $reviews->review_reactions()->delete();	 
      });
    }	
}
