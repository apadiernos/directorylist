<?php

namespace App\model\listings;

use Illuminate\Database\Eloquent\Model;

class ListingSubPost extends Model
{
	protected $table = 'listing_subposts';
	protected $fillable = [
		'user_id','listing_id','listing_post_id','title','description','unique_id','unique_post_id'
	];
	public function listing()
	{
		return $this->belongsTo('App\model\listings\Listing', 'listing_id', 'id');
	}	
	public function post()
	{
		return $this->belongsTo('App\model\listings\ListingPost', 'listing_post_id', 'id');
	}
	public function attachments() {
		$record = $this->hasMany('App\model\listings\Attachment','module_id','id')->where('module','subposts');
		return $record;
	}
	public function attachmentsTemp() {
		$record = $this->hasMany('App\model\listings\Attachment','module_id','unique_id')->where('module','subposts');
		return $record;
	}					
}
