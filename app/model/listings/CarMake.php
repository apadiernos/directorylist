<?php

namespace App\model\listings;

use Illuminate\Database\Eloquent\Model;

class CarMake extends Model
{
    protected $fillable = [
        'car_maker','updated_by','created_by'
    ];
}
