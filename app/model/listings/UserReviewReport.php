<?php

namespace App\model\listings;

use Illuminate\Database\Eloquent\Model;

class UserReviewReport extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','listing_id','review_id','message'
    ];
}
