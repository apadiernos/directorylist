<?php

namespace App\model\listings;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class ListingCategory extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category','icon','slug_term','updated_by','created_by'
    ];
	public function listings() {
		$record = $this->hasMany('App\model\listings\Listing','category_id','id');
		return $record;
	}
	protected static function boot() 
    {
      parent::boot();
      static::deleting(function($categories) {
         $categories->listings()->delete();	 
      });
    }	
}
