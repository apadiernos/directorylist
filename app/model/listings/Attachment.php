<?php

namespace App\model\listings;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    protected $fillable = [
		'file_name','file_type','file_size','file_ext','file_raw_name','module','module_id','updated_by','created_by'
	];    //
}
