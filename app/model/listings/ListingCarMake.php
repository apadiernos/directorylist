<?php

namespace App\model\listings;

use Illuminate\Database\Eloquent\Model;

class ListingCarMake extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','listing_id','car_make_id'
    ];
	public function listing()
	{
		return $this->belongsTo('App\model\listings\Listing', 'listing_id', 'id');
	}   
	public function carmake()
	{
		return $this->belongsTo('App\model\listings\CarMake', 'car_make_id', 'id');
	} 	 
}
