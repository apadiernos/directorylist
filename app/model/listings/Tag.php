<?php

namespace App\model\listings;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
	public $timestamps = true;

	protected $fillable = [
		'tag','updated_by','created_by'
	];
}
