<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class AdminUser extends Model
{
	use Notifiable;

	protected $guard = 'adminuser';

	protected $fillable = [
		'email', 'password',
	];

	protected $hidden = [
		'password', 'remember_token',
	];
}
