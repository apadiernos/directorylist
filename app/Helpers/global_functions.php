<?php
use Illuminate\Support\Facades\Crypt;
function di($data){	
	print "<pre>";	print_r($data);	print "</pre>";
}	
function emojione($message){
	return LaravelEmojiOne::toImage(nl2br($message));
}
function clean_jsonString($jsonString){
	return htmlspecialchars(json_encode($jsonString), ENT_QUOTES, 'UTF-8');
}	
function encode_data_short($data){
	$randomKey = '@@@';	
	return base64_encode($randomKey . $data);
}
function decode_data_short($data){
	return str_replace('@@@','',base64_decode($data));
}
function encode_array($array){
	return Crypt::encryptString(json_encode($array));
}
function decode_array($encrypted){
	return json_decode(Crypt::decryptString($encrypted));
}	
function prepare_datatable_results($tableParams){
	$response = array(
		"draw"=>$tableParams->offset,
		"recordsTotal"=>$tableParams->recordsTotal,
		"recordsFiltered"=>$tableParams->recordsFiltered,
	);	
	$response["data"] = $tableParams->dataList;
	return json_encode($response);
}	

function excerpt($title, $cutOffLength) {

    $charAtPosition = "";
    $titleLength = strlen($title);
    if( $titleLength <=  $cutOffLength)
     	return $title;
    do {
        $cutOffLength++;
        $charAtPosition = substr($title, $cutOffLength, 1);
    } while ($cutOffLength < $titleLength && $charAtPosition != " ");
   	return substr($title, 0, $cutOffLength) . '...';
}

function get_image_sizes(){
	$config["image_sizes"]["square1"] = array(60, 60);
	$config["image_sizes"]["square2"] = array(100, 100);
	$config["image_sizes"]["square3"] = array(130, 130);
	$config["image_sizes"]["square4"] = array(300, 300);

	$config["image_sizes"]["long1"]   = array(230, 300);

	$config["image_sizes"]["wide1"]   = array(224, 132);
	$config["image_sizes"]["wide2"]   = array(230, 160);
	$config["image_sizes"]["wide3"]   = array(285, 193);
	$config["image_sizes"]["wide4"]   = array(306, 180);
	$config["image_sizes"]["wide5"]   = array(306, 189);

	$config["image_sizes"]["large1"]   = array(638, 280);
	$config["image_sizes"]["large2"]   = array(700, 200);
	$config["image_sizes"]["large3"]   = array(700, 420);
	$config["image_sizes"]["thumbnail"]   = array(351, 220);
	return 	$config["image_sizes"];
}

function get_attachment_url($attachment,$filedirectory='all',$size=null)
{
	$attachments = array();
	if( $size ){
		$image_sizes = get_image_sizes();
		$size = $image_sizes[$size] ? $image_sizes[$size] : null;
	}
	$attachmentArr = array_map('trim',explode(",", $attachment));
	foreach($attachmentArr as $key => $recordArr){
		$record = array_map('trim',explode("::", $recordArr));
		if( isset($record[0]) && isset($record[2]) ){
			$filenametostorefolder = $record[0].'-'.$record[2];
			if( $size ){
				$thumbnail = $filenametostorefolder.'-'.$size[0].'x'.$size[1].'.'.$record[1];
				$attachments[] = Storage::url($filedirectory.'/'.$filenametostorefolder.'/'.$thumbnail);
			}else	
				$attachments[] = Storage::url($filedirectory.'/'.$filenametostorefolder.'.'.$record[1]);
		}
	}
	return $attachments;	
}

function get_attachment_url_by_record($record,$filedirectory='all',$size=null)
{
	$attachments = array();
	if( $size ){
		$image_sizes = get_image_sizes();
		$size = $image_sizes[$size] ? $image_sizes[$size] : null;
	}
	if( isset($record->file_raw_name) && isset($record->module_id) ){
		$filenametostorefolder = $record->file_raw_name.'-'.$record->module_id;
		if( $size ){
			$thumbnail = $filenametostorefolder.'-'.$size[0].'x'.$size[1].'.'.$record->file_ext;
			$attachment = Storage::url($filedirectory.'/'.$filenametostorefolder.'/'.$thumbnail);
		}else	
			$attachment = Storage::url($filedirectory.'/'.$filenametostorefolder.'.'.$record->file_ext);
	}
	return $attachment;	
}

function format_time($timestamp=null,$withtime=false)
{
	if( !$timestamp ) return null;
	$default_date_format = "d/m/Y";
	$default_time_format = "g:i A";	
	
	$format = $withtime ? " ".$default_time_format  : null;
	return date($default_date_format.$format,$timestamp);		
}



function get_time_ago( $time )
{
    $time_difference = time() - $time;

    if( $time_difference < 1 ) { return 'less than 1 second ago'; }
    $condition = array( 12 * 30 * 24 * 60 * 60 =>  'year',
                30 * 24 * 60 * 60       =>  'month',
                24 * 60 * 60            =>  'day',
                60 * 60                 =>  'hour',
                60                      =>  'min',
                1                       =>  'second'
    );

    foreach( $condition as $secs => $str )
    {
        $d = $time_difference / $secs;

        if( $d >= 1 )
        {
            $t = round( $d );
            return $t . ' ' . $str . ( $t > 1 ? 's' : '' );
        }
    }
}