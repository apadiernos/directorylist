<?php
function get_fullname($user)
{
	return ucfirst($user->first_name) . ' ' . ucfirst($user->last_name);
}
function get_comment_template($module,$is_reply=0,$review=array(),$review_reactions=array())
{	
	$post_id = strpos($module, 'post-') === true ? str_replace("post-",$module) : 0; 
	$listing_id = isset($review->listing_id) ? $review->listing_id : 0;
	$id = isset($review->id) ? $review->id : 0;
	$parent_id = isset($review->parent_id) ? $review->parent_id : 0;
	$message = isset($review->message) ? $review->message : null;
	$rating = isset($review->rating) ? $review->rating : 0;
	$pp = asset('assets/img/tmp/profile-3.jpg');
	if($review->user){
		$name = get_fullname($review->user);
		$pp = get_attachment_url_by_record($review->user->profilepicture,'user','square1');
	}else
		$name = isset($review->name) ? $review->name : null;
	
	if(Auth::user()){
		if( Auth::user()->id ==  $review->user->id){
			$dd_actions = '
				  <a class="dropdown-item p-xs p-h-xs fz-sm edit-post" href="#">Edit</a>
				  <a class="dropdown-item p-xs p-h-xs fz-sm delete-post" href="#">Delete</a>			
			';
			if( $id && $parent_id && $listing_id ){
				$encoded_data = encode_array(['module'=>$module,'is_ajax'=>1,'parent_id'=>$parent_id,'listing_id'=>$listing_id,'id'=>$id,'post_id'=>$post_id]);
				$edit_reply = '<a class="manage-reply fz-sm">			
					<input type="hidden" value="'.$encoded_data.'">
					<i class="m-l-xs fas fa-ellipsis-h fz-sm"></i>
				</a>';				
				$edit_reply .= '
					<div class="dropdown-menu dropdown-menu-sm" id="context-menu">
					  <a class="dropdown-item p-xs p-h-xs fz-sm edit-reply" href="#">Edit</a>
					  <a class="dropdown-item p-xs p-h-xs fz-sm delete-reply" href="#">Delete</a>	
					</div>					
				';

				
			}
			$encoded_post_data = encode_array(['module'=>$module,'is_ajax'=>1,'listing_id'=>$listing_id,'id'=>$id,'post_id'=>$post_id]);	
		}else{
			$dd_actions = '
				<a class="dropdown-item p-xs p-h-xs fz-sm report-post" href="#"><i class="fas fa-exclamation-circle"></i> Report Post</a>		
			';
			$encoded_post_data = encode_array(['module'=>$module,'identifier'=>3,'is_ajax'=>1,'listing_id'=>$listing_id,'review_id'=>$id,'post_id'=>$post_id]);		
		}
		$dd ='<a class="manage-post fz-sm">
			<input type="hidden" value="'.$encoded_post_data.'">
			<i class="fas fa-ellipsis-h"></i>
			</a>
			<div class="dropdown-menu dropdown-menu-sm" id="context-menu">
			  '.$dd_actions.'	
			</div>';		
	}
	$is_rating_plugin = $id ? 'data-plugin="rating"' : null;
	$timecreated = null;
	if( isset($review->timecreated) )
		$timecreated = !$parent_id ? format_time($review->timecreated,1) : get_time_ago($review->timecreated);
	$is_reply = $is_reply ? 'comment-content-reply' : null;
	$mlg = $is_reply ? null : 'm-lg';
	if( !$is_reply ){
		$optionsRating = array(
			'displayOnly'=>true,
			'size'=>'xs',
			'step'=>1,
			'starCaptions'=> [1=> 'Very Poor', 2=> 'Poor', 3=> 'Ok', 4=> 'Good', 5=> 'Very Good'],
			'starCaptionClasses'=> [1=> 'text-danger', 2=> 'text-warning', 3=> 'text-info', 4=> 'text-primary', 5=> 'text-success'],
			'emptyStar'=>'<i class="far fa-star"></i>',
			'filledStar'=>'<i class="fas fa-star"></i>',
		);		
	}
	$reviewreactionsicons = isset($review_reactions[$id]) ? $review_reactions[$id] : null;
	
	$reaction_id = $review_reactions[$id]['auth']['id'] ? $review_reactions[$id]['auth']['id'] : 0;
	$reaction_id_encr = $reaction_id ? encode_data_short($reaction_id) : 0;
	$defaultText = $review_reactions[$id]['auth']['react'] ? $review_reactions[$id]['auth']['react'] : 'Like';
	unset($reviewreactionsicons['auth']);
	
	$ajax_options = array(
		'reactions'=>$reviewreactionsicons,
		'defaultText'=>$defaultText,
		'ajax'=>array(
			'url'=>url('reviewreactions'),
			'data'=>[
				'_token'=>Session::token(),
				'encoded_data'=>encode_array(['module'=>$module,'is_ajax'=>1,'id'=>$reaction_id,'listing_id'=>$listing_id,'review_id'=>$id,'post_id'=>$post_id])
			],
		),	
	);	
	echo '
		<div class="comment" >			
			<div class="comment-author '.$mlg.'">
				<a href="#" style="background-image: url('.$pp.');"></a>
			</div><!-- /.comment-author -->

			<div class="comment-content p-xs '.$is_reply.'">	
			';
			
		if( !$is_reply ){	
			echo '	
					<div class="comment-meta">
						<div class="comment-meta-author">
							Posted by <a href="#" class="comment-name">'. $name  .'</a>
							<br>
							<span class="comment-timecreated">'. $timecreated .'</span>
						</div><!-- /.comment-meta-author -->	
						<div class="comment-meta-date fz-md p-t-0 m-r-md">
							'.$dd.'
						</div>
					</div><!-- /.comment -->
				';
		}
		
		echo '	
				<div class="comment-body p-t-xs m-0 m-h-xs">
			';
		$likeico = $is_reply ? 'no-icon' : null;
		if( !$is_reply ){
			echo '		<div class="comment-rating">		
							<input type="text"  value="'. $rating .'" '.$is_rating_plugin.' data-options="'.clean_jsonString($optionsRating).'" >
						</div><!-- /.comment-rating -->
						';
		}	
		if( !$is_reply ){
			echo '			
						<p class="comment-message">
						'. emojione($message) .'
						</p>
				</div><!-- /.comment-body -->		
				';
			
			if( Auth::user() ){
				echo '	
						<div class="comment-footer">
							<a data-plugin="facebookReactions" data-options="'.clean_jsonString($ajax_options).'" data-emoji-class="'.$defaultText.'" class="'.$likeico.'" data-reactions-type="horizontal" data-unique-id="'.$reaction_id_encr.'" data-emoji-class="">
								<span>'.$defaultText.'</span>
							</a>
						</div><!-- /.comment-footer -->
					';				
			}	
		}else{
			echo '			
						<p class="m-b-0">
							<span class="comment-text"><b class="text-primary reply-name comment-name">'.$name.'</b> <span class="comment-message">'. emojione($message) .'</span> '.$edit_reply.'</span>
						</p>
				</div><!-- /.comment-body -->		
				';	
			
			if( Auth::user() ){
				echo '	
						<div class="comment-footer '.$likeico.'">
							<a data-plugin="facebookReactions" data-options="'.clean_jsonString($ajax_options).'" data-emoji-class="'.$defaultText.'" class="'.$likeico.'" data-reactions-type="horizontal" data-unique-id="'.$reaction_id_encr.'" data-emoji-class="">
								<span>'.$defaultText.'</span>
							</a>
							<a class="m-l-xs comment-reply" href="javascript:void(0)">Reply</a><span class="m-l-xs comment-timecreated">'.$timecreated .'</span>
						</div><!-- /.comment-footer -->
					';				
			}	
			
		}	

			
		if( !$is_reply ){	
		$encoded_data = encode_array(['module'=>$module,'is_ajax'=>1,'listing_id'=>$listing_id,'parent_id'=>$id,'post_id'=>$post_id]);			
		echo '		
			<div class="comment-box">
				<input type="hidden" value="'.$encoded_data.'">
			';
			if( $id ){
				echo'	
					<textarea name="message" placeholder="Write a Comment..." rows="1"  class="m-h-xs"></textarea>
				';
			}
			echo '
			</div>	
			';	
		}	
		echo '		
			</div><!-- /.comment-content -->
		</div><!-- /.comment -->';	
}