<?php
function getAddress($data){
	if( isset($data->state) && $data->state != "" ){
		$dataAddr = getCountriesCities('PHL',$data->state);
		echo $data->address.' '.$data->brgy.' '.$data->city.', '.$dataAddr['state']['name'].' Philippines';
	}
}
function getCountriesCities($country=null,$state=null)
{
	try{
		$countrydata = Countries::where('cca3', $country)->first();
		$region = $countrydata->geo->region;
		$region_code = $countrydata->geo->region_code;
		$states = $countrydata->hydrateStates()->states->pluck('name', 'postal');
	}catch(Exception $e){
		return false;
	}
	$newData['states'] =  null;
	$newData['region'] =  null;	
	if( isset($country) && isset($states) && isset($region) ){
		
		if( isset($state) ){
			$state_selected = $countrydata->hydrateStates()->states->where('postal',$state)->first();
			$region_cod = $state_selected->extra->region_cod;
			$phl = File::get(resource_path('assets/php/'.$region_cod.'.json'));
			$data = json_decode($phl, true);		
			$newData['state']['lat'] =  $state_selected->geo->latitude;
			$newData['state']['long'] =  $state_selected->geo->longitude;			
			$newData['state']['name'] =  $state_selected->name;
			
			$cities = $countrydata->hydrate('cities')->cities->where('adm1name',$newData['state']['name'])->first();
			
			if( isset($cities->latitude) ){
				$newData['state']['lat'] =  $cities->latitude;
				$newData['state']['long'] =  $cities->longitude;	
			}		
			if( isset($data['province_list'][strtoupper($newData['state']['name'])]) ){
				$newData['towns'] = $data['province_list'][strtoupper($newData['state']['name'])];
			}else if(isset($data['province_list'])){
				foreach($data['province_list'] as $key => $mun){
					foreach( $mun['municipality_list'] as $keyMun => $dataMun )
						$newData['towns']['municipality_list'][$keyMun] = $dataMun;
				}
			}
		}
		$newData['country_name'] =  $countrydata->name->common;
		$newData['country_cca2'] =  $countrydata->cca2;
		$newData['states'] =  $states;
		$newData['region'] =  $region;
		$newData['region_code'] =  $region_code;
	}
	return $newData;
}
function getCountriesCitiesHtm($selected_data=null,$prefnameArr=null,$hide_label=false)
{

	$options = $selected = null;
	if($selected_data['options']) $options = clean_jsonString($selected_data['options']);
	$span = !$hide_label ? 9 : 12;
	$col = !$hide_label ? 4 : 12;
	echo '<div class="row address-select">';

		echo ' <div class="col-sm-12 col-md-'.$col.'" ><div class="form-group row has-feedback">';
			if( !$hide_label )
				echo '<label class="control-label col-sm-3 text-left" for="state">'.trans_choice('listings__category.state',1).'</label>';
			
			echo '<div class="col-sm-'.$span.' input-with-icon">';
				$sname = $prefnameArr != null ? $prefnameArr[1] : "country[state]";
				$rname = $prefnameArr != null ? $prefnameArr[2] : "country[region]";
				$rcodename = $prefnameArr != null ? $prefnameArr[3] : "country[region_code]";
				$data = getCountriesCities('PHL');
				if( $data['states'] ){
					echo '<select class="select-state form-control required" data-plugin="select2" data-options="'.$options.'" aria-describedby="state" name="'.$sname.'" style="width:100%"  >';	
						echo '<option value="">'.__('listings__category.choose_state').'</option>';
						foreach($data['states'] as $key => $state){
							if( isset($selected_data['state']) && $selected_data['state'] != "" ) $selected = $selected_data['state'] == $key ? 'selected="selected"' : null;
							echo '<option value="'.$key.'" '.$selected.'>'.$state.'</option>';
						}
					echo '</select>';
				}else{
					echo '<select style="display:none;width:100%" class="select-state form-control required" aria-describedby="state" data-plugin="select2" data-options="'.$options.'" name="'.$sname.'"  choose="'.__('listings__category.choose_state').'">';	
						echo '<option value="">'.__('listings__category.choose_state').'</option>';
					echo '</select>';				
				}
				echo '<input name="'.$rname.'" class="select-region" type="hidden" value="'.$data['region'].'">';
				echo '<input name="'.$rcodename.'" class="select-region-code" type="hidden" value="'.$data['region_code'].'">';
			echo '</div>'; 
		echo '</div></div>'; 
		
		$cname = $prefnameArr != null ? $prefnameArr[0] : "country[country]";
		if( !$hide_label ){
			echo ' <div class="col-sm-12 col-md-4"><div class="form-group row has-feedback">';
				if( !$hide_label )
					echo '<label class="control-label col-sm-3 text-left" for="country">'.trans_choice('listings__category.city',1).'</label>';
				
				$cityname = $prefnameArr != null ? $prefnameArr[4] : "country[city]";
				echo '<div class="col-sm-'.$span.' input-with-icon">';
					echo '<select class="select-city form-control required" data-plugin="select2" data-options="'.$options.'" aria-describedby="city" name="'.$cityname.'"  style="width:100%" choose="'.__('listings__category.choose_city').'">';
						echo '<option value="">'.__('listings__category.choose_city').'</option>';
						$brgys = null;
						if( isset($selected_data['state']) && $selected_data['state'] != "" ){
							$data = getCountriesCities('PHL',$selected_data['state']);
							
							if( isset($data['towns']) ){
								foreach( $data['towns']['municipality_list'] as $town => $brgysdata ){
									if( isset($selected_data['city']) && $selected_data['city'] == $town ){
										$selected = 'selected="selected"';
										$brgys = $brgysdata['barangay_list'];
									}else
										$selected = null;	
									echo '<option value="'.$town.'" '.$selected.' data-brgy="'.clean_jsonString($brgysdata['barangay_list']).'">'.$town.'</option>';
								}
							}
						}
					echo '</select>';
				echo '</div>'; 
			echo '</div></div>'; 

			echo ' <div class="col-sm-12 col-md-4"><div class="form-group row has-feedback">';
				if( !$hide_label )
					echo '<label class="control-label col-sm-3 text-left" for="brgy">'.trans_choice('listings__category.brgy',1).'</label>';
				
				$brgyname = $prefnameArr != null ? $prefnameArr[4] : "country[brgy]";
				echo '<div class="col-sm-'.$span.' input-with-icon">';
					echo '<select class="select-brgy form-control required" data-plugin="select2" data-options="'.$options.'" aria-describedby="brgy" name="'.$brgyname.'"  style="width:100%" choose="'.__('listings__category.choose_brgy').'">';
						echo '<option value="">'.__('listings__category.choose_brgy').'</option>';
						
						if( isset($selected_data['city']) && $selected_data['city'] != "" ){
							if( isset($brgys) ){
								foreach( $brgys as $brgy ){
									if( isset($selected_data['brgy'])  ) $selected = $selected_data['brgy'] == $brgy ? 'selected="selected"' : null;
									echo '<option value="'.$brgy.'" '.$selected.' >'.$brgy.'</option>';
								}
							}
						}
					echo '</select>';
				echo '</div>'; 
			echo '</div></div>'; 
		}
		echo '<input name="'.$cname.'" type="hidden" value="PHL">';

		
	
	echo '</div>';
}	