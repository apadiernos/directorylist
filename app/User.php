<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username','first_name','last_name', 'email', 'password','is_user_advertiser','advertiser_verifcation_duration'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

	public function profilepicture() {
		$record = $this->belongsTo('App\model\listings\Attachment','id','module_id')->where('module','userpicture');
		return $record;
	}
	public function reviews() {
		$record = $this->hasMany('App\model\listings\Review','user_id','id');
		return $record;
	}
	public function review_reactions() {
		$record = $this->hasMany('App\model\listings\ReviewReaction','user_id','id');
		return $record;
	}
	public function address() {
		$record = $this->hasOne('App\model\auth\UserAddress','user_id','id');
		return $record;
	}	
	public function list_likes() {
		$record = $this->hasMany('App\model\listings\UserListLike','user_id','id');
		return $record;
	}	
	public function list_views() {
		$record = $this->hasMany('App\model\listings\UserListView','user_id','id');
		return $record;
	}		
	public function list_reports() {
		$record = $this->hasMany('App\model\listings\UserListReport','user_id','id');
		return $record;
	}	
	public function lists() {
		$record = $this->hasMany('App\model\listings\Listing','user_id','id');
		return $record;
	}		
	public function review_reports() {
		$record = $this->hasMany('App\model\listings\UserReviewReport','user_id','id');
		return $record;
	}	
	
	protected static function boot() 
    {
      parent::boot();
      static::deleting(function($users) {
         $users->reviews()->delete();
         $users->review_reactions()->delete();
		 $users->profilepicture()->delete();	
		 $users->address()->delete();	
      });
    }
}
