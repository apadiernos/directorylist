<?php
namespace App\services;
use App\model\auth\SocialAccount;
use App\User;
use Laravel\Socialite\Contracts\User as ProviderUser;
use App\Http\Controllers\Listings\AttachmentController as AttachmentLibrary;

class SocialAccountService
{
    public function createOrGetUser(ProviderUser $providerUser,$provider)
    {
		$account = SocialAccount::whereProvider($provider)
            ->whereProviderUserId($providerUser->getId())
            ->first();	
		if ($account) {
			return $account->user;
		} else {
			$account = new SocialAccount([
							'provider_user_id' => $providerUser->getId(),
							'provider' => $provider
						]);						
			$user = User::whereEmail($providerUser->getEmail())->first();				
			if (!$user) {
				$name = strtolower(str_replace(' ','.',$providerUser->getName()));
				switch($provider){
				   case 'facebook':
					  $first_name = $providerUser->offsetGet('first_name');
					  $last_name = $providerUser->offsetGet('last_name');
					  $username = strtolower($first_name.'.'.$last_name);
					  break;

				   case 'google':
					  $first_name = $providerUser->offsetGet('given_name');
					  $last_name = $providerUser->offsetGet('family_name');
					  $username = strtolower($first_name.'.'.$last_name);
					  break;
				   default:
					  $first_name = $providerUser->getName();
					  $last_name = $providerUser->getName();
					  $username = $name;
				}
				
				$user = User::create([
									'username'=>$username,
									'first_name'=>$first_name,
									'last_name'=>$last_name,
									'email' => $providerUser->getEmail(),
									'password' => md5(rand(1,10000)),
								]);				
				$request = new \Illuminate\Http\Request();
				$request->setMethod('POST');
				$request->request->add(
					[
						'module_id' => $user->id,
						'user_id' => $user->id,
						'filedirectory' => 'user',
						'module' => 'userpicture',
						'is_url' => $providerUser->getAvatar(),
						'filename' => $name,
					]
				);				
				AttachmentLibrary::saveRecords($request,1);					
				$account->user()->associate($user);
				$account->save();
			}
			return $user;
        }
    }
}