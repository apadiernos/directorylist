<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Listings\ListingCategoryController as ListingCategoryLibrary;
use App\Http\Controllers\Listings\ListingController as ListingLibrary;

class Dashboard extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
		$this->middleware('auth:adminuser');
    }
	
	public function showAdminDashboard(Request $request)
    {
		$params = array(
			'additional_dttables_search'=>null,
			'categories'=>app(ListingCategoryLibrary::class)->count(),
			'listings'=>app(ListingLibrary::class)->count(),
		);		
		return view('admin/pages/dashboard',$params);
    }


}
