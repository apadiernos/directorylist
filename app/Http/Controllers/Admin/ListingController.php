<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Listings\ListingController as ListingLibrary;
use App\Http\Controllers\Listings\AttachmentController as AttachmentLibrary;
use Awps;
use Illuminate\Support\Facades\Crypt;
use App\model\listings\Listing as Listing;
use Countries;

class ListingController extends Controller
{
    protected $listingCategory;
    protected $listingCount;
	public function __construct()
    {
		$this->middleware('auth:adminuser');
		$this->listingCategory = app(\App\Http\Controllers\Listings\ListingCategoryController::class)->index();
    }	
	//views and posts
	public function showListing(Request $request)
    {
		
		$additional_dttables_search[] = array(
				'element' => 'link',
				'attributes' => array(
					'id'=>'new-category',
					'class'=>'new-category btn btn-primary m-xs',
					'href'=>url('admin/listings/add'),
				),		
				'value'=>'<i class="md-icon">add</i> '.__('listings__category.add_new_listing'),
			);
		
		$params['additional_dttables_search'] = clean_jsonString(json_encode($additional_dttables_search));	
		return view('admin/pages/list__lists',$params);
    }
	public function showAddListing(Request $request)
    {
		$icons = new Awps\FontAwesome();
		$icons->getArray();					
		$params['fa'] = $icons->getArray();
		$params['get_action'] = 1;	
		$listing = new Listing();
		$params['record'] = (object) array_fill_keys($listing->getFillable(),null);
		$params['record']->id = 0;
		$params['listingCategory'] = $this->listingCategory;
		return view('admin/pages/list__add',$params);
    }
	public function showEditListing(Request $request)
    {
		$id = $request->segment(3);

		$icons = new Awps\FontAwesome();
		$icons->getArray();					
		$params['fa'] = $icons->getArray();		
		$params['get_action'] = 2;	
		$params['record'] = app(ListingLibrary::class)->show($id)->first();	

		if( $params['record']->sections ) $params['record']->sections = unserialize(Crypt::decryptString($params['record']->sections));
		if( $params['record']->socialmedia ) $params['record']->socialmedia = unserialize(Crypt::decryptString($params['record']->socialmedia));
		$params['listingCategory'] = $this->listingCategory;	
		return view('admin/pages/list__add',$params);
    }
	
	public function getTableRecords(Request $request)
    {
		$data = $this->get_params();
		$count = $data['count'] ? $data['count'] : 0;
		$records = $data['records'] ? $data['records'] : array();
		
		$dtData = new \stdClass();
		$dtData->offset = $request['draw'] ? $request['draw'] : 0;
		$dtData->recordsTotal = $count;
		$dtData->recordsFiltered = $count;		

		$dtkey = 0;
		$dataForDttables = array();	
		foreach($records as $key => $record){
			$actions = '
				<a href="'.url('admin/listings/'.$record->id).'" class="btn btn-primary"><i class="md-icon">edit</i> '.__('listings__category.edit').'</a>
				<a href="#" data-options="'.clean_jsonString($record).'" class="btn btn-secondary btn-delete"><i class="md-icon">delete</i> '.__('listings__category.remove').'</a>			
			';
			
			$dataForDttables[$dtkey] = 
					array(
						$record->title,
						$record->website,
						$record->category,
						$record->created_by,
						$actions,
					);
			$dtkey++;	
		}
		$dtData->dataList = $dataForDttables;
		$dtPrepared = prepare_datatable_results($dtData);
		echo $dtPrepared;			
    } 
	//query parameters
	private function get_params()
	{
		return array(
			'records'=>app(ListingLibrary::class)->index(),
			'count'=>app(ListingLibrary::class)->count(),
		);
	}	
	
	
	public function saveRecords(Request $request)
    {

		$data = $request->all();
		$title = preg_replace( '/[\W]/', '', $data['title']);
		$slug_term = strtolower(trim(str_replace(' ','-',$title)));		
		$request->request->add(
			[
				'country' => $data['country']['country'],
				'state' => $data['country']['state'],
				'region' => $data['country']['region'],
				'region_code' => $data['country']['region_code'],
				'city' => $data['country']['city'],
				'brgy' => $data['country']['brgy'],
				'slug_term' => $slug_term,
				
				'socialmedia' => Crypt::encryptString(serialize($data['socialmedia'])),
			]
		);	
		if( isset($data['sections']) ){
			$request->request->add(['sections' => Crypt::encryptString(serialize($data['sections']))]);
		}
		$userLoggedInId = $request->user()->id;	
		$id = $request->input('id');
			
		
		if( $id ){
			$request->request->add(['updated_by' => $userLoggedInId]);	
			$data = app(ListingLibrary::class)->update($request,$id);
		}else{
			$request->request->add(['created_by' => $userLoggedInId]);
			$data = app(ListingLibrary::class)->store($request);
		}

		return redirect('admin/listings/'.$data->id);
    } 
	

	public function deleteRecords(Request $request)
    {
		$id = $request->segment(2);
		echo app(ListingLibrary::class)->delete($request,$id);
		$request->request->add(
			[
				'module_id' => $id,
				'module' => 'listings'
			]
		);		
		$data =  app(AttachmentLibrary::class)->deleteRecords($request);
    } 		
}

