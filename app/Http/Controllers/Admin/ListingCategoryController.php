<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Listings\ListingCategoryController as ListingCategoryLibrary;
use Awps;

class ListingCategoryController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
		$this->middleware('auth:adminuser');
    }
	
	//views and posts
	public function showListingCategory(Request $request)
    {
		$icons = new Awps\FontAwesome();
		$icons->getArray();					
		$params['fa'] = $icons->getArray();		
		$additional_dttables_search[] = array(
				'element' => 'button',
				'attributes' => array(
					'id'=>'new-category',
					'class'=>'new-category btn btn-primary m-xs',
				),		
				'value'=>'<i class="md-icon">add</i> '.__('listings__category.add_new_category'),
			);
		
		$params['additional_dttables_search'] = clean_jsonString(json_encode($additional_dttables_search));	
		return view('admin/pages/list__categories',$params);
    } 
	public function getTableRecords(Request $request)
    {
		$data = $this->get_params();
		$count = $data['count'] ? $data['count'] : 0;
		$records = $data['records'] ? $data['records'] : array();
		
		$dtData = new \stdClass();
		$dtData->offset = $request['draw'] ? $request['draw'] : 0;
		$dtData->recordsTotal = $count;
		$dtData->recordsFiltered = $count;		

		$dtkey = 0;
		$dataForDttables = array();	
		foreach($records as $key => $record){
			$actions = '
				<a href="#" data-options="'.clean_jsonString($record).'" class="btn btn-primary btn-update"><i class="md-icon">edit</i> '.__('listings__category.edit').'</a>
				<a href="#" data-options="'.clean_jsonString($record).'" class="btn btn-secondary btn-delete"><i class="md-icon">delete</i> '.__('listings__category.remove').'</a>			
			';
			
			$dataForDttables[$dtkey] = 
					array(
						$record->category,
						$record->created_by,
						$actions,
					);
			$dtkey++;	
		}
		$dtData->dataList = $dataForDttables;
		$dtPrepared = prepare_datatable_results($dtData);
		echo $dtPrepared;			
    } 
		//query parameters
	private function get_params()
	{
		return array(
			'records'=>app(ListingCategoryLibrary::class)->index(),
			'count'=>app(ListingCategoryLibrary::class)->count(),
		);
	}
	
	public function saveRecords(Request $request)
    {
		$userLoggedInId = $request->user()->id;	
		$id = $request->input('id');
		$category = $request->input('category');
		$title = preg_replace( '/[\W]/', '', $category);
		$slug_term = strtolower(trim(str_replace(' ','-',$title)));			
		$request->request->add(['slug_term' =>$slug_term]);		
		if( $id ){
			$request->request->add(['updated_by' => $userLoggedInId]);	
			echo json_encode(app(ListingCategoryLibrary::class)->update($request,$id));
		}else{
			$request->request->add(['created_by' => $userLoggedInId]);
			echo json_encode(app(ListingCategoryLibrary::class)->store($request));
		}
    } 
	public function deleteRecords(Request $request)
    {
		$id = $request->segment(3);
		echo app(ListingCategoryLibrary::class)->delete($request,$id);
    } 		
}
