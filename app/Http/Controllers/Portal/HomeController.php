<?php

namespace App\Http\Controllers\Portal;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Listings\ListingController as ListingLibrary;
use App\Http\Controllers\Listings\ListingCategoryController as ListingCategoryLibrary;


use App\model\listings\Listing as ListingModel;

class HomeController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
		$this->middleware(['web','guest']);
    }
	
	public function showHomePage(Request $request)
    {
		
		$params['recordsCat'] = $params['recordsLoc'] = $params['records'] = array();
		$params['recordsLoc'] = app(ListingLibrary::class)->index(['listings.status'=>1],['offset'=>0,'limit'=>10]);
		$params['records'] = app(ListingLibrary::class)->index(['listings.status'=>1],['offset'=>0,'limit'=>6]);
		$params['recordsCat'] = app(ListingCategoryLibrary::class)->index();
		return view('portal/pages/home',$params);
    } 	
		
	public function homesearch(Request $request)
    {
		$data = $request->all();
		if( $data['country']['state'] ){
			$request->request->add(
				[
					'country' => $data['country']['country'],
					'state' => $data['country']['state'],
				]
			);			
			$location = app(ListingLibrary::class)->geoCodeAddress($request);
			echo json_encode($location);
		}		
    } 	
	public function listsearch(Request $request)
    {
		$data = $request->all();
		$q = array();
		if( $data['country']['state'] ){
			$request->request->add(
				[
					'state' => $data['country']['state'],
				]
			);				
			$location = app(ListingLibrary::class)->geoCodeAddress($request);
			$q = array(
				'state'=>$data['country']['state'],
				'lat'=>$location['lat'],
				'long'=>$location['long'],
				'distance'=>$data['distance'],
				'status'=>1,
			);		
		}	
		$params['records'] = array();
		$params['records'] = app(ListingLibrary::class)->index($q,['offset'=>$request->input('page'),'limit'=>6]);	
		if( count($params['records']) == 0 )
			echo '<h4 class="text-center no-more">No results found.</h4>';
		echo view('portal/pages/list__ajax',$params);	
    } 
	
	public function showListingLists(Request $request)
    {
		$q = array();
		$params['category_id'] = 0;
		$params['records'] = array();
		$params['records'] = app(ListingLibrary::class)->index($q,['offset'=>1,'limit'=>6]);
		if( !count($params['records']) )
			redirect(url('/lists'));
		return view('portal/pages/list',$params);
    } 
	
	public function showListingListByCategory($category)
    {
		$q = array();
		if($category){
			$q = array(
				'listing_categories.slug_term'=>$category,
				'listings.status'=>1,
			);				
		}
		$params['records'] = array();
		$params['records'] = app(ListingLibrary::class)->index($q,['offset'=>0,'limit'=>6]);
		if( !count($params['records']) ){
			$params['category_id'] = 0;
			redirect(url('/lists'));
		}else
			$params['category_id'] = $params['records'][0]->category_id;
		return view('portal/pages/list',$params);
    } 

    public function verifyUserAdvertiser($encodeddata)
    {
		$data_encrypted = decode_array($encodeddata);

		if( $data_encrypted->id && $data_encrypted->email ){
			$user = User::findOrfail($data_encrypted->id);
			if( $user->email != $data_encrypted->email || strtotime('+1 day',$user->advertiser_verifcation_duration) > time() || ( $user->is_user_advertiser != 1 && $user->is_user_advertiser != 2  ) )
				abort(405);
			else{
				if( $user->is_user_advertiser == 1 ){
					$data = $user->toArray();
					$data['is_user_advertiser'] = 2;
					$user->update($data);
				}			
				auth()->login($user);
				return redirect()->to('/ads/onboarding');
			}
		}else
			abort(405);
	  	
    }
}
