<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Portal\Ads\AdSingleController;
use App\Http\Controllers\Portal\ProfileController;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ManageAdvertiserController extends Controller
{
    public static $pages;
    public function __construct()
    {
		$this->middleware('web');
    }

	public function showManageAdvertiserPage($term)
    {
		$params = array();
		$params['fullname'] = get_fullname(Auth::user());
		$params['user'] = User::findOrfail(Auth::user()->id);

		$request = request();
		if( $request->get('encoded_data') )
			$data_encrypted = decode_array($request->get('encoded_data'));

		if( $params['user']->is_user_advertiser != 2 )
			abort(405);

		$pages = self::get_pages();
		if( is_int($term) && $pages[$term] )
			$term = $pages[$term];	
		
		$breadcrumbs = [
			$params['fullname']=>'/profile/user',
			'Shop'=>'/ads/shop'
		
		];
		switch ($term) {
		    case 'onboarding':
				$breadcrumbs['Help & Support'] = '/ads/userguide';
		    	$params['page'] = 'ads/userguide';
		    	$params['identifier'] = 6;
		    	break;	
		    case 'shop':
				$breadcrumbs['My Shop'] = '/ads/shop';
		    	$params['page'] = 'lists';
		    	$params['identifier'] = 4;
		    	$params['records'] = ProfileController::get_list_records(4); 
		    	break;	
		    case 'single':
				$breadcrumbs['Posting Ad'] = '/ads/single';
				$params['identifier'] = 7;
				$page_params = AdSingleController::get_ad_single_page();
		    	break;				
		    default:
		        abort(404);
		}
		
		if( $page_params )
			$params = array_merge($params,$page_params);
		
		$params['breadcrumbs'] = $breadcrumbs;
		if( $data_encrypted->is_ajax )
			return view('portal/pages/profile/'.$params['page'],$params);
		else
			return view('portal/pages/profile',$params);
    } 
    private function get_pages()
    {
		return self::$pages = [
			6=>'onboarding',
		]; 
 	}

}
