<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Portal\ManageAdvertiserController;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class ProfileController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
		$this->middleware('web');
    }

    public function registerUserAdAdvertiser(Request $request)
    {
		$fullname = get_fullname(Auth::user());
		$user = User::findOrfail(Auth::user()->id);

		if( !$user->email )	
			return;
		$data = $user->toArray();
		$data['is_user_advertiser'] = 1;
		$data['advertiser_verifcation_duration'] = time();
		$user->update($data);

		$url = url('/verifyadvertiser/'.encode_array(['id'=>$user->id,'email'=>$user->email]));
		$from_name = 'Karnid No Reply';

		$subject = 'Welcome to Karnid Business Owner Club!';
		$data = array('name'=>$fullname,"link"=>$url,'duration'=>strtotime('+1 day',$data['advertiser_verifcation_duration']));
		try{
			Mail::send('portal.emails.userverify', $data, function($message) use ($from_name, $from_email, $subject, $user) {
				$message->to($user->email, $fullname)
						->cc(['payumoje@gmail.com'])
						->subject($subject);
				$message->from(env('MAIL_NOREPLY'),$from_name);
			});
			echo 1;
		}catch(\Exception $e){
		    exit;
		}		    	
    }

	public function showProfilePage($term)
    {
		$params = array();
		$params['page'] = 'lists';
		switch ($term) {
		    case 'user':
		    	$params['page'] = 'info';
		    	$params['identifier'] = 0;
		    	break;
		    case 'likes':
		    	$params['records'] = self::get_list_records(1);  
		    	$params['identifier'] = 1;
		    	break;
		    case 'dislikes':
		    	$params['records'] = self::get_list_records(2);  
		    	$params['identifier'] = 2;
		    	break;	
		    case 'recent':
		    	$params['records'] = self::get_list_records(3);  
		    	$params['identifier'] = 3;
		    	break;			    	
		    case 'join':
				$params = self::get_register_user_as_advertiser_page_params();
				$params['identifier'] = 5;
		    	$params['page'] = 'advertiser';
		    	break;					
		    default:
		        abort(405);
		}
		$params['fullname'] = get_fullname(Auth::user());
		$params['user'] = User::findOrfail(Auth::user()->id);		
		return view('portal/pages/profile',$params);
    }	

   	public function showProfilePageContents(Request $request)
   	{
 		$user_id = Auth::user() ? Auth::user()->id : 0;
		$data_encrypted = decode_array($request->get('encoded_data'));
		$page = $request->get('page') ? $request->get('page') : 1;	
		if( isset($data_encrypted->identifier) ){
			switch($data_encrypted->identifier){
			    case 0:
			    	$content = self::get_user_profile_page();
			    	break;
			    case 5:
			    	$content = self::get_register_user_as_advertiser_page();
			    	break;
			    default:
			    	if( $data_encrypted->identifier >= 6 )
			    		$content = ManageAdvertiserController::showManageAdvertiserPage($data_encrypted->identifier);
			    	else	
			        	$content = self::get_lists($data_encrypted->identifier,$page);				    			    	
			}

			
			echo $content;
		}		
   	}
	
	private function get_register_user_as_advertiser_page_params()
	{
		$params = array();
		$params['user'] = User::findOrfail(Auth::user()->id);
		if( $params['user']->is_user_advertiser == 1  ){
			$params['user']->advertiser_verifcation_duration = strtotime('+1 day',$params['user']->advertiser_verifcation_duration);
			if( $params['user']->advertiser_verifcation_duration < time() )
				$params['expired'] = $params['user']->advertiser_verifcation_duration;
			else
				$params['togo'] = $params['user']->advertiser_verifcation_duration;
		}else if( $params['user']->is_user_advertiser == 2  ){
			return redirect()->to('/ads/onboarding');
		}	
		return $params;
	}
	private function get_register_user_as_advertiser_page()
    {

		$params = self::get_register_user_as_advertiser_page_params();
		return view('portal/pages/profile/advertiser',$params);
    }  

	private function get_user_profile_page()
    {
		$params = array();
		$params['user'] = User::findOrfail(Auth::user()->id);		
		return view('portal/pages/profile/info',$params);
    }  

	private function get_lists($identifier,$page=1)
    {
		$params = array();
		$params['records'] = self::get_list_records($identifier,$page=1); 
		$params['identifier'] = $identifier;
		if( count($params['records']) == 0 ){
			echo '<h4 class="text-center no-more">No results found.</h4>';
			echo '
					<div class="center m-b-md">
						<a href="#" style="width: 100%" class="btn btn-primary btn-medium load-more btn-list">Go Back</a>
					</div><!-- /.center -->
			';			
		}
		else 		
			return view('portal/pages/profile/lists',$params);
    }  

    public function get_list_records($identifier,$page=1)
    {
  		$params = array();
		$params['user'] = User::findOrfail(Auth::user()->id);  
		switch ($identifier) {
		    case 1:
		    	$records = $params['user']->list_likes->forPage($page,12);
		    	break;
		    case 2:
		    	$records = $params['user']->list_reports->forPage($page,12);
		    	break;
		    case 3:
		    	$records = $params['user']->list_views->forPage($page,12);
		    	break;	
		    case 4:
		    	$records = $params['user']->lists->forPage($page,12);	    	
		    	break;		    		    	
		    default:
		    	return;	
		}   
		return $records;    	
    }
}
