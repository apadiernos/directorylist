<?php

namespace App\Http\Controllers\Portal\Ads;

use App\Http\Controllers\Controller;
use App\User;
use App\model\listings\ListingCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

class AdSingleController extends Controller
{
    public static $common_params;
	public function __construct()
    {
		$this->middleware('web');		
    }

    public function get_ad_single_page()
    {
    	$params = array();
	    $params['categories'] = ListingCategory::all();
	    $params['ad_title'] = 'Basic Information';
	    $params['page'] = 'ads/shop/single';
		$params['ad_page'] = 'step1';
    	$params['js'] = ['/ads/manage-single.js','/ads/manage-form.js'];  
    	return $params;	
    }

    public function showSingleAdPage($id)
    {
        $params = self::get_ad_single_page();         
		$params = array_merge($params,self::get_common_params($id));	
		$params['ad_title'] =  $params['record'] ? 'Update '.$params['record']->title : 'Basic Information';
		$params['identifier'] = 8; 
		
        return view('portal/pages/profile',$params);
    }
	
    public function showSingleAdSectionsPage($id)
    {
		$params = self::get_common_params($id);	
        $params['identifier'] = 8;  
	    $params['page'] = 'ads/shop/single';
	    $params['ad_page'] = 'sections';
		$params['ad_title'] = 'Manage '.$params['record']->title.' Posts';
    	$params['js'] = ['/ads/manage-single.js','/ads/manage-section.js']; 
		
        return view('portal/pages/profile',$params);
    }	
    private function get_common_params($id)
    {
		$user = User::findOrfail(Auth::user()->id);
		$record = $user->lists()->findOrFail($id);
		if( $record->socialmedia ) $record->socialmedia = unserialize(Crypt::decryptString($record->socialmedia));			
		else $record->socialmedia = array();	
		return self::$common_params = [
			'user'=>$user,
			'fullname'=>get_fullname(Auth::user()),
			'record'=>$record,
		]; 
 	}    
}
