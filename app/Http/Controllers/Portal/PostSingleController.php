<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Listings\ListingCategoryController as ListingCategoryLibrary;
use App\Http\Controllers\Listings\ListingController as ListingLibrary;
use App\Http\Controllers\Listings\ReviewController as ReviewLibrary;
use App\Http\Controllers\Listings\ReviewReactionController as ReviewReactionLibrary;
use App\Http\Controllers\Listings\UserListLikeController;
use App\Http\Controllers\Listings\UserListReportController;
use App\Http\Controllers\Listings\UserListViewController;
use App\Http\Controllers\Listings\UserReviewReportController;
use App\model\listings\Listing as ListingModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Share;

class PostSingleController extends Controller
{
    public function __construct()
    {
		$this->middleware(['web','guest']);
    }

	public function showListingSingle($category, $title, $post=null)
    {	
		$q = array(
			'listings.slug_term'=>$title,
			'listing_categories.slug_term'=>$category,
			'listings.status'=>1,
		);		
		$params['record'] = array();
		$params['is_single'] = 1;
		$record = app(ListingLibrary::class)->index($q)->first();
		if( !$record )
			abort(404);
		$params['new_listings'] = ListingModel::forPage(0,3)->get();
		if($record){
			$params['record'] = ListingModel::findOrfail($record->id);
			if($post){
				$post_record = $params['record']->posts->where('slug_term',$post)->first();
				$params['module'] = 'post-'.$post_record->id;
				$params['post'] = $post_record;
				$params['identifier'] = $post_record->id;
				if( !$post_record->subposts )
					abort(404);
			}			
			//reviews
			$reviewRes = array();
			if( !$params['module'] && !$params['post'] ){
				$params['module'] = 'main-'.$params['record']->id;
				$params['identifier'] = 0;
			}
			$reviewRes = self::get_reviews_tree($params['module'],$params['record']->id);
			$params['reviews'] = $reviewRes['records'];
			$params['currentreviewcount'] = $reviewRes['current_count'];	
			$params['reviews_avg'] = $reviewRes['avg'];
			$params['reviews_count'] = $reviewRes['count'];
			$params['reviewreactions'] = $reviewRes['reviewreactions'];
			$params['recordcat'] = $record;
			

			if( $params['record']->sections ) $params['record']->sections = unserialize(Crypt::decryptString($params['record']->sections));
			else $params['record']->sections = array();
			if( $params['record']->socialmedia ) $params['record']->socialmedia = unserialize(Crypt::decryptString($params['record']->socialmedia));			
			else $params['record']->socialmedia = array();
			$countrydata = getCountriesCities($params['record']->country,$params['record']->state);
			$params['record']->address = ucwords($params['record']->address.' '.$params['record']->brgy.' '.$params['record']->city.' '.$countrydata['state']['name'].' '.$countrydata['country_name']);

			if( Auth::user() ){
				//add to view
				$request = new Request();
				$request->setMethod('POST');
				$request->request->add(['listing_id' => $record->id]);	
				UserListViewController::saveRecords($request);
							
				$params['is_liked'] = Auth::user()->list_likes->where('listing_id',$record->id)->first() ? 'icon-hover' : false;
				$params['is_disliked'] = Auth::user()->list_reports->where('listing_id',$record->id)->first() ? 'icon-hover' : false;
			}

			return view('portal/pages/list__sinlge',$params);
		}else{
			abort(404);
		}
	}

	private function get_reviews_tree($module,$listing_id,$offset=0)
	{
		$reviewRes = array();
		$reviews = app(ReviewLibrary::class)->index(['listing_id'=>$listing_id,'parent_id'=>0,'module'=>$module],['offset'=>$offset,'limit'=>3],[['column'=>'timecreated','sorted'=>'DESC'],['column'=>'parent_id','sorted'=>'DESC']]);	
		$reviewids = array_column($reviews->toArray(),'id','id');
		$paramReviewChild['parent_id']  = array(
			'value'=>$reviewids,
			'method'=>'whereIn',				
		);			
		$reviewchildren = app(ReviewLibrary::class)->index($paramReviewChild,null,[['column'=>'timecreated','sorted'=>'DESC'],['column'=>'parent_id','sorted'=>'DESC']]);
		$currentreviewcnt = 0;
		foreach( $reviews as $review ){
			if( !$review->parent_id ){
				$reviewRes[$review->id]['parent'] = $review;
				$reviewIdx[] = $review->id;
				$currentreviewcnt++;
			}	
			foreach( $reviewchildren as $reviewchild ){
				if( $reviewchild->parent_id == $review->id ){
					$reviewRes[$review->id]['children'][$reviewchild->id] = $reviewchild;
					$reviewIdx[] = $reviewchild->id;
					$currentreviewcnt++;
				}
			}
		}
		$avg = $offset == 0 ? round(app(ReviewLibrary::class)->avg(['listing_id'=>$params['record']->id],'rating'),1) : 0;
		$cnt = $offset == 0 ? app(ReviewLibrary::class)->count(['listing_id'=>$listing_id,'module'=>$module]) : 0;
		
		//reviewreactions
		if( $reviewIdx ){
			$reviewreactionRes = array();
			$paramReviewReactions['review_id']  = array(
				'value'=>$reviewIdx,
				'method'=>'whereIn',				
			);				
			$reviewreactions = app(ReviewReactionLibrary::class)->distinctreviewreactions($paramReviewReactions);
			foreach($reviewreactions as $reviewreaction){
				if( !isset($reviewreactionRes[$reviewreaction->review_id]['total']) )
					$reviewreactionRes[$reviewreaction->review_id]['total'] = 0;
				$reviewreactionRes[$reviewreaction->review_id]['reacts'][] = $reviewreaction->reaction;
				$reviewreactionRes[$reviewreaction->review_id]['total'] = $reviewreaction->total + $reviewreactionRes[$reviewreaction->review_id]['total'];
			}
		
			if( Auth::user() ){
				$paramReviewReactions['user_id']  = Auth::user()->id;				
				$reviewreactionsbyuser = app(ReviewReactionLibrary::class)->index($paramReviewReactions);
				foreach($reviewreactionsbyuser as $reviewreactionbyuser){
					if( $reviewreactionRes[$reviewreactionbyuser->review_id] )
						$reviewreactionRes[$reviewreactionbyuser->review_id]['auth'] = ['id'=>$reviewreactionbyuser->id,'react'=>$reviewreactionbyuser->reaction];
				}
			}
		}
			
		return [
			'records'=>$reviewRes,
			'current_count'=>$currentreviewcnt,
			'count'=>$cnt,
			'avg'=>$avg,
			'reviewreactions'=>$reviewreactionRes,
		];	
	}
	
	public function commentssearch(Request $request)
    {	
		
		
		$data = $request->all();
		$data_encrypted = decode_array($data['data_params']);
		if( isset($data_encrypted->listing_id) ){
			
			$reviewRes = array();
			$params['module'] = $data_encrypted->post_id ? 'post-'.$data_encrypted->post_id : 'main-'.$data_encrypted->listing_id;
			$reviewRes = self::get_reviews_tree($params['module'],$data_encrypted->listing_id,$data['offset']);
			$params['reviews'] = $reviewRes['records'];
			$params['currentreviewcount'] = $reviewRes['current_count'];	
			$params['reviewreactions'] = $reviewRes['reviewreactions'];
			$params['offset'] = $data['offset'];
			

			if( count($params['reviews']) )
				echo view('portal/pages/comments__ajax',$params);	
		}
    }
	
	public function emaillist(Request $request)
    {
		$data_encrypted = decode_array($request->get('encoded_data'));
		if( Auth::user() ){
			$request->request->add(
				[
					'full_name' => get_fullname(Auth::user()),
					'email' => Auth::user()->email,
				]
			);			
		}
		$request->request->add(
			[
				'listing_id' => $data_encrypted->id,
			]
		);				
		
		$data = $request->all();
		$errors = array();
		$validator = self::validatoremailinquiry($data);
		if ($validator->fails())
			$errors = $validator->getMessageBag()->toArray();
		$listing = ListingModel::find($data_encrypted->id);
		if( !count($listing ) )
			$errors['no_list'] = 'No record found';	
		
		
		if( $data_encrypted->is_ajax && $errors ){
			echo json_encode(array(
				'success' => false,
				'errors' => $errors

			));
			return;
		}		
		$url = url('lists/'.$data_encrypted->url.'/'.$listing->slug_term);
		$from_name = $data['full_name'];
		$from_email = $data['email'];
		$subject = $data['subject'];
		$data = array('name'=>'Karnid Admin User', "body" => $data['message'],"post"=>$listing->title,"link"=>$url);

		Mail::send('portal.emails.listing', $data, function($message) use ($from_name, $from_email, $subject) {
			$message->to(env('MAIL_USERNAME'), 'Alan Padiernos')
					->cc(['payumoje@gmail.com'])
					->subject($subject);
			$message->from($from_email,$from_name);
		});
		
		if( $data_encrypted->is_ajax ){
			echo json_encode(array(
				'success' => true,
				'errors' => $errors

			));			
		}
    } 
    protected function validatoremailinquiry(array $data)
    {
        
		if( Auth::user() ){
			$rules = [
				'message' => ['required', 'string', 'max:255'],
				'listing_id' => ['required', 'integer'],
				'subject' => ['required', 'string', 'max:255'],
			];			
		}else{
			$rules = [
				'message' => ['required', 'string', 'max:255'],
				'full_name' => ['required', 'string', 'max:255'],
				'subject' => ['required', 'string', 'max:255'],
				'listing_id' => ['required', 'integer'],
				'email' => ['required', 'string', 'email', 'max:255'],
				'captcha' => 'required|captcha',
			];
		}
		return Validator::make($data, $rules);
    }

    public function saveListReaction(Request $request){
		$data_encrypted = decode_array($request->get('encoded_data'));	
		if( isset($data_encrypted->identifier) ){
			switch ($data_encrypted->identifier) {
			    case 1:
			    	UserListLikeController::saveRecords($request);
			    	break;
			     case 2:
			        UserListReportController::saveRecords($request);
			        break;
			     case 3:
			        UserReviewReportController::saveRecords($request);
			        break;			        
			}
		}    	
    }
    
}
