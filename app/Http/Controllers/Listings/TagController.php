<?php

namespace App\Http\Controllers\Listings;

use App\Http\Controllers\Controller;
use App\model\listings\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class TagController extends Controller
{
	public function getRecordsByTerm(Request $request)
    {
		$data = $request->all();

		$count = Tag::where('tag', 'like', '%' . $data['tag'] . '%')->count();
		$records = Tag::where('tag', 'like', '%' . $data['tag'] . '%')->forPage($data['page'],10)->get();
		if( $data['encoded_data'] ){
			$data_encrypted = decode_array($data['encoded_data']);
			if( $data_encrypted->is_ajax ){
				if( $records ){
					$tagrecords = array();
					foreach($records as $key => $record){
						$tagrecords[$key] = array(
							'id'=>$record->id,
							'text'=>$record->tag,
						);
					}	
				}		
				echo json_encode(array(
					'success' => $records ? true : false,
					'records'=>$records ? $tagrecords : array(),
					'count'=>$records ? $count : 0
				));				
			}	
		}	
	} 
	public function getRecords(Request $request)
    {

    } 
	public function getRecord(Request $request)
    {

    } 	
	public function saveRecords(Request $request)
    {
	}
    protected function validator(array $data)
    {
		$rules = [
			'tag' => ['required', 'string', 'max:255', 'unique:tags'],
		];	
		return Validator::make($data, $rules);
    }	
	public function saveRecord(Request $request)
    {
		$tag = $request->get('tag');
		$record = Tag::where('tag',$tag)->first();
		$userLoggedInId = $request->user()->id;	
		if( $record ){
			$id = $record->id;
			$request->request->add(['updated_by' => $userLoggedInId]);	
			$this->update($request,$id);
			$data =  Tag::findorFail($id);
		}else{
			$request->request->add(['created_by' => $userLoggedInId]);
			$data = $this->store($request);
		}		
    } 
    public function show($id)
    {
		return Tag::where(['id'=>$id]);
    }

    public function store(Request $request)
    {
        return Tag::create($request->all());
    }

    public function update(Request $request, $id)
    {
        $Listing = Tag::findorFail($id);
        $Listing->update($request->all());

        return $Listing;
    }

    public function delete(Request $request, $id)
    {
        $Listing = Tag::where('id', $id);
        $Listing->delete();

        return 204;
    }	
}
