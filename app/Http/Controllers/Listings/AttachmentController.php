<?php

namespace App\Http\Controllers\Listings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;
use Illuminate\Support\Facades\Storage; 
use Illuminate\Filesystem\Filesystem;
use App\model\listings\Attachment as Attachment;
use Illuminate\Support\Facades\Log;

class AttachmentController extends Controller
{
    public function __construct()
    {
		//$this->middleware('guest');
    }	
	public function getRecords(Request $request)
    {
		$res = array();
		$module_id = $request->input('module_id');
		$module = $request->input('module');
		$filedirectory = $request->input('filedirectory') ? $request->input('filedirectory') : 'all';
		$records = Attachment::where('module_id', $module_id)->where('module', $module)->get();
		foreach($records as $key => $attachment){
			$filenametostorefolder = $attachment->file_raw_name.'-'.$module_id;
			$filenametostore = $filenametostorefolder.'-130x130.'.$attachment->file_ext;
			$res[$key] = $attachment;
			$res[$key]->thumbnail = Storage::url($filedirectory.'/'.$filenametostorefolder.'/'.$filenametostore);
			$res[$key]->full = Storage::url($filedirectory.'/'.$filenametostorefolder.'.'.$attachment->file_ext);
		}
		echo json_encode($res);
    } 
	public function getRecord(Request $request)
    {

    } 	
	public function saveRecords(Request $request,$is_return=false)
    {
		$id = $request->get('module_id');
		$module = $request->get('module');
		$is_url = $request->get('is_url');
		$filename = $request->get('filename');
		$allow_multiple = $request->get('allow_multiple');
		$user = $request->get('user_id');
		$filedirectory = $request->get('filedirectory') ? $request->get('filedirectory') : 'all';
		$imagesizes = get_image_sizes();
		if( !$is_url ){
			$file = $request->file('file');
			//get filename with extension
			$filenamewithextension = preg_replace("/[^a-z0-9\_\-\.]/i", '',$file->getClientOriginalName());
			
			//get filename without extension
			$filename = preg_replace("/[^a-z0-9\_\-\.]/i", '',pathinfo($filenamewithextension, PATHINFO_FILENAME));
			//get file extension
			$extension = $file->getClientOriginalExtension();
			$filesize = $file->getSize();
			$file_type = $file->getClientMimeType();		
		}else{
			$extension = 'jpg';
			$fileContents = file_get_contents($is_url);
			$filenamewithextension =  $filename . ".".$extension;
			$tempurl = 'public/temp/' .$filenamewithextension;
			Storage::put($tempurl, $fileContents);
			$pinfo = pathinfo(storage_path($tempurl));
			$tempstorage = Storage::disk('public');
			$filesize = $tempstorage->size('temp/' .$filename . ".".$extension);
			$file_type = $tempstorage->getMimeType('temp/' .$filename . ".".$extension);			
		}
		
		$record = [
				'file_name' => $filenamewithextension,
				'file_raw_name' => $filename,
				'file_ext' => $extension,
				'file_size' => $filesize,
				'file_type' => $file_type,
				'module_id' => $id,
				'created_by' => $user,
				'filedirectory' => $filedirectory,
			];
		$request->request->add($record);	

		//filename to store
		$filenametostorefolder = $filename.'-'.$id;
		$filenametostore = $filename.'-'.$id.'.'.$extension;
		
		if( $allow_multiple )
			$record_exists = Attachment::where('module',$module)->where('module_id',$id)->where('file_raw_name',$filename)->where('file_size',$filesize)->first();
		else	
			$record_exists = Attachment::where('module',$module)->where('module_id',$id)->first();
		
		if( $record_exists ){
			$record_delete = $record_exists->toArray();
			$record_delete['filedirectory'] = $filedirectory;
			self::removeAttachment((object) $record_exists);
		}else	
			self::removeAttachment((object) $record);
		
		if( !$is_url )
			Storage::put('public/'.$filedirectory.'/'. $filenametostore, fopen($file, 'r+'));
		else
			Storage::move($tempurl, 'public/'.$filedirectory.'/'. $filenametostore);
		
		//Resize image here
		foreach( $imagesizes as $aliasimg => $dimensions ){
			$thumbnail = $filename.'-'.$id.'-'.$dimensions[0].'x'.$dimensions[1].'.'.$extension;
			$dir = storage_path('app/public/'.$filedirectory.'/'.$filenametostorefolder).'/';
			if (!file_exists($dir)) {
				mkdir($dir, 0777, true);
			}			
			$thumbnailpath = $dir.$thumbnail;
			if( !$is_url )
				Storage::put('public/'.$filedirectory.'/'. $filenametostorefolder.'/'.$thumbnail, fopen($file, 'r+'));
			else
				Storage::put('public/'.$filedirectory.'/'. $filenametostorefolder.'/'.$thumbnail, $fileContents);
			$img = Image::make($thumbnailpath)->resize($dimensions[0], $dimensions[1], function($constraint) {
				$constraint->aspectRatio();
			});
			$img->save($thumbnailpath);	
		}	
		
		if( $record_exists ){
			$record_exists->update($record);
			$record = self::show($record_exists->id)->first();
		}else
			$record = self::store($request);
		
		if( $is_return )
			return $record;
		
		echo json_encode(
			['success'=>1,'id'=>$record->id,'url'=>Storage::url($filedirectory.'/'.$filenametostore)]
		);
    } 
	public function updateRecords(Request $request)
    {
		$module_id = $request->get('module_id');
		$module = $request->get('module');
		$new_module_id = $request->get('new_module_id');
		$filedirectory = $request->get('filedirectory') ? $request->get('filedirectory') : 'all';

		$record_exists = Attachment::where('module',$module)->where('module_id',$module_id)->get();
		if( $record_exists ){
			foreach( $record_exists as $record ){
				$filenametostorefolder = $record->file_raw_name.'-'.$module_id;
				$dir_old = storage_path('app/public/'.$filedirectory.'/'.$filenametostorefolder).'/';
				$filenametostore = $record->file_raw_name.'-'.$module_id.'.'.$record->file_ext;
				
				$filenametostorefolder_new = $record->file_raw_name.'-'.$new_module_id;
				$dir_new = storage_path('app/public/'.$filedirectory.'/'.$filenametostorefolder_new).'/';
				$filenametostore_new = $record->file_raw_name.'-'.$new_module_id.'.'.$record->file_ext;

				$beforeurl = 'public/'.$filedirectory.'/'. $filenametostore;
				$nowurl = 'public/'.$filedirectory.'/'. $filenametostore_new;
				//moving single file
				Storage::move($beforeurl, $nowurl);

				if( file_exists($dir_old) ){
					$imagesizes = get_image_sizes();
					
					//delete old directory
					$file = new Filesystem;
					$file->cleanDirectory($dir_old);
					rmdir($dir_old);

					$fileContents = file_get_contents(public_path('storage/'.$filedirectory.'/'.$filenametostore_new));
					//create thumbnails
					foreach( $imagesizes as $aliasimg => $dimensions ){
						$thumbnail = $record->file_raw_name.'-'.$new_module_id.'-'.$dimensions[0].'x'.$dimensions[1].'.'.$record->file_ext;
						if (!file_exists($dir_new)) {
							mkdir($dir_new, 0777, true);
						}			
						$thumbnailpath = $dir_new.$thumbnail;
						Storage::put('public/'.$filedirectory.'/'. $filenametostorefolder_new.'/'.$thumbnail, $fileContents);
						$img = Image::make($thumbnailpath)->resize($dimensions[0], $dimensions[1], function($constraint) {
							$constraint->aspectRatio();
						});
						$img->save($thumbnailpath);	
					}					
				}
			}
			//finally update 
			Attachment::where('module_id', $module_id)->update(['module_id'=>$new_module_id]);
		}		
    } 	

	public function deleteRecords(Request $request)
    {	
		$module_id = $request->input('module_id');
		$module = $request->input('module');
		$records = Attachment::where('module_id', $module_id)->where('module', $module)->get();
		foreach( $records as $record ){
			$this->removeAttachment($record);
			$this->delete($request,$record->id);
		}
    } 	
	public function deleteRecord(Request $request)
    {	
		$id = $request->segment(3);
		$record = $this->show($id)->first();
		if( $request->get('filedirectory') )
			$record->filedirectory = $request->get('filedirectory');
		$module_id = $request->input('module_id');
		$this->removeAttachment($record);
		$this->delete($request,$id);
    } 
	public function removeAttachment($file)
	{
		$filedirectory = $file->filedirectory ? $file->filedirectory : 'all';
		$id = $file->module_id;
		$filenametostorefolder = $file->file_raw_name.'-'.$id;
		$filenametostore = $filenametostorefolder.'.'.$file->file_ext;		
		$dir = public_path('storage/'.$filedirectory.'/'.$filenametostorefolder);
		$file = new Filesystem;
		if( file_exists( public_path('storage/'.$filedirectory.'/'.$filenametostore) ) ) {
			unlink(public_path('storage/'.$filedirectory.'/'.$filenametostore));
			$file->cleanDirectory($dir);
			rmdir($dir);
		}		
	}
	//getters and setters
	public function index()
    {
        return Attachment::all();
    }
 	public function count()
    {
        return Attachment::all()->count();
    }
    public function show($id)
    {
        return Attachment::where('id', $id);
    }

    public function store(Request $request)
    {
        return Attachment::create($request->all());
    }

    public function update(Request $request, $id)
    {
        $Attachment = Attachment::where('id', $id);
        $Attachment->update($request->all());

        return $Attachment;
    }

    public function delete(Request $request, $id)
    {
        $Attachment = Attachment::where('id', $id);
        $Attachment->delete();

        return 204;
    }	
}
