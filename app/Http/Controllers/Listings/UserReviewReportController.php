<?php

namespace App\Http\Controllers\Listings;

use App\Http\Controllers\Controller;
use App\User;
use App\model\listings\UserReviewReport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use LaravelEmojiOne;

class UserReviewReportController extends Controller
{
    public function __construct()
    {
		$this->middleware('web');
    }
	
	
	public function saveRecords(Request $request)
    {	
		$user_id = Auth::user() ? Auth::user()->id : 0;
		$data_encrypted = decode_array($request->get('encoded_data'));		
		$userLiked = User::find($user_id)->review_reports()->where('review_id',$data_encrypted->review_id)->first();
		$request->request->add(
			[
				'id' => $userLiked ? $userLiked->id : 0,
				'user_id' => Auth::user() ? Auth::user()->id : 0,
				'listing_id' => $data_encrypted->listing_id ? $data_encrypted->listing_id : 0,
				'review_id' => $data_encrypted->review_id ? $data_encrypted->review_id : 0,
				'message' => LaravelEmojiOne::toShort($request->get('message')),
			]
		);
		
		$data = $request->all();
		$validator = self::validator($data);
		if ($validator->fails())
		{
			if( $request->get('is_ajax') ){
				echo json_encode(array(
					'success' => false,
					'errors' => $validator->getMessageBag()->toArray()

				));
				return;
			}
		}

		$id = $request->input('id');
		if( $id ){
			$data = self::update($request,$id);
		}else{
			$data =  self::store($request);
		}
		if( $request->get('is_ajax') ){
			echo json_encode(array(
				'success' => true,
				'errors' => null

			));
			return;
		}		
    }

    protected function validator(array $data)
    {
		$rules = [
			'user_id' => ['required', 'integer'],
			'listing_id' => ['required', 'integer'],
			'message' => ['required', 'string', 'max:255'],
		];	
		return Validator::make($data, $rules);
    }
    public function store(Request $request)
    {
        return UserReviewReport::create($request->all());
    }

    public function update(Request $request, $id)
    {
        $review = UserReviewReport::findorFail($id);
        $review->update($request->all());

        return $review;
    }
}
