<?php

namespace App\Http\Controllers\Listings;

use Illuminate\Http\Request;
use App\model\listings\ListingCarMake;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ListingCarMakeController extends Controller
{
	public function getRecords(Request $request)
    {

    } 
	public function getRecord(Request $request)
    {

    } 	
    protected function validator(array $data)
    {
		$rules = [
			'user_id' => ['required', 'integer'],
			'listing_id' => ['required', 'integer'],
		];	
		return Validator::make($data, $rules);
    }	
	public function saveRecords(Request $request)
    {
		$data = $request->all();
		
		if( $data['encoded_data'] )
			$data_encrypted = decode_array($data['encoded_data']);
		
		$request->request->add(['user_id' => $data_encrypted->user_id,'listing_id'=>$data_encrypted->listing_id]);
		$validator = self::validator($request->all());
		if ($validator->fails()){
			if( $data_encrypted->is_ajax ){
				echo json_encode(array(
					'success' => false,
					'errors' => $validator->getMessageBag()->toArray()

				));
				return;	
			}					
		}		
		$car_makes = $request->get('car_makes');
		if( $car_makes ){
			$now = date('Y-m-d H:i:s');
			ListingCarMake::where('listing_id',$data_encrypted->listing_id)->delete();
			foreach( $car_makes as $key => $car_make ){
				$tobeadded_listingcar_makes[] = ['listing_id'=>$data_encrypted->listing_id,'car_make_id'=>$car_make,'user_id'=>$data_encrypted->user_id,'created_at' => $now,'updated_at' => $now];
			}
			if( $tobeadded_listingcar_makes )
				ListingCarMake::insert($tobeadded_listingcar_makes);
			if( $data_encrypted->is_ajax ){
				echo json_encode(array(
					'success' => true,
					'errors' => ''

				));
				return;	
			}								
		}		
    } 

    public function show($id)
    {
		return ListingCarMake::where(['id'=>$id]);
    }

    public function store(Request $request)
    {
        return ListingCarMake::create($request->all());
    }

    public function update(Request $request, $id)
    {
        $Listing = ListingCarMake::findorFail($id);
        $Listing->update($request->all());

        return $Listing;
    }

    public function delete(Request $request, $id)
    {
        $Listing = ListingCarMake::where('id', $id);
        $Listing->delete();

        return 204;
    }
}
