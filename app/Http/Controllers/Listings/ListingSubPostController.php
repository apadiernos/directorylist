<?php

namespace App\Http\Controllers\Listings;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Listings\AttachmentController;
use App\model\listings\ListingSubPost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use LaravelEmojiOne;

class ListingSubPostController extends Controller
{
	public function getRecords(Request $request)
    {

    } 
	public function getRecord(Request $request)
    {

    } 	
    protected function validator(array $data)
    {
		$rules = [
			'title' => ['required', 'string', 'max:255'],
		];	
		return Validator::make($data, $rules);
    }	
	public function saveRecords(Request $request)
    {
		$data = $request->all();
		$validator = self::validator($data);
		if( $data['encoded_data'] ){
			$data_encrypted = decode_array($data['encoded_data']);
			if ($validator->fails()){
				if( $data_encrypted->is_ajax ){
					echo json_encode(array(
						'success' => false,
						'errors' => $validator->getMessageBag()->toArray()

					));
					return;	
				}					
			}
		}	
		$request->request->add(['listing_post_id'=>$data_encrypted->listing_post_id ?? 0,'unique_post_id'=>$data_encrypted->unique_post_id,'unique_id'=>$data_encrypted->unique_id,'id'=>$data_encrypted->id ?? 0,'user_id' => $data_encrypted->user_id,'listing_id'=>$data_encrypted->listing_id]);
		if( isset($data['description']) )
			$request->request->add(['description' => LaravelEmojiOne::toShort($data['description'])]);		
		$id = $request->input('id');
		if( $id ){	
			$this->update($request,$id);
		}else{
			$data = $this->store($request);
			$id = $data->id;
			$options = array(
				'url'=>url('attachments'),
				'geturl'=>url('attachments/get'),
				'removeurl'=>url('attachments/delete'),
				'module'=>'subposts',
				'module_id'=>$id,
				'addeddata'=>['user_id'=>$data_encrypted->user_id,'filedirectory'=>'subposts','allow_multiple'=>1],
				'maxFiles'=>3,
				'callbacks'=>[
					'success'=>'callback_success',
					'sending'=>'callback_processing',
				],
			);			
		}
		$data =  ListingSubPost::findorFail($id);

		if( $data_encrypted->is_ajax &&  $data ){
			$data_encrypted->id = $data->id;	
			if( $data->attachmentsTemp ){
				$request = new \Illuminate\Http\Request();
				$request->setMethod('POST');
				$request->request->add(
					[
						'module_id' => $data->unique_id,
						'module' => 'subposts',
						'new_module_id' => $data->id,
						'filedirectory' => 'subposts',
					]
				);			
			}
			AttachmentController::updateRecords($request);	
			if( $data->attachments ){
				$img = get_attachment_url_by_record($data->attachments->first(),'subposts','thumbnail');
			}else if( $data->attachmentsTemp ){
				$img = get_attachment_url_by_record($data->attachmentsTemp->first(),'subposts','thumbnail');
			}	
			echo json_encode(array(
				'success' => true,
				'record' =>json_encode(['title'=>$data->title,'description'=>$data->description]),
				'data_encoded' =>encode_array((array) $data_encrypted),
				'dz_options' =>$options ? json_encode($options) : false,
				'image' =>$img ? $img : false,
			));
			return;				
		}	
    } 

    public function show($id)
    {
		return ListingSubPost::where(['id'=>$id]);
    }

    public function store(Request $request)
    {
        return ListingSubPost::create($request->all());
    }

    public function update(Request $request, $id)
    {
        $Listing = ListingSubPost::findorFail($id);
        $Listing->update($request->all());

        return $Listing;
    }

    public function delete(Request $request, $id)
    {
        $Listing = ListingSubPost::where('id', $id);
        $Listing->delete();

        return 204;
    }
}
