<?php

namespace App\Http\Controllers\Listings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\listings\CarMake;

class CarMakeController extends Controller
{
	public function getRecordsByTerm(Request $request)
    {
		$data = $request->all();

		$count = CarMake::where('car_maker', 'like', '%' . $data['car_maker'] . '%')->count();
		$records = CarMake::where('car_maker', 'like', '%' . $data['car_maker'] . '%')->forPage($data['page'],10)->get();
		if( $data['encoded_data'] ){
			$data_encrypted = decode_array($data['encoded_data']);
			if( $data_encrypted->is_ajax ){
				if( $records ){
					$car_makers = array();
					foreach($records as $key => $record){
						$car_makers[$key] = array(
							'id'=>$record->id,
							'text'=>$record->car_maker,
						);
					}	
				}		
				echo json_encode(array(
					'success' => $records ? true : false,
					'records'=>$records ? $car_makers : array(),
					'count'=>$records ? $count : 0
				));				
			}	
		}	
	} 
}
