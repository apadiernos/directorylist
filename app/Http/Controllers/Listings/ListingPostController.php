<?php

namespace App\Http\Controllers\Listings;

use App\Http\Controllers\Controller;
use App\model\listings\ListingPost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ListingPostController extends Controller
{
	public function getRecords(Request $request)
    {

    } 
	public function getRecord(Request $request)
    {

    } 	
    protected function validator(array $data)
    {
		$rules = [
			'title' => ['required', 'string', 'max:255'],
		];	
		return Validator::make($data, $rules);
    }	
	public function saveRecords(Request $request)
    {
		$data = $request->all();
		$validator = self::validator($data);
		if( $data['encoded_data'] ){
			$data_encrypted = decode_array($data['encoded_data']);
			if ($validator->fails()){
				if( $data_encrypted->is_ajax && !$data_encrypted->is_redirect ){
					echo json_encode(array(
						'success' => false,
						'errors' => $validator->getMessageBag()->toArray()

					));
					return;	
				}else{
					return redirect('ads/single/'.$data_encrypted->listing_id.'/sections')->withErrors($validator)
	                        ->withInput();					
				}					
			}
		}
		$title = preg_replace( '/[\W]/', '', $data['title']);
		$slug_term = strtolower(trim(str_replace(' ','-',$title)));					
		$request->request->add(['unique_id'=>$data_encrypted->unique_id,'id'=>$data_encrypted->id ?? 0,'user_id' => $data_encrypted->user_id,'listing_id'=>$data_encrypted->listing_id,'slug_term'=>$slug_term]);
		$id = $request->input('id');
		if( $id ){	
			$this->update($request,$id);
			$data =  ListingPost::findorFail($id);
		}else{
			$data = $this->store($request);
		}
		if( $data->subpostsTemp )
			$data->subpostsTemp()->update(['listing_post_id'=>$data->id]);
		if( $data_encrypted->is_ajax && !$data_encrypted->is_redirect ){
			echo json_encode(array(
				'success' => true,
				'errors' =>null
			));
			return;				
		}else{
			return redirect('ads/single/'.$data_encrypted->listing_id.'/sections');
		}	
    } 

    public function show($id)
    {
		return ListingPost::where(['id'=>$id]);
    }

    public function store(Request $request)
    {
        return ListingPost::create($request->all());
    }

    public function update(Request $request, $id)
    {
        $Listing = ListingPost::findorFail($id);
        $Listing->update($request->all());

        return $Listing;
    }

    public function delete(Request $request, $id)
    {
        $Listing = ListingPost::where('id', $id);
        $Listing->delete();

        return 204;
    }	
}
