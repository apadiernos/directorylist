<?php

namespace App\Http\Controllers\Listings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\model\listings\UserListView;
use App\User;

class UserListViewController extends Controller
{
    public function __construct()
    {
		$this->middleware('web');
    }
	
	
	public function saveRecords(Request $request)
    {	
		$user_id = Auth::user() ? Auth::user()->id : 0;
		$userLiked = User::find($user_id)->list_views()->where('listing_id',$request->get('listing_id'))->first();
		$request->request->add(
			[
				'id' => $userLiked ? $userLiked->id : 0,
				'user_id' => $user_id,
			]
		);
		
		$data = $request->all();
		$validator = self::validator($data);
		if ($validator->fails())
		{
			if( $request->get('is_ajax') ){
				echo json_encode(array(
					'success' => false,
					'errors' => $validator->getMessageBag()->toArray()

				));
				return;
			}
		}

		$id = $request->input('id');
		if( $id ){
			$request->request->add(['updated_by' => $user_id]);
			$data = self::update($request,$id);
		}else{
			$request->request->add(['created_by' => $user_id]);
			$data =  self::store($request);
		}
		if( $request->get('is_ajax') ){
			echo json_encode(array(
				'success' => true,
				'errors' => null

			));
			return;
		}		
    }

    protected function validator(array $data)
    {
		$rules = [
			'user_id' => ['required', 'integer'],
			'listing_id' => ['required', 'integer'],
		];	
		return Validator::make($data, $rules);
    }
    public function store(Request $request)
    {
        return UserListView::create($request->all());
    }

    public function update(Request $request, $id)
    {
        $review = UserListView::findorFail($id);
        $review->update($request->all());

        return $review;
    }
}
