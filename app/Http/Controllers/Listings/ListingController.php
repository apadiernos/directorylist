<?php

namespace App\Http\Controllers\Listings;

use App\Http\Controllers\Controller;
use App\model\listings\Listing as ListingModel;
use App\model\listings\ListingTag;
use App\model\listings\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use LaravelEmojiOne;

class ListingController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
		//$this->middleware('guest');
    }

	public function getRecordsByAjax(Request $request)
    {
		$data = $request->all();
		$data['pagination'] = isset($data['pagination']) ? $data['pagination'] : array();
		$data['orderby'] = isset($data['orderby']) ? $data['orderby'] : array();
		$records = $this->index($data['params'],$data['pagination'],$data['orderby']);
		if( count($records) > 0 ){
			$locs = array();
			foreach( $records as $record ){
				$attach = get_attachment_url($record->attachments,'listings','thumbnail')[0];
				$attach = $attach ? $attach : 'assets/img/tmp/card-1.jpg';							
				$locs[] = [
					'id'=>$record->id,
					'icon'=>'<i class="'.$record->icon.'"></i>',
					'title'=>$record->title,
					'category'=>$record->category,
					'category_url'=>url('lists/'.$record->slug_term),
					'url'=>url('lists/'.$record->slug_term.'/'.$record->list_slug_term),
					'image'=>$attach,
					'center'=>[$record->lat,$record->long],
				]; 				
			}
			echo json_encode($locs);
		}
    } 	
	public function getLocationAjax(Request $request)
	{
		echo json_encode(self::geoCodeAddress($request));
    }  	
	public function getRecords(Request $request)
    {

    } 
	public function getRecord(Request $request)
    {

    } 	
	public function saveRecords(Request $request)
    {
		$data = $request->all();
		if( $data['encoded_data'] ){
			$data_encrypted = decode_array($data['encoded_data']);
			$request->request->add(
				[
					'id' => isset($data_encrypted->id) ? $data_encrypted->id : 0,
					'user_id' => isset($data_encrypted->user_id) ? $data_encrypted->user_id : 0,
				]
			);
			if( $data_encrypted->id  ){
				$record = ListingModel::where('user_id',$data_encrypted->user_id)->where('id',$data_encrypted->id)->first();	
				if( !$record ){
					if( $data_encrypted->is_ajax && !$data_encrypted->redirect ){
						echo json_encode(array(
							'success' => false,
							'errors' =>'',

						));
						return;
					}else
						abort(405);	
				}
				$data['title'] = $record->title;	
				$status = ( $record->attachments->count() && $record->address && $record->long && $record->lat ) ? 1 : 0;
				$request->request->add(
					[
						'category_id' => $record->category_id,
						'title' => $record->title,
						'email' => $record->email,
						'status' => $status,
					]
				);				
			}		
		}
		$title = preg_replace( '/[\W]/', '', $data['title']);
		$slug_term = strtolower(trim(str_replace(' ','-',$title)));			
		if( $data['lat'] && $data['long'] ){
			$request->request->add(
				[
					'country' => $data['country']['country'],
					'state' => $data['country']['state'],
					'region' => $data['country']['region'],
					'region_code' => $data['country']['region_code'],
					'city' => $data['country']['city'],
					'brgy' => $data['country']['brgy'],
				]
			);
		}	
		if( isset($data['sections']) )
			$request->request->add(['sections' => Crypt::encryptString(serialize($data['sections']))]);
		if( isset($data['socialmedia']) )
			$request->request->add(['socialmedia' => Crypt::encryptString(serialize($data['socialmedia']))]);
		if( isset($data['description']) )
			$request->request->add(['description' => LaravelEmojiOne::toShort($data['description'])]);
		$request->request->add(['slug_term' => $slug_term]);	

		$validator = self::validator($request->all());
		if ($validator->fails())
		{
			if( $data_encrypted->is_ajax && !$data_encrypted->redirect ){
				echo json_encode(array(
					'success' => false,
					'errors' => $validator->getMessageBag()->toArray()

				));
				return;
			}else{
				return redirect('ads/single/')->withErrors($validator)
                        ->withInput();
			}

		}

		$userLoggedInId = $request->user()->id;	
		$id = $request->get('id');
		if( $id ){
			$request->request->add(['updated_by' => $userLoggedInId]);	
			$this->update($request,$id);
			$data =  ListingModel::findorFail($id);
		}else{
			$request->request->add(['created_by' => $userLoggedInId]);
			$data = $this->store($request);
		}
		$tags = $request->get('tags');
		if( $tags ){
			$now = date('Y-m-d H:i:s');
			ListingTag::where('listing_id',$data->id)->delete();
			foreach( $tags as $key => $tag ){
				if( !is_numeric($tag) ){
					$tobeadded_tags = ['tag'=>$tag,'created_by'=>$userLoggedInId,'created_at' => $now,'updated_at' => $now];
					$tag_id = Tag::insertGetId($tobeadded_tags);
					$tobeadded_listingtags[] = ['listing_id'=>$data->id,'tag_id'=>$tag_id,'user_id'=>$userLoggedInId,'created_at' =>$now,'updated_at' => $now];
				}else{
					$tobeadded_listingtags[] = ['listing_id'=>$data->id,'tag_id'=>$tag,'user_id'=>$userLoggedInId,'created_at' => $now,'updated_at' => $now];
				}
			}
			if( $tobeadded_listingtags )
				ListingTag::insert($tobeadded_listingtags);				
		}	
		if( $data_encrypted->redirect && $data->id )
			return redirect('ads/single/'.$data->id);
		else if( $data_encrypted->is_ajax && $data->id ){
			echo json_encode(array(
				'success' => true,
				'errors' =>'',

			));
			return;			
		}
    } 

    protected function validator(array $data)
    {
		$rulesSet = [
			'user_id' => ['required', 'integer'],
			'category_id' => ['required', 'integer'],
			'long' => ['required', 'numeric'],
			'lat' => ['required', 'numeric'],
			'country' => ['required', 'string', 'max:3'],
			'state' => ['required', 'string', 'max:6'],
			'address' => ['required', 'string', 'max:255'],
			'brgy' => ['required', 'string', 'max:255'],
			'city' => ['required', 'string', 'max:255'],
		];	
		foreach( $data as $key => $val ){
			if( $rulesSet[$key] )
					$rules[$key] = $rulesSet[$key];
		}
		return Validator::make($data, $rules);
    }

	public function saveRecord(Request $request)
    {
		
    } 	
	public function geoCodeAddress($request)
	{
		if( isset($request['state']) ){
			$countrydata = getCountriesCities('PHL',$request['state']);
			return $countrydata['state'];
		}
	}
	
	//getters and setters
	public function index($params=array(),$pagination=array(),$orderby=array())
    {
		
		$location = array();
		$select = array(
			DB::raw("GROUP_CONCAT(DISTINCT concat_ws('::',attachments.file_raw_name,attachments.file_ext,attachments.module_id) SEPARATOR ',') AS attachments, AVG(reviews.rating) as rating"),
			'listings.slug_term as list_slug_term',
			'listings.id',
			'listings.title',
			'listings.website',
			'listings.long',
			'listings.lat',
			'listings.email',
			'listing_categories.icon',
			'listing_categories.category',
			'listing_categories.slug_term',
			'listings.category_id',
			'listings.created_by',
			'listings.status',
			'listings.user_id',
			
		);
	    $Listings = DB::table('listings');
		
		$Listings->leftJoin('attachments', function($attachments){
            $attachments->on('listings.id', '=', 'attachments.module_id');
            $attachments->on('attachments.module', '=', DB::raw('"listings"'));
        });
		$Listings->join('listing_categories', function($listing_categories){
            $listing_categories->on('listings.category_id', '=', 'listing_categories.id');
        });	
		$Listings->leftJoin('reviews', function($reviews){
            $reviews->on('listings.id', '=', 'reviews.listing_id');
        });			
		if( $params ){
			foreach($params as $column => $value){
				if( is_array($value) ){
					if( $value['method'] ){
						$method = $value['method'];
						$Listings->$method($column,$value['operator'],$value['value']);
					}else	
						$Listings->where($column,$value['operator'],$value['value']);
				}else{
					if( $column == 'lat' || $column == 'long' || $column == 'distance' )
						$location[$column] = $value;
					else
						$Listings->where($column,$value);
				}
			}
		}

		if( count($location) > 0 ){
			$select[] = (DB::raw('( 
                        3959 * acos( cos( radians('.$location['lat'].') 
                    ) * cos( 
                        radians( listings.lat ) 
                    ) * cos( 
                        radians( listings.long ) - radians('.$location['long'].') 
                    ) + sin( 
                        radians('.$location['lat'].') 
                    ) * sin( radians( listings.lat ) ) 
                ) 
            ) AS distance'));
			
			if( isset($location['distance']) )
				$Listings->having('distance', '<', $location['distance']);
			else
				$Listings->having('distance', '<', 20);
		}
		
		$Listings->select($select);
		unset($select[0]);
		$select[1] = 'listings.slug_term';
		if( count($location) > 0 )
			unset($select[count($select)]);

		$Listings->groupBy($select);
		
		if( $pagination ){
			$Listings->forPage($pagination['offset'],$pagination['limit']);
		}	
		if( $orderby ){
			$Listings->orderBy($orderby['column'],$orderby['sorted']);
		}else
			$Listings->latest('listings.created_at');
		
		return $Listings->get();
    }
 	public function count()
    {
        return ListingModel::all()->count();
    }
    public function show($id)
    {
		return ListingModel::where(['id'=>$id]);
    }

    public function store(Request $request)
    {
        return ListingModel::create($request->all());
    }

    public function update(Request $request, $id)
    {
        $Listing = ListingModel::findorFail($id);
        $Listing->update($request->all());

        return $Listing;
    }

    public function delete(Request $request, $id)
    {
        $Listing = ListingModel::where('id', $id);
        $Listing->delete();

        return 204;
    }
}


class googleGeocoding {
	/**
	 * The Google Geocoding API key holder
	 * @var string
	 */
	private $apiKey;
	/**
	 * googleGeocoding constructor.
	 *
	 * @param string $apiKey
	 *
	 * @throws Exception
	 */
	public function __construct( $apiKey = '' ) {
		if ( $apiKey ) {
			$this->apiKey = $apiKey;
		} else {
			throw new Exception( __CLASS__ . ' error : You must set your API key before using this class!' );
		}
	}
	/**
	 * Get address, latitude, longitude from address
	 *
	 * @param $address
	 *
	 * @return array|bool
	 */
	public function getCoordinates( $request ) {
		
		$countrydata = getCountriesCities($request['country'],$request['state']);
		
/* 		$country = $countrydata['country_name'];	
		$city = $countrydata['state']['name'];	
		$address = $request['address'].', '.$city.' '.$country;	
		$address = urlencode( $address );
		$url     = "http://open.mapquestapi.com/geocoding/v1/address?location={$address}&key={$this->apiKey}&thumbMaps=false&inFormat=kvp&outFormat=json";
		$resp    = json_decode( file_get_contents( $url ), true );	
		if ( $resp['results'][0]['locations'] ) {

			$lat               = $resp['results'][0]['locations'][0]['latLng']['lat'] ?? '';
			$long              = $resp['results'][0]['locations'][0]['latLng']['lng'] ?? '';
			return array( 'lat' => $lat, 'long' => $long );
		}
	 */
		return $countrydata['state'];
	}
}