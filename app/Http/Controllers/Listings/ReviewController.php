<?php

namespace App\Http\Controllers\Listings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\listings\Review as ReviewModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use LaravelEmojiOne;

class ReviewController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
		$this->middleware('guest:web');
    }
	
	public function getRecords(Request $request)
    {
		$this->index($request);
    } 
	public function getRecord(Request $request)
    {
		echo '1';
    } 	
	public function saveRecords(Request $request)
    {
		$data = $request->all();
		$data_encrypted = decode_array($data['encoded_data']);		
		$data['listing_id'] = $data_encrypted->listing_id ? $data_encrypted->listing_id : null;
		$data['parent_id'] = $data_encrypted->parent_id ? $data_encrypted->parent_id : null;
		$data['module'] = $data_encrypted->module ? $data_encrypted->module : null;
		$validator = self::validator($data);
		if ($validator->fails())
		{
			if( $data_encrypted->is_ajax ){
				echo json_encode(array(
					'success' => false,
					'errors' => $validator->getMessageBag()->toArray()

				));
				return;
			}
		}
		
		$request->request->add(
			[
				'id' => isset($data_encrypted->id) ? $data_encrypted->id : 0,
				'message' => LaravelEmojiOne::toShort(str_replace($data['name'],'<b>'.$data['name'].'</b>',$data['message'])),
				'listing_id' => isset($data_encrypted->listing_id) ? $data_encrypted->listing_id : null,
				'parent_id' => isset($data_encrypted->parent_id) ? $data_encrypted->parent_id : 0,
				'visitor' => $request->ip(),
				'timecreated' => time(),
				'rating' => $data['rating'] ?? 0,
				'user_id' => Auth::user() ? Auth::user()->id : 0,
				'module' => $data_encrypted->module,
			]
		);

		$id = $request->input('id');
		if( $id ){
			$data = $this->update($request,$id);
		}else{
			$data = $this->store($request);
		}

		$data->timecreated = $data->parent_id ? get_time_ago($data->timecreated) : format_time($data->timecreated,1);
		if( !$data->parent_id ){
			$data->encoded_data = encode_array(['module'=>$data->module,'listing_id'=>$data->listing_id,'parent_id'=>$data->id]);	
			$data->encoded_post_data = encode_array(['module'=>$data->module,'id'=>$data->id,'listing_id'=>$data->listing_id]);	
		}else{
			$encoded_data = encode_array(['module'=>$data->module,'is_ajax'=>1,'parent_id'=>$data->parent_id,'listing_id'=>$data->listing_id,'id'=>$data->id]);
			$edit_reply = '<a class="manage-reply fz-sm">			
				<input type="hidden" value="'.$encoded_data.'">
				<i class="m-l-xs fas fa-ellipsis-h fz-sm"></i>

			</a>';				
			$edit_reply .= '
				<div class="dropdown-menu dropdown-menu-sm" id="context-menu">
				  <a class="dropdown-item p-xs p-h-xs fz-sm edit-reply" href="#">Edit</a>
				  <a class="dropdown-item p-xs p-h-xs fz-sm delete-reply" href="#">Delete</a>	
				</div>					
			';
			$data->edit_reply = $edit_reply;			
		}
		if( Auth::user() ){
			$data->name = get_fullname(Auth::user());
			$data->pp = get_attachment_url_by_record(Auth::user()->profilepicture,'user','square1');
		}
		if( isset($data_encrypted->id) )
			$data->edited = encode_data_short($data_encrypted->id);
		
		$data->message = emojione($data->message);
		if( $data_encrypted->is_ajax ){
			if( $data->parent_id )
				$data->parent_id_encr = encode_data_short($data->parent_id);
			
			$data->id_encr = encode_data_short($data->id);
			echo json_encode($data);
		}else
			return $data;
    } 
	
	public function deleteRecords(Request $request)
    {
		$data = $request->all();
		if( $data['encoded_data'] ){
			$data_encrypted = decode_array($data['encoded_data']);
			$request->request->add(
				[
					'id' => isset($data_encrypted->id) ? $data_encrypted->id : 0,
				]
			);			
		}
		$err = 1;
		if( $request->get('id') ){
			ReviewModel::find($request->get('id'))->delete();
			$err = 0;
		}
		if( $data_encrypted->is_ajax ){
			echo json_encode(
				[
					'id_encr'=>$request->get('id') ? encode_data_short($request->get('id')) : 0,
					'message'=>$err ? 'error' : 'success',
				]
			);
		}
    } 	

    protected function validator(array $data)
    {
        
		if( Auth::user() ){
			$rules = [
				'message' => ['required', 'string', 'max:255'],
				'listing_id' => ['required', 'integer'],
				'module' => ['required', 'string', 'max:255'],
			];	
			if( isset($data['parent_id'])  )
				$rules['parent_id'] = ['required', 'integer'];			
		}else{
			$rules = [
				'message' => ['required', 'string', 'max:255'],
				'name' => ['required', 'string', 'max:255'],
				'listing_id' => ['required', 'integer'],
				'email' => ['required', 'string', 'email', 'max:255'],
				'module' => ['required', 'string', 'max:255'],
			];
			if( !isset($data['verified'])  )
				$rules['captcha'] = 'required|captcha';			
			if( !isset($data['parent_id'])  )
				$rules['rating'] = ['required', 'integer'];	
		}
		return Validator::make($data, $rules);
    }
	
	//getters and setters
	public function index($params=array(),$pagination=array(),$orderby=array())
    {
		$Listings = ReviewModel::select();
		if( $params ){
			foreach($params as $column => $value){
				if( is_array($value) ){
					if( $value['method'] ){
						$method = $value['method'];
						if( isset($value['operator']) )
							$Listings->$method($column,$value['operator'],$value['value']);
						else
							$Listings->$method($column,$value['value']);
					}else{	
						if( isset($value['operator']) )
							$Listings->where($column,$value['operator'],$value['value']);
						else
							$Listings->where($column,$value['value']);
					}
				}else
					$Listings->where($column,$value);
			}
		}
		
		if( $pagination ){
			$Listings->forPage($pagination['offset'],$pagination['limit']);
		}	
		if( $orderby ){
			foreach( $orderby as $orderbyRecord )
				$Listings->orderBy($orderbyRecord['column'],$orderbyRecord['sorted']);
		}else
			$Listings->latest('created_at');
		
		return $Listings->get();
    }
 	public function count($params=array())
    {
        $Listings = ReviewModel::select();
		if( $params ){
			foreach($params as $column => $value){
				if( is_array($value) ){
					if( $value['method'] ){
						$method = $value['method'];
						$Listings->$method($column,$value['operator'],$value['value']);
					}else	
						$Listings->where($column,$value['operator'],$value['value']);
				}else
					$Listings->where($column,$value);
			}
		}		
		return $Listings->count();        
    }
 	public function avg($params=array(),$columnselected)
    {
		$Listings = ReviewModel::select();
		if( $params ){
			foreach($params as $column => $value){
				if( is_array($value) ){
					if( $value['method'] ){
						$method = $value['method'];
						$Listings->$method($column,$value['operator'],$value['value']);
					}else	
						$Listings->where($column,$value['operator'],$value['value']);
				}else
					$Listings->where($column,$value);
			}
		}		
		return $Listings->avg($columnselected);
    }	
    public function show($id)
    {
        return ReviewModel::where('id', $id);
    }

    public function store(Request $request)
    {
        return ReviewModel::create($request->all());
    }

    public function update(Request $request, $id)
    {
        $review = ReviewModel::findorFail($id);
        $review->update($request->all());

        return $review;
    }


	
}
