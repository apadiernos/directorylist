<?php

namespace App\Http\Controllers\Listings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\listings\ReviewReaction as ReviewReactionModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ReviewReactionController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
		//$this->middleware('guest');
    }
	
	public function getRecords(Request $request)
    {
		$this->index($request);
    } 
	public function getRecord(Request $request)
    {

    } 	
	public function saveRecords(Request $request)
    {
		$data = $request->all();
		$data_encrypted = decode_array($data['encoded_data']);	
		$id = 0;
		if( $data_encrypted->id )
			$id = $data_encrypted->id;
		else if( $request->input('control_id') )
			$id = decode_data_short($request->input('control_id'));
		$request->request->add(
			[
				'id' => $id,
				'reaction' => $request->input('value'),
				'visitor' => $request->ip(),
				'timecreated' => time(),
				'listing_id' => $data_encrypted->listing_id,
				'review_id' => $data_encrypted->review_id,
				'user_id' => Auth::user() ? Auth::user()->id : 0,
			]
		);
		$id = $request->input('id');
		if( $id ){
			$data = $this->update($request,$id);
		}else{
			$data = $this->store($request);
		}
		if( $data->id && $data_encrypted->is_ajax )
			echo json_encode([
				'control_id'=>encode_data_short($data->id),
				'encoded_data'=>encode_array(['is_ajax'=>1,'id'=>$data->id,'listing_id'=>$data->listing_id,'review_id'=>$data->review_id]),
			]);
		else
			return $data;
    } 

	//getters and setters
	public function index($params=array(),$pagination=array(),$orderby=array())
    {
		$Listings = DB::table('review_reactions');
		if( $params ){
			foreach($params as $column => $value){
				if( is_array($value) ){
					if( $value['method'] ){
						$method = $value['method'];
						if( isset($value['operator']) )
							$Listings->$method($column,$value['operator'],$value['value']);
						else
							$Listings->$method($column,$value['value']);
					}else{	
						if( isset($value['operator']) )
							$Listings->where($column,$value['operator'],$value['value']);
						else
							$Listings->where($column,$value['value']);
					}
				}else
					$Listings->where($column,$value);
			}
		}
		
		if( $pagination ){
			$Listings->forPage($pagination['offset'],$pagination['limit']);
		}	
		if( $orderby ){
			foreach( $orderby as $orderbyRecord )
				$Listings->orderBy($orderbyRecord['column'],$orderbyRecord['sorted']);
		}else
			$Listings->latest('created_at');
		
		return $Listings->get();
    }
	//getters and setters
	public function distinctreviewreactions($params=array(),$pagination=array(),$orderby=array())
    {
		$Listings = ReviewReactionModel::select('review_id','reaction',DB::raw('count(reaction) as total'));
		if( $params ){
			foreach($params as $column => $value){
				if( is_array($value) ){
					if( $value['method'] ){
						$method = $value['method'];
						if( isset($value['operator']) )
							$Listings->$method($column,$value['operator'],$value['value']);
						else
							$Listings->$method($column,$value['value']);
					}else{	
						if( isset($value['operator']) )
							$Listings->where($column,$value['operator'],$value['value']);
						else
							$Listings->where($column,$value['value']);
					}
				}else
					$Listings->where($column,$value);
			}
		}
		
		if( $pagination ){
			$Listings->forPage($pagination['offset'],$pagination['limit']);
		}	
		if( $orderby ){
			foreach( $orderby as $orderbyRecord )
				$Listings->orderBy($orderbyRecord['column'],$orderbyRecord['sorted']);
		}else
			$Listings->latest('created_at');
		
		$Listings->groupBy('review_id');
		$Listings->groupBy('reaction');
		return $Listings->get();
    }	
 	public function count($params=array())
    {
        $Listings = ReviewReactionModel::select();
		if( $params ){
			foreach($params as $column => $value){
				if( is_array($value) ){
					if( $value['method'] ){
						$method = $value['method'];
						$Listings->$method($column,$value['operator'],$value['value']);
					}else	
						$Listings->where($column,$value['operator'],$value['value']);
				}else
					$Listings->where($column,$value);
			}
		}		
		return $Listings->count();        
    }
 	public function avg($params=array(),$columnselected)
    {
		$Listings = ReviewReactionModel::select();
		if( $params ){
			foreach($params as $column => $value){
				if( is_array($value) ){
					if( $value['method'] ){
						$method = $value['method'];
						$Listings->$method($column,$value['operator'],$value['value']);
					}else	
						$Listings->where($column,$value['operator'],$value['value']);
				}else
					$Listings->where($column,$value);
			}
		}		
		return $Listings->avg($columnselected);
    }	
    public function show($id)
    {
        return ReviewReactionModel::where('id', $id);
    }

    public function store(Request $request)
    {
        return ReviewReactionModel::create($request->all());
    }
    public function update(Request $request, $id)
    {
        $review = ReviewReactionModel::findorFail($id);
        $review->update($request->all());

        return $review;
    }

    public function delete(Request $request, $id)
    {
        $review = ReviewReactionModel::where('id', $id);
        $review->delete();

        return 204;
    }
	
}
