<?php

namespace App\Http\Controllers\Listings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\listings\ListingCategory as ListingCategory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ListingCategoryController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
		//$this->middleware('guest');
    }
	
	public function getRecords(Request $request)
    {

    } 
	public function getRecord(Request $request)
    {

    } 	
	public function saveRecords(Request $request)
    {
		$userLoggedInId = $request->user()->id;	
		$id = $request->input('id');
		if( $id ){
			$request->request->add(['updated_by' => $userLoggedInId]);	
			echo $this->update($request,$id);
		}else{
			$request->request->add(['created_by' => $userLoggedInId]);
			echo $this->store($request);
		}
    } 
	public function saveRecord(Request $request)
    {
		
    } 	

	
	//getters and setters
	public function index($params=array(),$pagination=array(),$orderby=array())
    {
		$select = array(
			DB::raw("COUNT(listings.id) as listings"),
			'listing_categories.id',
			'listing_categories.icon',
			'listing_categories.category',
			'listing_categories.slug_term',
			'listing_categories.created_by',
			'listing_categories.updated_by',
		);
	    $listing_categories = DB::table('listing_categories')->select($select);

		$listing_categories->leftJoin('listings', function($listing_categories){
            $listing_categories->on('listing_categories.id', '=', 'listings.category_id');
        });	
		
		if( $params ){
			foreach($params as $column => $value){
				if( is_array($value) ){
					if( $value['method'] ){
						$method = $value['method'];
						$listing_categories->$method($column,$value['operator'],$value['value']);
					}else	
						$listing_categories->where($column,$value['operator'],$value['value']);
				}else
					$listing_categories->where($column,$value);
			}
		}
		unset($select[0]);
		$listing_categories->groupBy($select);
		
		if( $pagination ){
			$listing_categories->forPage($pagination['offset'],$pagination['limit']);
		}	
		if( $orderby ){
			$listing_categories->orderBy($orderby['column'],$orderby['sorted']);
		}else
			$listing_categories->latest('listing_categories.created_at');
		
		return $listing_categories->get();		
    }
 	public function count()
    {
        return ListingCategory::all()->count();
    }
    public function show($id)
    {
        return ListingCategory::where('id', $id);
    }

    public function store(Request $request)
    {
        return ListingCategory::create($request->all());
    }

    public function update(Request $request, $id)
    {
        $listingcategory = ListingCategory::findorFail($id);
        $listingcategory->update($request->all());

        return $listingcategory;
    }

    public function delete(Request $request, $id)
    {
        $listingcategory = ListingCategory::where('id', $id);
        $listingcategory->delete();

        return 204;
    }
	

}
