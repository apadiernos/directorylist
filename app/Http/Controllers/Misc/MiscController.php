<?php

namespace App\Http\Controllers\Misc;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use PhpSpellcheck\SpellChecker\Aspell;

class MiscController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
		$this->middleware('guest');
    }
	
	public function getCities(Request $request){
		$cities = getCountriesCities('PHL',$request['state']);
		if( isset($cities['towns']) )
			echo json_encode($cities['towns']['municipality_list']);
	}
	
	public function checkSpelling(Request $request){
		$aspell = Aspell::create();
		$text = $request->get('text');
		$misspellings = $aspell->check($text, ['en_US'], ['from_example']);
		$res = array();
		foreach ($misspellings as $misspelling) {
			$word = $misspelling->getWord();
			$res[$text][$word] = $misspelling->getSuggestions();
		}
		if( $res ) 
		echo json_encode($res);
	}	
}
