<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
		$this->middleware('guest:adminuser')->except('logout');
		$this->middleware('guest:web')->except('logout');
    }
	
	public function adminLogin(Request $request)
    {
		$this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);
		
        if (Auth::guard('adminuser')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {
            return redirect()->intended('/admin/dashboard');
        }
        return back()->withInput($request->only('email', 'remember'));
    }

	public function login(Request $request)
    {
		$this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);
		
        if (Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {
            if( $request->get('is_ajax') )
				echo 1;
			else
				return redirect()->intended('/');
        }
		if( $request->get('is_ajax') )
			echo __('auth.failed');
		else
			return back()->withInput($request->only('email', 'remember'));
    }
	
	public function logout(Request $request)
    {
        $is_admin = Auth::guard('adminuser')->check();
		if( $is_admin ){
			Auth::guard('adminuser')->logout();
			return redirect('/admin/login');
		}else
			return redirect('/login')->with(Auth::logout());
    }	
}
