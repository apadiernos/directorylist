<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\model\auth\UserAddress;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Listings\AttachmentController as AttachmentLibrary;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('web');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $rules =  [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ];
		if( isset($data['captcha']) ){
			$rules['email'] = ['required', 'string', 'email', 'max:255', 'unique:users'];	
			$rules['captcha'] = 'required|captcha';	
			$rules['username'] = ['required', 'max:255', 'unique:users'];
		}	
		return Validator::make($data,$rules);
    }

	
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function saveRecords(Request $request)
    {
		Log::info('dasdsa');
		$errors = array();
		$data = $request->all();
		$file = $request->file('file');
		
		$validator = self::validator($data);
		if ($validator->fails())
			$errors = $validator->getMessageBag()->toArray();
		
		$id = Auth::user()->id;
		$user = User::find($id);
	
		if( !$user )
			$errors['no_user_found'] = 'No user found';	
		

		if( $data['is_ajax']  && $errors ){
			echo json_encode(array(
				'success' => false,
				'errors' =>$errors 

			));
			return;
		}
		
		$request->request->add(
			[
				'id' => $id ? $id : 0,
				'email' => $user->email,
				'password' => Hash::make($data['password']),
			]
		);	
		$user->update($request->all());
	
		$useraddress = $user->address;	
		$useraddress_id = $useraddress ? $useraddress->id : 0;

		$request_address =
			[
				'id' => $useraddress_id,
				'user_id' => $id,
				'long' => $data['long'],
				'lat' => $data['lat'],
				'address' => $data['address'],
				'city' => $data['country']['city'],
				'brgy' => $data['country']['brgy'],
				'country' => $data['country']['country'],
				'state' => $data['country']['state'],
				'region' => $data['country']['region'],
				'region_code' => $data['country']['region_code'],
			];

		if( $useraddress ){
			$request_address['updated_by'] = $id;
			$useraddress->update($request_address);
		}else{
			$request_address['created_by'] = $id;
			$useraddress = UserAddress::create($request_address);
		}
		
		$file = $request->file('file');
		if( $file ){
			$request->request->add(
				[
					'module_id' => $id,
					'user_id' => $id,
					'filedirectory' => 'user',
					'module' => 'userpicture',
				]
			);
			AttachmentLibrary::saveRecords($request);	
		}else{
			if( $data['is_ajax'] ){
				echo json_encode(array(
					'success' => !$errors && $useraddress ? true : false,
					'errors' =>null 

				));
				return;
			}
		}		
		
    }	
}
