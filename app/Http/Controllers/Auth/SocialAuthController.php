<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Socialite;
use App\services\SocialAccountService as SocialAccountServiceLibrary;

class SocialAuthController extends Controller
{
    public function __construct()
    {
		$this->middleware('guest');
    }    
	public function redirect_google()
    {
		return Socialite::driver('google')->redirect();
    }
    public function callback_google(SocialAccountServiceLibrary $service)
    {
		$user = $service->createOrGetUser(Socialite::driver('google')->stateless()->user(),'google');
		auth()->login($user);
        return redirect()->to('/');
    }

	public function redirect_facebook()
    {
		return Socialite::driver('facebook')->redirect();
    }
    public function callback_facebook(SocialAccountServiceLibrary $service)
    {
		$user = $service->createOrGetUser(Socialite::driver('facebook')->fields([
                    'first_name', 
                    'last_name', 
                    'email', 
                    'middle_name'
                ])->stateless()->user(),'facebook');
		auth()->login($user);
        return redirect()->to('/');
    }	
}
