Hi <strong>{{ $name }}</strong>,
<p>Thank you for registering to Karnid as one of our partners. We hope to serve and help you in your business. Enjoy!</p>
<p>This link will be expired on {{ format_time($duration,1) }}</p>
<p><a href="{{$link}}" target="_blank">Click this link to complete the verification process</a></p>