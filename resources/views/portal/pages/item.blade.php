<div class="card" data-location="{{ $location }}">
	<div class="card-image" style="background-image: url('{{ $attach }}');">
		<a href="{{ $url['list'] }}"></a> 

		<div class="card-image-rating">
			<input type="text"  value="{{ ceil($rating) }}" data-plugin="rating" data-options="<?=clean_jsonString($optionsRating)?>" >
		</div><!-- /.card-image-rating -->
	</div><!-- /.card-image -->

	<div class="card-content">
		<h3><a href="{{ $url['category'] }}">{{ $category }}</a></h3>
		<h2><a href="{{ $url['list'] }}">{{ $title }}</a></h2>
	</div><!-- /.card-content -->

	<div class="card-actions clearfix">
		<!--
		<a href="listing-detail.html" class="card-action-icon"><i class="md-icon">favorite</i></a>
		<a href="listing-detail.html" class="card-action-icon"><i class="md-icon">flag</i></a>
		-->
		<a href="{{ $url['list'] }}" class="card-action-btn btn btn-transparent">{{$btn_label ?? 'Show More'}}</a>
	</div><!-- /.card-actions -->
</div><!-- /.card -->