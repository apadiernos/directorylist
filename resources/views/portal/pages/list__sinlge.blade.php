@extends('portal.layouts.portal')

@section('title',$record->title)
@section('page_title',$record->title)

@section('js')
	@parent
	<script type="text/javascript" src="{{ asset('assets/js/custom/portal/single.js') }}"></script> 
	<script type="text/javascript" src="{{ asset('assets/js/custom/portal/share.js') }}"></script> 
@endsection

@section('content')
<div class="modal-backdrop modal-backdrop-custom fade in" style="display: none"></div>
<div class="content-title">
	<div class="container">
		<h1>{{ $record->title }}</h1>

		<ul class="breadcrumb">
			<li><a href="{{ url('/') }}">{{ __('listings__category.home') }}</a> <i class="md-icon">keyboard_arrow_right</i></li>
			<li><a href="{{ url('lists/'.$recordcat->slug_term) }}">{{ $recordcat->category }}</a> <i class="md-icon">keyboard_arrow_right</i></li>
			<li class="active">{{ $record->title }}</li>
		</ul><!-- /.breadcrumb -->
	</div><!-- /.container -->
</div><!-- /.content-title -->

<div class="container">

	@include('portal.pages.single.gallery')
	@include('portal.pages.single.sections')

	<div class="row">
		<div class="col-md-4 col-lg-3">
			<div class="sidebar p-t-0">	
				@include('portal.pages.single.ads')
			</div><!-- /.sidebar -->
		</div><!-- /.col-sm-4 -->
		
		<div class="col-md-5 col-lg-6">
			<div class="">			
				<div class="listing-detail">
					@include('portal.pages.single.comments')
			 	</div><!-- /.listing-detail -->
			</div><!-- /.push-top-bottom -->
		</div><!-- /.col-sm-9 -->	
		
		<div class="col-md-3 col-lg-3">
			<div class="sidebar p-t-0">	
				@include('portal.pages.single.widgets')
			</div><!-- /.sidebar -->
		</div><!-- /.col-sm-4 -->
	</div><!-- /.row -->
</div><!-- /.container-->

<div class="comment-template" style="display:none">
	<? get_comment_template($module) ?>
</div>
<div class="comment-template-reply" style="display:none">
	<? get_comment_template($module,1) ?>
</div>

@include('portal.pages.single.modalcomments')

@endsection