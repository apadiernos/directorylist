@extends('portal.layouts.portal')

@section('title','Listings')
@section('page_title','Listings')

@section('js')
	@parent
	<script type="text/javascript" src="{{ asset('assets/js/custom/portal/list.js') }}"></script> 
@endsection

@section('content')
<div class="container-fluid fullwidth-wrapper listings">
	<div class="row">
		<div class="col-sm-5">
			<?
			$optionsMap = $locs = array();
			?>
			@if( count($records) > 0 )
				@foreach($records as $record)
					<? 
					$attach = get_attachment_url($record->attachments,'listings','thumbnail')[0];
					$attach = $attach ? $attach : 'assets/img/tmp/card-1.jpg';							
					$locs[] = [
						'id'=>$record->id,
						'icon'=>'<i class="'.$record->icon.'"></i>',
						'title'=>$record->title,
						'category'=>$record->category,
						'category_url'=>url('lists/'.$record->slug_term),
						'url'=>url('lists/'.$record->slug_term.'/'.$record->list_slug_term),
						'image'=>$attach,
						'center'=>[$record->lat,$record->long],
					]; 
					
					?>	
				@endforeach
			@endif	
			<?
			$optionsMap['location'] = $locs;
			$optionsMap['url'] = url('listings/ajax');
			?>			
			<div id="map-leaflet" class="full" data-plugin="leaflet" data-options="<?=clean_jsonString($optionsMap)?>" ></div>
		</div><!-- /.col-* -->		

		<div class="col-sm-7">			
			<div class="filter filter-white push-bottom">
				<form method="get" id="homemapform">
					<div class="row">
						<input type="hidden" name="category_id" value="<?=$category_id?>" />
						<div class="col-sm-12 col-md-8">
							{{ getCountriesCitiesHtm(null,null,true) }}
						</div><!-- /.col-* -->

						<div class="col-sm-12 col-md-2">
							<select class="form-control" name="distance" id="distance" >
								<option value="10">10 {{ trans_choice('listings__category.km',2) }}</option>
								<option value="20">20 {{ trans_choice('listings__category.km',2) }}</option>
								<option value="30">30 {{ trans_choice('listings__category.km',2) }}</option>
								<option value="40">40 {{ trans_choice('listings__category.km',2) }}</option>
								<option value="50">50 {{ trans_choice('listings__category.km',2) }}</option>
							</select>
						</div>	

						<div class="col-md-2">
							<div class="form-group-btn form-group-btn-placeholder-gap">
								<a type="submit" class="btn btn-primary btn-block load-more reset">Filter</a>
							</div><!-- /.form-group -->		
						</div><!-- /.col-* -->			
					</div><!-- /.row -->
				</form>
			</div><!-- /.filter -->
			<div id="list-container">
				<div class="row">
					<?
					$optionsRating = array(
						'displayOnly'=>true,
						'size'=>'xs',
						'step'=>1,
						'starCaptions'=> [1=> ' ', 2=> ' ', 3=> ' ', 4=> ' ', 5=> ' '],
						'starCaptionClasses'=> [1=> 'text-danger', 2=> 'text-warning', 3=> 'text-info', 4=> 'text-primary', 5=> 'text-success'],
						'emptyStar'=>'<i class="far fa-star"></i>',
						'filledStar'=>'<i class="fas fa-star"></i>',
					);						
					?>	
					@foreach( $records as $record ) 
						<div class="col-sm-6">
							<?
							$attach = $record->attachments ? get_attachment_url($record->attachments,'listings','thumbnail')[0] : 'assets/img/tmp/card-1.jpg';							

							$params = [
								'attach' => $attach,
								'url' => ['list' => url('lists/'.$record->slug_term.'/'.$record->list_slug_term),'category'=> url('lists/'.$record->slug_term) ],
								'rating' => $record->rating ,
								'category' => $record->category ,
								'title' => $record->title ,
								'optionsRating' => $optionsRating ,
							];
							?>
							@include('portal.pages.item', $params)
						</div><!-- /.col-* -->
					@endforeach 
				</div><!-- /.row -->		
			</div><!-- /#list-container -->		

			<div class="center">
				<a href="#" style="width:100%" class="btn btn-primary btn-medium load-more btn-list">Load More</a>
			</div><!-- /.center -->
		</div><!-- /.col-* -->		
	</div><!-- /.row -->
</div><!-- /.container -->

@endsection
