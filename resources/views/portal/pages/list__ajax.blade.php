<div class="row">
	<?
	$optionsRating = array(
		'displayOnly'=>true,
		'size'=>'xs',
		'step'=>1,
		'starCaptions'=> [1=> ' ', 2=> ' ', 3=> ' ', 4=> ' ', 5=> ' '],
		'starCaptionClasses'=> [1=> 'text-danger', 2=> 'text-warning', 3=> 'text-info', 4=> 'text-primary', 5=> 'text-success'],
		'emptyStar'=>'<i class="far fa-star"></i>',
		'filledStar'=>'<i class="fas fa-star"></i>',
	);						
	?>	
	@foreach( $records as $record ) 
		<div class="col-sm-6 col-md-4">
			<?
			$attach = $record->attachments ? get_attachment_url($record->attachments,'listings','thumbnail')[0] : 'assets/img/tmp/card-1.jpg';							

			$params = [
				'attach' => $attach,
				'url' => ['list' => url('lists/'.$record->slug_term.'/'.$record->list_slug_term),'category'=> url('lists/'.$record->slug_term) ],
				'rating' => $record->rating ,
				'category' => $record->category ,
				'title' => $record->title ,
				'optionsRating' => $optionsRating ,
				'location' => clean_jsonString(array('long'=>$record->long,'lat'=>$record->lat)) ,
			];
			?>	
			@include('portal.pages.item', $params)		
		</div><!-- /.col-* -->
	@endforeach 
</div><!-- /.row -->		
