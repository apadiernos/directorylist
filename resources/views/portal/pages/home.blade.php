@extends('portal.layouts.portal')

@section('title','Portal')
@section('page_title','Portal')

@section('js')
	@parent
	<script type="text/javascript" src="{{ asset('assets/js/custom/portal/home.js') }}"></script> 
	<script type="text/javascript" src="{{ asset('assets/js/custom/portal/test.js') }}"></script> 
@endsection

@section('content')
	<div class="hero">
		<?
		$optionsMap = $locs = array();
		?>
		@if( count($recordsLoc) > 0 )
			@foreach($recordsLoc as $recordLoc)
				<? 
				$attach = $recordLoc->attachments != null ? get_attachment_url($recordLoc->attachments,'listings','thumbnail')[0] : 'assets/img/tmp/card-1.jpg';							
				$locs[] = [
					'id'=>$recordLoc->id,
					'icon'=>'<i class="'.$recordLoc->icon.'"></i>',
					'title'=>$recordLoc->title,
					'category'=>$recordLoc->category,
					'category_url'=>url('lists/'.$recordLoc->slug_term),
					'url'=>url('lists/'.$recordLoc->slug_term.'/'.$recordLoc->list_slug_term),
					'image'=>$attach,
					'center'=>[$recordLoc->lat,$recordLoc->long],
				]; 
				?>	
			@endforeach
		@endif	
		<?
		$optionsMap['location'] = $locs;
		$optionsMap['url'] = url('listings/ajax');
		?>
		<div id="map-leaflet" data-plugin="leaflet" data-options="<?=clean_jsonString($optionsMap)?>"  class="full">
	</div>

	<div class="hero-form dark">
	<div class="container">
		<form method="get" id="homemapform" data-plugin="validate">
			<div class="row">
				<div class="input-group col-sm-12 col-md-8">
					{{ getCountriesCitiesHtm(null,null,true) }}
				</div><!-- /.input-group  -->
				<div class="form-group col-sm-12 col-md-2">
					<select class="form-control" name="distance" id="distance" style="background-color:rgb(0,0,0,0);border-bottom: 1px solid #fff">
						<option value="10">10 {{ trans_choice('listings__category.km',2) }}</option>
						<option value="20">20 {{ trans_choice('listings__category.km',2) }}</option>
						<option value="30">30 {{ trans_choice('listings__category.km',2) }}</option>
						<option value="40">40 {{ trans_choice('listings__category.km',2) }}</option>
						<option value="50">50 {{ trans_choice('listings__category.km',2) }}</option>
					</select>
				</div>	
				<div class="form-group last col-sm-12 col-md-2">
					<button type="submit" class="btn btn-primary btn-block search-home">Search</button>
				</div><!-- /.col-* -->
			</div>

		</form> 
		<!--
		<div class="hero-form-sub">
			<strong class="hidden-sm-down">Or search by category</strong>

			<ul>
				<li><a href="#">Properties</a></li>
				<li><a href="#">Cars</a></li>
				<li><a href="#">Jobs</a></li>
				<li><a href="#">Events</a></li>
				<li><a href="#">Food &amp; Drink</a></li>
				<li><a href="#">Travel</a></li>
				<li><a href="#">Hotel</a></li>
			</ul>
		</div>
		-->
	</div><!-- /.container search-->
</div><!-- /.hero-form -->
</div><!-- /.hero -->

<div class="container">
	<div class="page-title">
		<h2>Directory Listings</h2>
	</div><!-- /.page-title -->
	<div class="row">
		<div class="col-sm-12 col-xl-12">
			<div class="cards-wrapper">
				<div class="row">
					<?
					$optionsRating = array(
						'displayOnly'=>true,
						'size'=>'xs',
						'step'=>1,
						'starCaptions'=> [1=> ' ', 2=> ' ', 3=> ' ', 4=> ' ', 5=> ' '],
						'starCaptionClasses'=> [1=> 'text-danger', 2=> 'text-warning', 3=> 'text-info', 4=> 'text-primary', 5=> 'text-success'],
						'emptyStar'=>'<i class="far fa-star"></i>',
						'filledStar'=>'<i class="fas fa-star"></i>',
					);						
					?>	
					@foreach( $records as $record ) 
						<div class="col-sm-6 col-md-4">
							
							<?
							$attach = $recordLoc->attachments != null ? get_attachment_url($record->attachments,'listings','thumbnail')[0] : 'assets/img/tmp/card-1.jpg';

							$params = [
								'attach' => $attach,
								'url' => ['list' => url('lists/'.$record->slug_term.'/'.$record->list_slug_term),'category'=> url('lists/'.$record->slug_term) ],
								'rating' => $record->rating ,
								'category' => $record->category ,
								'title' => $record->title ,
								'optionsRating' => $optionsRating ,
							];
							?>
							@include('portal.pages.item', $params)
							
						</div><!-- /.col-* -->
					@endforeach 
				
	
				</div><!-- /.row -->
			</div><!-- /.card-wrapper -->
		</div><!-- /.col-* -->
	
	</div><!-- /.row -->
</div><!-- /.container -->


<div class="container">
	<div class="page-title">
		<h2>Directory Categories</h2>
	</div><!-- /.page-title -->
	
	<div class="cards-wrapper">
		<div class="row">
			@if( count($recordsCat) )
				@foreach( $recordsCat as $category )
					<div class="col-xs-12 col-sm-6 col-lg-3">
						<div class="card card-category">
							<div class="card-category-content card-content">
								<h2>
									<a href="{{ url('lists/'.$category->slug_term) }}"><i class="{{ $category->icon }} fz-lg"></i> {{ $category->category }}</a>
								</h2>
							</div><!-- /.card-content -->
						</div><!-- /.card -->
					</div><!-- /.col-* -->
				@endforeach
			@endif
			
		</div><!-- /.row -->
	</div><!-- /.cards-wrapper -->
</div><!-- /.container -->

@endsection
