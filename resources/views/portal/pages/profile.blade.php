@extends('portal.layouts.portal')

@section('title',$fullname)
@section('page_title',$fullname)

@section('js')
	@parent
	<script type="text/javascript" src="{{ asset('assets/js/custom/locations.js') }}"></script> 
	<script type="text/javascript" src="{{ asset('assets/js/custom/portal/profile/profile.js') }}"></script> 
	<script type="text/javascript" src="{{ asset('assets/js/custom/portal/profile/lists.js') }}"></script> 
	@if( $js )
		@foreach( $js as $key => $jsFile )	
			<script type="text/javascript" src="{{ asset('assets/js/custom/portal/profile'.$jsFile) }}"></script> 
		@endforeach
	@endif
@endsection

@section('content')
<div class="content-title">
	<div class="container">
		<h1>{{ $fullname }}</h1>
		<ul class="breadcrumb">
			@if( $breadcrumbs )
				@foreach( $breadcrumbs as $key => $breadcrumb )		
					@if ($loop->last)
						<li class="active">{{ $key }}</li>
					@else
						<li><a href="{{ url($breadcrumb) }}">{{ $key }}</a> <i class="md-icon">keyboard_arrow_right</i></li>
					@endif					
				@endforeach
			@else	
			
				<li><a href="{{ url('/') }}">{{ __('listings__category.home') }}</a> <i class="md-icon">keyboard_arrow_right</i></li>
				<li class="active">{{ $fullname }}</li>
			@endif
		</ul><!-- /.breadcrumb -->
	</div><!-- /.container -->
</div><!-- /.content-title -->

<div class="container profile">
	<div class="row m-t-md">
		<div class="col-md-3 col-lg-2">
			<div class="sidebar p-t-0">	
				@include('portal.pages.profile.ads')
			</div><!-- /.sidebar -->
		</div><!-- /.col-sm-4 -->
		
		<div class="col-md-10 col-lg-10">
			<div class="profile-container-main">
				<div class="profile-container-ajax">
					@include('portal.pages.profile.'.$page)
				</div><!-- /.push-top-bottom -->
			</div><!-- /.push-top-bottom -->
		</div><!-- /.col-sm-9 -->	
		
	</div><!-- /.row -->
</div><!-- /.container-->

@endsection