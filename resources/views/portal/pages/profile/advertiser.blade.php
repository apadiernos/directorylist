@if( $togo  )
<?
$subject = 'Welcome to Karnid Business Owner Club!';
?>
<h4>Pending Email Verification</h4>
<div class="widget widget-background-white">
	Please kindly check your inbox at {{ $user->email }} and find this subject {{$subject}}.
	<br>
	<br>
	You can still verify the link until {{ format_time($togo,1) }}
</div>

@else
<h4>Terms of Service</h4>
<div class="widget widget-background-white">

	@if( $expired  )
	<div class="alert alert-danger p-xs text-center" role="alert">Unfortunately your verification link has been expired on {{ format_time($expired,1) }}. Please try again and register. Thanks</div>	
	@endif

	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
	<br>
	<br>
	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
	<br>
	<br>
	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
</div>

<div class="center m-b-md">
	<a href="#"   class="btn btn-primary btn-medium join-as-advert w-100">Accept & Register</a>
</div>


<div id="advertiser-modal" class="modal fade" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title">Congratulations!</h4>
			</div>

			<div class="modal-body">
				A verification email has been sent to you. Please follow the link to complete the registration process!
			</div><!-- .modal-body -->
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal"> Close</button>
			</div><!-- .modal-footer -->

		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>
@endif