
<div class="listing-detail">
	<div class="faq">
	    <form method="post" id="profile-info" data-plugin="validate" enctype="multipart/form-data">		    
			
			<div class="alert alert-danger p-xs text-center alert-danger-inquiry" style="display: none" role="alert">Error Processing Request!</div>
			<div class="alert alert-success p-xs text-center alert-success-inquiry" style="display: none" role="alert">Request successfully submitted.</div>
			
			<div class="faq-item">
				<div class="faq-item-question">
					<h2>Basic Information</h2>
				</div><!-- /.faq-item-question -->

				<div class="faq-item-answer">
					<fieldset>
						<div class="col-md-3 col-sm-3">
							<div class="profile-img-wrapper">
								<div class="profile-img" style="background: url('{{ get_attachment_url_by_record($user->profilepicture,'user','square4') }}')">
									<input id="files" type="file" name="file" class="filer" accept="image/*"/>
									<div id="profilepic-select">
										<i class="fas fa-camera fz-md m-t-lg"></i>
										<strong class="text-white show">Update</strong>
									</div>							
								</div>
							</div>
						</div>
						<div class="col-md-9 col-sm-9">
							<div class="form-group">
								<label>First Name</label>
								<input type="text" name="first_name" value="{{ $user->first_name }}" class="form-control required" required>
							</div><!-- /.form-group -->
							<div class="form-group">
								<label>Last Name</label>
								<input type="text" name="last_name" value="{{ $user->last_name }}" class="form-control required" required>
							</div><!-- /.form-group -->										
							<div class="form-group">
								<label>Username</label>
								<input type="text" name="username" value="{{ $user->username }}" class="form-control required" required>
							</div><!-- /.form-group -->	
						</div>					
						<div class="form-group">
							<label>Password</label>
							<input type="password" name="password" value="" class="form-control required" required>
						</div><!-- /.form-group -->	
						<div class="form-group">
							<label>Confirm Password</label>
							<input type="password" name="password_confirmation" value="" class="form-control required" required>
						</div><!-- /.form-group -->						
					</fieldset>
				</div><!-- /.faq-item-answer -->

				<div class="faq-item-meta">
					<button class="btn btn-primary btn-small">Save</button>
				</div><!-- /.faq-item-meta -->
			</div><!-- /.faq-item -->
			
			
			<div class="faq-item">
				<div id="autolocate" class="alert alert-success p-xs text-center" style="display: none" role="alert">Request successfully submitted.</div>
				
				<div class="faq-item-question">
					<h2>Address</h2>
				</div><!-- /.faq-item-question -->

				<div class="faq-item-answer">
					<fieldset>
						<legend>{{ trans_choice('listings__category.location',1) }}</legend>
						<div class="form-group">
							<label>Address</label>
							<input name="address" value="{{ $user->address->address }}" type="text" name="title" class="form-control" >
							
						</div><!-- /.form-group -->
							<?
							$data = array(
								'country'=>$user->address->country,
								'state'=>$user->address->state,
								'city'=>$user->address->city,
								'brgy'=>$user->address->brgy,
							);
							?>
							{{ getCountriesCitiesHtm($data) }}
						
					</fieldset>

					<fieldset>
						<legend>Lat/Long</legend>

						<div class="row">
							<div id="map-leaflet" data-plugin="leaflet" data-options=""  style="height: 500px">
						</div><!-- /.row -->
						<div class="row">
							<div class="col-sm-4">
								<div class="form-group">
									<label>{{ __('listings__category.lattitude') }}</label>
									<input name="lat" id="lat" value="{{ $user->address->lat }}" type="text" class="form-control" >
								</div><!-- /.form-group -->
							</div><!-- /.col-* -->

							<div class="col-sm-4">
								<div class="form-group">
									<label>{{ __('listings__category.loghitude') }}</label>
									<input name="long" id="long" value="{{ $user->address->long }}" type="text" class="form-control" >
								</div><!-- /.form-group -->
							</div><!-- /.col-* -->
							<div class="col-sm-12">
							<p class="m-b-0">
							Use this link to find lat/long :<a href="https://www.latlong.net/" target="_blank">https://www.latlong.net/</a>
							</p class="m-b-0">
							Paste this address: <label id="label-str">{{ getAddress($user->address) }}</label>
							</p>
							</div>
						</div>				
					</fieldset>
				
				</div><!-- /.faq-item-answer -->

				<div class="faq-item-meta">
					<button class="btn btn-primary btn-small">Save</button>
				</div><!-- /.faq-item-meta -->
			</div><!-- /.faq-item -->		
		</form>
	</div>
</div>