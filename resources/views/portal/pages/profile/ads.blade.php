<?
for( $i = 0; $i <=6; $i++ )
	$x[$i] = encode_array(['is_ajax'=>1,'identifier'=>$i]);
?>
<div class="widget p-0 b-0 m-0 profile-nav" style="background:none">
	<ul class="menu nav nav-stacked">
		<li class="nav-item p-l-xs {{ $identifier == 0 ? 'nav-item-hovered' : null }} ">
			<a href="javascript:void(0)" param-data="{{ $x[0] }}" class="nav-link nav-link-ajax"><i class="home"></i> <span>{{ $fullname }}</span></a>			
		</li>

		<li class="nav-item p-l-xs {{ $identifier == 1 ? 'nav-item-hovered' : null }} ">
			<a href="javascript:void(0)" param-data="{{ $x[1] }}" class="nav-link nav-link-ajax"><i class="recommend"></i> <span>Saved Listings</span></a>		
		</li>

		<li class="nav-item p-l-xs {{ $identifier == 2 ? 'nav-item-hovered' : null }}">
			<a href="javascript:void(0)" param-data="{{ $x[2] }}" class="nav-link nav-link-ajax"><i class="exclamation"></i> <span>Reported Listings</span></a>		
		</li>

		<li class="nav-item p-l-xs {{ $identifier == 3 ? 'nav-item-hovered' : null }}">
			<a href="javascript:void(0)" param-data="{{ $x[3] }}" class="nav-link nav-link-ajax"><i class="history"></i> <span>Recently Viewed</span></a>		
		</li>
		
	</ul><!-- /.nav -->
</div><!-- /.widget -->				

@if( Auth::user()->is_user_advertiser != 2 )
<div class="widget p-0 b-0 m-0 m-t-md profile-nav" style="background:none">
	<ul class="menu nav nav-stacked">
		<li class="nav-item p-l-xs {{ $identifier == 5 ? 'nav-item-hovered' : null }} ">
			<a href="javascript:void(0)" param-data="{{ $x[5] }}" class="nav-link nav-link-ajax"><i class="user-tag"></i> <span>Join as Advertiser</span></a>			
		</li>
	</ul><!-- /.nav -->
</div><!-- /.widget -->		
@endif


@if( Auth::user()->is_user_advertiser == 2 )
<div class="widget p-0 b-0 m-0 m-t-md profile-nav" style="background:none">
	<ul class="menu nav nav-stacked">
		<li class="nav-item p-l-xs {{ $identifier == 6 ? 'nav-item-hovered' : null }} ">
			<a href="javascript:void(0)" param-data="{{ $x[6] }}" class="nav-link nav-link-ajax"><i class="question"></i> <span>Help & Support</span></a>			
		</li>
		<li class="nav-item p-l-xs {{ $identifier == 4 ? 'nav-item-hovered' : null }}">
			<a href="javascript:void(0)" param-data="{{ $x[4] }}" class="nav-link nav-link-ajax"><i class="market"></i> <span>My Page</span></a>
		</li>					
	</ul><!-- /.nav -->
</div><!-- /.widget -->	


<div class="widget p-0 b-0 m-0 m-t-md profile-nav" style="background:none">
	<ul class="menu nav nav-stacked">
		<li class="nav-item p-l-xs {{ $identifier == 7 ? 'nav-item-hovered' : null }} ">
			<a href="{{url('ads/single')}}" param-data="{{ $x[7] }}" class="nav-link"><i class="plus"></i> <span>Add Listing</span></a>			
		</li>
		@if( $identifier == 8 )
			<li class="nav-item p-l-xs {{ $identifier == 8 ? 'nav-item-hovered' : null }} ">
				<a href="{{url('ads/single/'.$record->id)}}" param-data="{{ $x[8] }}" class="nav-link"><i class="edit-realtime"></i> <span>{{$record->title}}</span></a>			
			</li>		
		@endif
	</ul><!-- /.nav -->
</div><!-- /.widget -->		
@endif