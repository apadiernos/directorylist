@if( $identifier == 4 && Auth::user()->is_user_advertiser == 2 )
	<h4><a href="{{ url('ads/single') }}" class="btn btn-primary btn-medium align-right"><i class="fas fa-plus"></i> Add Listing</a></h4>
@endif
<?
$optionsRating = array(
	'displayOnly'=>true,
	'size'=>'xs',
	'step'=>1,
	'starCaptions'=> [1=> ' ', 2=> ' ', 3=> ' ', 4=> ' ', 5=> ' '],
	'starCaptionClasses'=> [1=> 'text-danger', 2=> 'text-warning', 3=> 'text-info', 4=> 'text-primary', 5=> 'text-success'],
	'emptyStar'=>'<i class="far fa-star"></i>',
	'filledStar'=>'<i class="fas fa-star"></i>',
);						
?>	
<div id="list-container" class="clearfix">
	@foreach( $records as $record ) 	
		<div class="col-sm-4">
			<?
			$record->list = $record->list ? $record->list : $record;
			$url_list = url('lists/'.$record->list->category->slug_term.'/'.$record->list->slug_term);
			$url_category = url('lists/'.$record->list->category->slug_term);
			if( $identifier == 4 ){
				$url_list = $url_category = url('ads/single/'.$record->list->id);
			}
			$attach = $record->list->attachments  ? get_attachment_url_by_record($record->list->attachments->first(),'listings','thumbnail') : 'assets/img/tmp/card-1.jpg';
			$params = [
				'attach' => $attach,
				'url' => ['list' => $url_list,'category'=> $url_category],
				'rating' =>$record->list->rating ? $record->list->rating->first()->aggregate : 0,
				'category' => $record->list->category->category ,
				'title' => $record->list->title ,
				'optionsRating' => $optionsRating ,
				'btn_label' => $identifier == 4  ? 'Edit' : 'View',
			];
			?>
			@include('portal.pages.item', $params)		
		</div>
	@endforeach 


</div>

<div class="center m-b-md">
	<a href="#"   class="btn btn-primary btn-medium load-more btn-list w-100">Load More</a>
</div><!-- /.center -->