<div id="ad-share-modal" class="modal fade" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title text-white"><i class="fas fa-share-alt"></i> Social Media</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="alert alert-danger p-xs text-center alert-danger-location alert-location" style="display: none" role="alert">Error Processing Request!</div>
					<div class="alert alert-success p-xs text-center alert-success-location alert-location" style="display: none" role="alert">Request successfully submitted.</div>					
					<form method="post" id="ad-location" data-plugin="validate">
						<div class="col-md-12 col-sm-12">
							<?
							$encoded_data = encode_array(['user_id'=>$user->id,'id'=>$record->id,'is_ajax'=>1]);
							$sm = [
								['Facebook','fa-facebook'],
								['Youtube','fa-youtube'],
								['Twitter','fa-twitter'],
								['Instagram','fa-instagram'],
								['Google-Plus','fa-google-plus-g'],
							];

							?>

							<input type="hidden" name="encoded_data" value="{{$encoded_data}}" />	
							@for( $i=0; $i<=4; $i++ )
								<div class="form-group">
									<label><i class="fab {{$sm[$i][1]}} bg-hover-{{strtolower($sm[$i][0])}} icon-zoom p-sm text-white"></i> {{ $sm[$i][0] }}</label>
									<input type="url" name="socialmedia[0][content][{{$i}}]" value="{{ $record->socialmedia[0]['content'][$i] }}" class="form-control" >
									<input type="hidden" name="socialmedia[0][icon][{{$i}}]" value="fab {{$sm[$i][1]}}" />
								</div><!-- /.form-group -->							
							@endfor						
																					
						</div>
					</form>
				</div><!-- /.row -->				
			</div><!-- .modal-body -->
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal"> Close</button>
				<button type="button" class="btn btn-success save-ad-location" >Save</button>
			</div><!-- .modal-footer -->

		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>