<div id="ad-post-modal" class="modal fade" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bg-success">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title text-white"><i class="fas fa-images"></i> Post</h4>
			</div>

			<div class="modal-body">
				<div class="alert alert-danger p-xs text-center alert-danger-post alert-post" style="display: none" role="alert">Error Processing Request!</div>
				<div class="alert alert-success p-xs text-center alert-success-post alert-post" style="display: none" role="alert">Request successfully submitted.</div>					
				<form method="post" id="posts" data-plugin="validate" enctype="multipart/form-data">		
					<div class="form-group">
						<label>Post Title</label>
						<input type="text" name="title" value="" class="form-control required" required>						
					</div><!-- /.form-group -->
					<?
						$optionsEmojione = array(
							'pickerPosition'=>'bottom',
							'filtersPosition'=>'bottom',
							'tonesStyle'=> 'checkbox',
							'search'=> false,
						);	
					?>				
					<div class="form-group">
						<label>{{ __('listings__category.description') }}</label>
						<textarea class="form-control"  rows="3" data-options="<?=clean_jsonString($optionsEmojione)?>" data-plugin="emojioneArea" name="description" rows="5"></textarea>
					</div><!-- /.form-group -->	
					
					<div class="row">
						<div class="col-md-12">
							<div class="dropzone dz-clickable">
								<div class="dz-message">
									<div class="fallback">
										<input name="gallery[]" type="file" multiple />
									</div>
									<h3 class="m-h-lg m-b-0">{{ __('listings__category.drop_upload') }}</h3>
									<p class="m-b-lg text-muted">({{ __('listings__category.gallery') }})</p>
								</div>
							</div>	
							<label>3 images only</label>	
						</div>
					</div>
										
				</form>	

			</div><!-- .modal-body -->
			<div class="modal-footer">
				<button type="button" class="btn btn-danger ad-post-modal-close"> Close</button>
				<button type="button" class="btn btn-success ad-post-modal-save" >Save</button>
			</div><!-- .modal-footer -->

		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>