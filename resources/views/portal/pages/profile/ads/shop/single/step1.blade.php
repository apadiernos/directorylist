<div class="widget widget-background-white">
	<form method="post" action="{{ url('listings') }}" data-plugin="validate" >	
		@csrf
		<?
		$encoded_data = encode_array(['user_id'=>$user->id,'id'=>$record->id,'redirect'=>1,'is_ajax'=>1]);	
		?>
		<input type="hidden" name="encoded_data" value="{{$encoded_data}}" />
		<fieldset>
			<div class="col-md-12 col-sm-12">
				<div class="form-group">
					<label>{{ __('listings__category.title') }}</label>
					<input type="text" name="title" value="{{ $record->title }}" class="form-control required" required>
				</div><!-- /.form-group -->
				<div class="form-group">
					<label>{{ trans_choice('listings__category.email',1) }}</label>
					<input type="email" name="email" value="{{ $record->email ? $record->email : $user->email }}" class="form-control required email" required>
				</div><!-- /.form-group -->		
				<div class="form-group">
					<label>{{ trans_choice('listings__category.website',1) }}</label>
					<input type="text" name="website" value="{{ $record->website }}" class="form-control">
				</div><!-- /.form-group -->							
				<?
					$optionsEmojione = array(
						'pickerPosition'=>'bottom',
						'filtersPosition'=>'bottom',
						'tonesStyle'=> 'checkbox',
						'search'=> false,
					);	
				?>				
				<div class="form-group">
					<label>{{ __('listings__category.description') }}</label>
					<textarea class="form-control"  rows="3" data-options="<?=clean_jsonString($optionsEmojione)?>" data-plugin="emojioneArea" name="description" rows="5">{{ $record->description }}</textarea>
				</div><!-- /.form-group -->										
				<div class="form-group">
					<label>{{ trans_choice('listings__category.category',1) }}</label>
					<select name="category_id" data-plugin="select2" class="form-control required" required>
						@foreach ($categories as $key => $category)
							<?
							$selected = $record->category_id ==  $category->id ? 'selected="selected"' : null;
							?>
							<option value="{{ $category->id }}" {{ $selected }}>{{ $category->category }}</option>
						@endforeach
					</select>
				</div><!-- /.form-group -->	
				<div class="form-group">
					<label>Tags</label>
					<select name="tags[]" class="form-control required tags" multiple required>
						@foreach ($record->tags as $key => $tag)
							<option value="{{ $tag->tag->id }}" selected="selected">{{ $tag->tag->tag }}</option>
						@endforeach
					</select>
				</div><!-- /.form-group -->					
			</div>													
		</fieldset>
		<div class="center m-t-0 m-b-md">
			<button class="btn btn-primary btn-medium w-100">Save</button>
		</div>		
	</form>
</div>	

@if( $record->id )
	@include('portal.pages.profile.ads.shop.single.modal.wrongtag')
@endif