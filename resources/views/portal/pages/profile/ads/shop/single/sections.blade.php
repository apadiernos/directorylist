<div class="widget widget-background-white">
	<?
	$optionsCarousel = array(
		'items'=>1,
		'nav'=>false,
	);			
	?>			
	<form method="post" action="{{ url('listingposts') }}" data-plugin="validate">
		@csrf
		<div class="sections-container" data-plugin="owlCarousel" data-options="<?=clean_jsonString($optionsCarousel)?>" >	
			@for($i = 1; $i <= 3; $i++)
				@include('portal.pages.profile.ads.shop.single.template',['temp_id'=>$i])
			@endfor
		</div><!-- /.sections-container -->
		
		<div class="center m-t-0 m-b-md">
			<button class="btn btn-primary btn-medium w-100 m-b-xs">Save</button>
		</div>	
	</form>		
</div>			