<div class="item">
	<fieldset>
		<div class="col-md-12 col-sm-12">
			<div class="form-group">
				<?
				$postRecord = null;
				$post_arr = $commonarr = array();
				$commonarr = ['user_id'=>$user->id,'listing_id'=>$record->id,'is_ajax'=>1,'is_redirect'=>1];
				$post_unique = array_merge($commonarr,['temp_post_id'=>$temp_id]);
				$post_arr['unique_id'] = encode_data_short(json_encode($post_unique));
				foreach ($record->posts as $post){
					if($post->unique_id == $post_arr['unique_id']){
						$postRecord = $post;
						$post_arr['id'] = $post->id;
						break; 
					}
				}
				$encoded_data = encode_array(array_merge($commonarr,$post_arr));				
				?>
				<label>{{ __('listings__category.title') }}</label>
				<input type="text" name="{{$temp_id == 1 ? 'title' : ''}}" value="{{$postRecord->title ?? ''}}" class="form-control title {{$temp_id == 1 ? 'required' : ''}}">				
				<input type="hidden" name="{{$temp_id == 1 ? 'encoded_data' : ''}}" value="{{$encoded_data}}" class="encoded_data" />					
			</div><!-- /.form-group -->
			

		</div>
		@for($i = 1; $i <= 4; $i++)
		<div class="col-md-3 col-sm-3" >
			<?	
			$img = url('assets/img/tmp/card-1.jpg');
			$subpostRecord = null;
			$subpostrecord = $subpost_arr = array();
			$subpost_arr['temp_post_id'] = $temp_id;
			$subpost_arr['temp_subpost_id'] = $i;
			$sub_post_unique = array_merge($commonarr,$subpost_arr);
			$subpost_arr['unique_id'] = encode_data_short(json_encode($sub_post_unique));
			if( $postRecord )
				$subpost_arr['listing_post_id'] = $postRecord->id; 	
			$subpost_arr['unique_post_id'] = $post_arr['unique_id'];
			foreach ($record->subposts as $subpost){
				if($subpost->unique_id == $subpost_arr['unique_id']){
					$subpostRecord = $subpost;
					$subpost_arr['id'] = $subpost->id;
					if($subpost->attachments)
						$img = get_attachment_url_by_record($subpost->attachments->first(),'subposts','thumbnail');
					break; 
				}					
			}
			$encoded_data = encode_array(array_merge($commonarr,$subpost_arr));
			$subpostrecord = [
				'title'=>$subpostRecord->title ?? '',
				'description'=>$subpostRecord->description ?? '',
			];
			$options = array(
				'url'=>url('attachments'),
				'geturl'=>url('attachments/get'),
				'removeurl'=>url('attachments/delete'),
				'module'=>'subposts',
				'module_id'=>$subpostRecord ? $subpostRecord->id : $subpost_arr['unique_id'],
				'addeddata'=>['user_id'=>Auth::id(),'filedirectory'=>'subposts','allow_multiple'=>1],
				'maxFiles'=>3,
				'callbacks'=>[
					'success'=>'callback_success',
					'sending'=>'callback_processing',
				],				
			);			
			?>			
			<div class="subposts" style="background-image: url({{$img}}); background-size: cover;" {{!$subpostRecord ? 'bg-data="1"' :''}}>
				<span class="title">{{$subpostRecord->title ?? 'Post Title'}}</span>
				<input type="hidden" name="encoded_data_subpost" value="{{$encoded_data}}" />	
				<input type="hidden" name="dz_options" value="{{json_encode($options)}}" />	
				<input type="hidden" name="data" value="{{json_encode($subpostrecord)}}" />	
			</div>
		</div>
		@endfor	
	</fieldset>	
</div>
