<div id="ad-gallery-modal" class="modal fade" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bg-success">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title text-white"><i class="fas fa-images"></i> Gallery</h4>
			</div>

			<div class="modal-body">
				<?
				$options = array(
					'url'=>url('attachments'),
					'geturl'=>url('attachments/get'),
					'removeurl'=>url('attachments/delete'),
					'module'=>'listings',
					'module_id'=>$record->id,
					'addeddata'=>['user_id'=>Auth::id(),'filedirectory'=>'listings','allow_multiple'=>1],
					'maxFiles'=>5,
				);			
				?>		
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-12">
							<div class="dropzone dz-clickable" data-options="<?=clean_jsonString($options)?>">
								<div class="dz-message">
									<div class="fallback">
										<input name="gallery[]" type="file" multiple />
									</div>
									<h3 class="m-h-lg">{{ __('listings__category.drop_upload') }}</h3>
								</div>
							</div>	
							<label>image size at least 825x408</label>	
						</div>
					</div>
				</div>

			</div><!-- .modal-body -->
			<div class="modal-footer">
				<button type="button" class="btn btn-primary ad-gallery-modal-close"> Save</button>
			</div><!-- .modal-footer -->

		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>