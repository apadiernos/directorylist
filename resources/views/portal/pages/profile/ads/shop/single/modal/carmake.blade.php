<div id="ad-carmakes-modal" class="modal fade" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-info">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title text-white"><i class="fa fas fa-car"></i> Car Makes</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="alert alert-danger p-xs text-center alert-danger-carmake alert-carmake" style="display: none" role="alert">Error Processing Request!</div>
					<div class="alert alert-success p-xs text-center alert-success-carmake alert-carmake" style="display: none" role="alert">Request successfully submitted.</div>					
					<form method="post" id="ad-carmake" data-plugin="validate">
						<div class="col-md-12 col-sm-12">
							<div class="form-group">
								<?
								$encoded_data = encode_array(['user_id'=>$user->id,'listing_id'=>$record->id,'redirect'=>1,'is_ajax'=>1]);	
								?>
								<input type="hidden" name="encoded_data" value="{{$encoded_data}}" />								
								<label>Car Makers</label>
								<select name="car_makes[]" class="form-control required carmakes" multiple required>
									@foreach ($record->carmakes as $key => $carmake)
										<option value="{{ $carmake->carmake->id }}" selected="selected">{{ $carmake->carmake->car_maker }}</option>
									@endforeach
								</select>
							</div><!-- /.form-group -->																				
						</div>
					</form>
				</div><!-- /.row -->				
			</div><!-- .modal-body -->
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal"> Close</button>
				<button type="button" class="btn btn-success save-ad-carmake" >Save</button>
			</div><!-- .modal-footer -->

		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>