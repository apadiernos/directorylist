<div id="ad-location-modal" class="modal fade" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bg-danger">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title text-white"><i class="fas fa-map-marked-alt"></i> Location</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="alert alert-danger p-xs text-center alert-danger-location alert-location" style="display: none" role="alert">Error Processing Request!</div>
					<div class="alert alert-success p-xs text-center alert-success-location alert-location" style="display: none" role="alert">Request successfully submitted.</div>					
					<form method="post" id="ad-location" data-plugin="validate">
						<div class="col-md-12 col-sm-12">
							<?
							$encoded_data = encode_array(['user_id'=>$user->id,'id'=>$record->id,'is_ajax'=>1]);	
							?>
							<input type="hidden" name="encoded_data" value="{{$encoded_data}}" />							
							<div class="form-group">
								<label>{{ __('listings__category.address') }}</label>
								<input type="text" name="address" value="{{ $record->address }}" class="form-control required" required>
							</div><!-- /.form-group -->
							<?
							$data = array(
								'country'=>$record->country,
								'state'=>$record->state,
								'city'=>$record->city,
								'brgy'=>$record->brgy,
								'options'=>['dropdownParent'=>'#ad-location-modal'],
							);
							?>
							{{ getCountriesCitiesHtm($data) }}			

							<div class="row">

								<div class="col-sm-12">
								<p class="m-b-0">
								Use this link to find lat/long :<a href="https://www.latlong.net/" target="_blank">https://www.latlong.net/</a>
								</p class="m-b-0">
								Paste this address: <label id="label-str">{{ getAddress($user->address) }}</label>
								</p>
								</div>
							</div>								
							
							<div id="map-leaflet" data-plugin="leaflet" data-options=""  style="height: 300px;"></div>
								<div class="col-sm-6">
									<div class="form-group">
										<label>{{ __('listings__category.lattitude') }}</label>
										<input name="lat" id="lat" value="{{ $user->address->lat }}" type="text" class="form-control" >
									</div><!-- /.form-group -->
								</div><!-- /.col-* -->

								<div class="col-sm-6">
									<div class="form-group">
										<label>{{ __('listings__category.loghitude') }}</label>
										<input name="long" id="long" value="{{ $user->address->long }}" type="text" class="form-control" >
									</div><!-- /.form-group -->
								</div><!-- /.col-* -->
						</div>
					</form>
				</div><!-- /.row -->

			</div><!-- .modal-body -->
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal"> Close</button>
				<button type="button" class="btn btn-success save-ad-location" >Save</button>
			</div><!-- .modal-footer -->

		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>