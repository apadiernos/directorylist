<div class="manage-ad-single">
	<h4>
		{{ $ad_title }}

		@if( $record->id )
			<ul class="steps">
				<li>
					<a href="#"  class="btn btn-danger btn-medium location-modal" ><i class="fas fa-map-marked-alt"></i></a>
				</li>		
				<li>
					<a href="#"   class="btn btn-success btn-medium ad-gallery-modal-open"><i class="fas fa-images"></i></a>
				</li>
				<li>
					<a href="{{ url('/ads/single/'.$record->id.'/sections') }}"   class="btn btn-info btn-medium"><i class="fab fa-buromobelexperte"></i></a>
				</li>
				<li>
					<a href="#"   class="btn btn-primary btn-medium" data-toggle="modal" data-target="#ad-share-modal"><i class="fas fa-share-alt"></i></a>
				</li>
				<li>
					<a href="#"   class="btn btn-info btn-medium" data-toggle="modal" data-target="#ad-carmakes-modal"><i class="fa fas fa-car"></i></a>
				</li>
				@if( $record->status == 1 )
					<li class="m-l-lg">
						<a href="{{url('lists/'.$record->category->slug_term.'/'.$record->slug_term)}}"  target="_blank" class="btn btn-success btn-medium"><i class="far fa-eye"></i></a>
					</li>	
				@endif
				<li {{ $record->status == 0 ?  'class="m-l-lg"' : null }}>
					<a href="#"   class="btn btn-danger btn-medium" data-toggle="modal" data-target="#ad-delete-modal"><i class="fas fa-trash"></i></a>
				</li>	

			</ul>
		@endif
	</h4>

	<div class="shop-single-container">
		@include('portal.pages.profile.ads.shop.single.'.$ad_page)
		
		@if( $record->id )
			@include('portal.pages.profile.ads.shop.single.modal.location')
			@include('portal.pages.profile.ads.shop.single.modal.carmake')
			@include('portal.pages.profile.ads.shop.single.modal.share')
			@include('portal.pages.profile.ads.shop.single.modal.photos')
			@include('portal.pages.profile.ads.shop.single.modal.delete')
			@include('portal.pages.profile.ads.shop.single.modal.subposts')
		@endif
	</div>
</div>