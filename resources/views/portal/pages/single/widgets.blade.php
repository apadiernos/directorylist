<div class="focus-modal">
	<div class="custom-form-header">Inquire Form</div>
	<div class="widget widget-background-white">
		<div class="alert alert-danger p-xs text-center alert-danger-inquiry" style="display: none" role="alert">Error Processing Request!</div>
		<div class="alert alert-success p-xs text-center alert-success-inquiry" style="display: none" role="alert">Request successfully submitted.</div>
		<form method="post" id="inquiry" data-plugin="validate">
			
			@if( !Auth::user() )
				<div class="form-group">
					<label>Full Name</label>
					<input type="text" name="full_name" class="form-control required" required>
				</div><!-- /.form-group -->

				<div class="form-group">
					<label>E-mail</label>
					<input type="email" name="email" class="form-control required" required>
				</div><!-- /.form-group -->
			@endif
			
			<input type="hidden" name="encoded_data" value="{{ encode_array(['is_ajax'=>1,'id'=>$record->id,'url'=>$record->category->slug_term]) }}">
			<div class="form-group">
				<label>Subject</label>
				<input type="text" name="subject" class="form-control required" required>
			</div><!-- /.form-group -->                

			<div class="form-group">
				<label>Message</label>
				<textarea class="form-control required" name="message" required rows="4"></textarea>
			</div><!-- /.form-group -->                                
			
			@if( !Auth::user() )
				<div class="form-group">
					<p>
						<img src="{{ captcha_src() }}" alt="captcha" class="captcha-img" data-refresh-config="default">
						<i class="fa fa-refresh fa-2x refresh-captcha" aria-hidden="true" style="padding: 0 15px;cursor: pointer;"></i>
					</p>
					<input type="number" name="captcha" class="form-control required captcha" required>
				</div><!-- /.form-group -->
			@endif
			
			<div class="form-group-btn">
				<button type="submit" class="btn btn-primary btn-block btn-large send-email">Send Message</button>
			</div><!-- /.form-group-btn -->
		</form>
	</div><!-- /.widget -->
</div><!-- /.widget -->	


	   
<div class="custom-form-header m-t-md">Recent Listings</div>         
<div class="widget ">
	<div class="cards-small-wrapper">
		@foreach($new_listings as $listing)
			<?
			$attachment = $listing->attachments()->first();
			$attach = $attachment != null ? get_attachment_url_by_record($attachment,'listings','square2') : 'assets/img/tmp/card-1.jpg';
			?>
			<div class="card-small">
				<div class="card-small-image">
					<a href="{{ url('lists/'.$listing->category->slug_term.'/'.$listing->slug_term) }}" style="background-image: url('{{ $attach }}');"></a>
				</div><!-- /.card-small-image -->

				<div class="card-small-content">
					<h3><a href="{{ url('lists/'.$listing->category->slug_term.'/'.$listing->slug_term) }}">{{ $listing->title }}</a></h3>		
					<h4><a href="{{ url('lists/'.$listing->category->slug_term) }}">{{ $listing->category->category }}</a></h4>
				</div><!-- /.card-small-content -->
			</div><!-- /.card-small -->
		@endforeach
	</div><!-- /.card-small -->
</div><!-- /.card-small -->
				