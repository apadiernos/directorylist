<div class="row m-t-md">
	<div class="col-md-8 col-lg-9">
		<div class="m-b-0">
			<div class="listing-detail">
					<?
						$attach = $recordcat->attachments ? get_attachment_url($recordcat->attachments,'listings') : null;
						if( !count($attach) )	
							$attach[] = 'assets/img/tmp/card-1.jpg';					
						$optionsCarousel = array(
							'items'=>1,
							'nav'=>count($attach) > 1 ? true : false,
							'navText'=>['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>']
						);	
					?>			
				@if( Auth::user() )
					<div class="extra-controls">
						<div class="owl-like bg-hover-customizer-indigo icon-zoom {{ $is_liked }}" data-params="{{ encode_array(['listing_id'=>$record->id,'identifier'=>1]) }}"><i class="fa fas fa-grin-hearts fz-lg"></i></div>
						<div class="owl-report bg-hover-red icon-zoom {{ $is_disliked }}" data-params="{{ encode_array(['listing_id'=>$record->id,'identifier'=>2]) }}"><i class="fa fas fa-heart-broken fz-lg"></i></div>
					</div>	
				@endif						
				<div class="gallery" data-plugin="owlCarousel" data-options="<?=clean_jsonString($optionsCarousel)?>" >							
					@foreach($attach as $file)
						<div class="gallery-item" style="background-image: url('{{ $file }}');"> 
							<div class="gallery-item-description">
								{{ $record->title }}
							</div><!-- /.gallery-item-description -->
						</div><!-- /.gallery-item -->            
					@endforeach  
				</div><!-- /.gallery -->

			</div><!-- /.listing-detail -->
		</div><!-- /.push-top-bottom -->
	</div><!-- /.col-sm-9 -->

	<div class="col-md-4 col-lg-3">
		<div class="sidebar p-t-0">
			<div class="custom-form-header">About</div>
			<div class="widget">
				<div class="overview">        
					<ul>
						<li><strong>{{ trans_choice('listings__category.location',1) }}</strong><span>{{ $record->address }}</span></li>
						<li><strong>{{ trans_choice('listings__category.category',1) }}</strong><span>{{ $recordcat->category }}</span></li>
						<li><strong>Rating</strong><span>{{ $reviews_avg }} by {{ $reviews_count }} users</span></li>
						@if( isset($record->website) )
							<li><strong>{{ trans_choice('listings__category.website',1) }}</strong><span><a href="{{ $record->website }}">{{ $record->website }}</a></span></li>                    
						@endif
					</ul>
				</div><!-- /.overview -->
			</div><!-- /.widget -->
			
		</div><!-- /.sidebar -->
	</div><!-- /.col-* -->
</div><!-- /.row -->