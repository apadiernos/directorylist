<div id="comment-modal" class="modal fade" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title">Writing reply</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<form method="post" id="review-reply" data-plugin="validate">
						<div class="col-sm-6">
							<div class="form-group">
								<label>Name</label>
								<input type="text" name="name" class="form-control required" required>
								<input type="hidden" name="listing_id" value="{{ $record->id }}">
								<input type="hidden" name="id" class="review-id">
							</div><!-- /.form-group -->
						</div><!-- /.col-* -->
						<div class="col-sm-6">
							<div class="form-group">
								<label>E-mail</label>
								<input type="email" name="email" class="form-control">
							</div><!-- /.form-group -->
						</div><!-- /.col-* -->
						<div class="col-sm-12">
							<div class="form-group">
								<p>
									<img src="{{ captcha_src() }}" alt="captcha" class="captcha-img" data-refresh-config="default">
									<i class="fa fa-refresh fa-2x refresh-captcha" aria-hidden="true" style="padding: 0 15px;cursor: pointer;"></i>
								</p>
								<input id="captcha-comment" type="number" name="captcha" class="form-control required captcha" required>
							</div><!-- /.form-group --> 
						</div><!-- /.col-* -->
					</form>
				</div><!-- /.row -->

			</div><!-- .modal-body -->
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal"> Close</button>
				<button type="button" class="btn btn-success verify-comment" >Post</button>
			</div><!-- .modal-footer -->

		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>

<div id="delete-modal" class="modal fade" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title">Delete</h4>
			</div>

			<div class="modal-body">
				Are you sure you want to delete this <span>Post</span>?
			</div><!-- .modal-body -->
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal"> Cancel</button>
				<button type="button" class="btn btn-success verify-delete-comment" >Yes</button>
			</div><!-- .modal-footer -->

		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>



<div id="edit-post-modal" class="modal fade" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title">Edit Post</h4>
			</div>

			<div class="modal-body">
				<form method="post" id="edit-review" data-plugin="validate">
					<input type="hidden" name="encoded_data" value="">
					@include('portal.pages.single.postform')
				</form>
			</div><!-- .modal-body -->
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal"> Cancel</button>
				<button type="button" class="btn btn-success submit-review" >Save</button>
			</div><!-- .modal-footer -->

		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>


<div id="report-post-modal" class="modal fade" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title">Report Post</h4>
			</div>

			<div class="modal-body">
				<div class="alert alert-danger p-xs text-center alert-danger-report" style="display: none" role="alert">Error Processing Request!</div>
				<div class="alert alert-success p-xs text-center alert-success-report" style="display: none" role="alert">Request successfully submitted.</div>				
				<form method="post" id="report-post" data-plugin="validate">
					<div class="form-group">
						<input type="hidden" name="id" class="review-id">										
						<label>Message</label>
						<?
							$optionsEmojione = array(
								'pickerPosition'=>'bottom',
								'filtersPosition'=>'bottom',
								'tonesStyle'=> 'checkbox',
								'search'=> false,
							);	
						?>				
						<textarea class="form-control"  rows="3" data-options="<?=clean_jsonString($optionsEmojione)?>" data-plugin="emojioneArea" name="report_message" rows="5"></textarea>
					</div><!-- /.form-group -->
				</form>
			</div><!-- .modal-body -->
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal"> Cancel</button>
				<button type="button" class="btn btn-success submit-report" >Report</button>
			</div><!-- .modal-footer -->

		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>