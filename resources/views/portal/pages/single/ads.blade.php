
			<div class="widget p-0 b-0 m-0" style="background:none">
				<ul class="menu nav nav-stacked">
					<li class="nav-item p-l-xs {{ $identifier == 0 ? 'nav-item-hovered' : null }}">
						<a href="{{ url('lists/'.$record->category->slug_term.'/'.$record->slug_term) }}" class="nav-link"><i class="market"></i> <span>{{ excerpt($record->title,20) }}</span></a>			
					</li>
					<li class="nav-item p-l-xs">
						<a href="{{ url('/') }}" class="nav-link"><i class="home"></i> <span>Karnid.CoM</span></a>			
					</li>
					@if( $record->posts )
						@foreach( $record->posts as $post )
							<li class="nav-item p-l-xs {{ $identifier == $post->id ? 'nav-item-hovered' : null }}">
								<a href="{{ url('lists/'.$record->category->slug_term.'/'.$record->slug_term.'/'.$post->slug_term) }}" class="nav-link"><i class="recommend"></i> <span>{{ excerpt($post->title,20) }}</span></a>			
							</li>
						@endforeach						
					@endif


				</ul><!-- /.nav -->

				<ul class="menu nav nav-stacked m-t-lg">
					<li class="nav-item p-l-xs">
						<a href="{{ url('lists') }}" class="nav-link"><i class="list"></i> <span>Listings</span></a>		
					</li>

					<li class="nav-item p-l-xs">
						<a href="{{ url('lists/'.$record->category->slug_term) }}" class="nav-link"><i class="single"></i> <span>{{ excerpt($record->category->category,20) }}</span></a>		
					</li>
				</ul><!-- /.nav -->
			</div><!-- /.widget -->				