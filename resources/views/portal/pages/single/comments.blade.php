@if( $post )
	<div class="listing-subposts">
		@foreach( $post->subposts as $subpost )
			@include('portal.pages.single.subposts',['subpost'=>$subpost])
		@endforeach
 	</div><!-- /.listing-detail -->					
@endif

<div class="focus-modal trim" id="feedback">
	<div class="custom-form-header">Write Feedback</div>
	<div class="comment-create clearfix">
		<form method="post" id="review" data-plugin="validate">
			<input type="hidden" name="encoded_data" value="{{ encode_array(['module'=>$module,'is_ajax'=>1,'listing_id'=>$record->id,'post_id'=>$post->id]) }}">
			@include('portal.pages.single.postform')
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group-btn">
						<button class="btn btn-primary btn-small submit-review">Post Comment</button>
						@if( !Auth::user() )
							<button class="m-l-xs btn btn-danger btn-small submit-register">Register</button>
						@endif	
					</div><!-- /.form-group-btn -->								
				</div><!-- /.col-* -->
			</div><!-- /.row -->				
		</form>
	</div><!-- /.comment-create -->
</div><!-- /#focus-modal -->	
	
<div class="custom-form-header m-t-md">Showing <span class="recent-count">{{ $currentreviewcount }}</span> out of {{ $reviews_count }} comments</div>
<div class="comments-container" <?=Auth::user() ? 'data-auth="1"' : null ?>  <?=Auth::user() ? 'data-verified="1"' : null ?> style="height: 600px; overflow: hidden" data-params="{{ encode_array(['listing_id'=>$record->id,'post_id'=>$post->id]) }}">
	@if( isset($reviews) && count($reviews) )
		<div class="scrollInner">
			<ul id="comments" class="comments">
				@include('portal.pages.comments__ajax')
			</ul>
		</div>
	@endif
</div>	