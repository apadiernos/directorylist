	<div class="row ">
		<div class="col-md-11 col-lg-12">
			<div class="">
				<div class="listing-detail">
				
					@if( $record->description )
						<div class="custom-form-header">{{ __('listings__category.description') }}</div>
						<div class="widget widget-background-white m-b-0">
							<p>{{ $record->description }}</p>
						</div>
					@endif
					<? $found = 0; ?>
					<div class="row m-t-md">
							<div class="col-md-6">	
								<div class="custom-form-header">Share this post!</div>
								<div class="widget widget-background-white p-md m-b-md">
									<?=Share::page(url('lists/'.$recordcat->slug_term.'/'.$record->slug_term), $record->title)
											->facebook()
											->messenger()
											->twitter()
											->whatsapp();										
									?>
								</div>
							</div>						
					@if( isset($record->socialmedia[0]['content'][0]) || isset($record->sections[0]['content'][0]) )
						
							<? $found = 1; ?>
								@if( isset($record->socialmedia) && count($record->socialmedia) )
									@if( isset($record->socialmedia[0]['content'][0]) && $record->socialmedia[0]['content'][0] != null )
									<div class="col-md-6 m-b-md">	
										<div class="custom-form-header">Social Connections</div>
										<div class="widget widget-background-white p-md">	
											<ul class="social">
												@foreach($record->socialmedia[0]['content'] as $key => $content)
													@if( !$content )
														@continue
													@endif												
													<li class="bg-hover-{{ str_replace('fab fa-','',$record->socialmedia[0]['icon'][$key]) }} icon-zoom">
														<a href="{{$content}}" target="_blank">
														@if( isset($record->socialmedia[0]['icon'][$key]) )
															<i class="{{$record->socialmedia[0]['icon'][$key]}}"></i>
														@endif	
														</a>
													</li>
												@endforeach
											</ul>
										</div>			
									</div>			
									@endif	
								@endif							
								@if( count($record->sections) )
									@if( isset($record->sections[0]['content'][0]) )
										@foreach($record->sections as $section)
										<div class="col-md-6 m-b-md">	
											<div class="custom-form-header">{{ $section['title'][0] }}</div>
											<div class="widget widget-background-white p-md m-b-md">
												<ul class="amenities">
													@if( isset($section['content']) )
														@foreach($section['content'] as $key => $content)
															@if( !$content )
																@continue
															@endif
															<li>
																@if( isset($section['icon'][$key]) )
																	<i class="{{$section['icon'][$key]}}"></i>&nbsp;&nbsp;
																@endif	
																{{$content}}
																@if( isset($section['price'][$key]) )
																	<strong> - ₱ {{ $section['price'][$key] }}</strong>
																@endif		
															</li>
														@endforeach
													@endif
												</ul>
											</div>	
										</div>	
										@endforeach 
									@endif	
								@endif
							
						
					@endif
					
					</div>
					
					
				</div><!-- /.listing-detail -->
			</div><!-- /.push-top-bottom -->
		</div><!-- /.col-sm-12 -->	
	</div><!-- /.row -->	