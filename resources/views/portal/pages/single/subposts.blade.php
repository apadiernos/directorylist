<div class="widget widget-background-white p-md m-b-md subpost">
	@if( !$subpost->attachments->count() && !$subpost->description )
		<div class="vertical-center empty-post bg-primary"><h1 class="text-center text-content text-white">{{ $subpost->title }}</h1></div>
	@elseif( !$subpost->attachments->count() && strlen($subpost->description) <= 100 )
		<h5 class="fz-sm fw-600 text-gray">{{ $subpost->title }}</h5>
		<div class="vertical-center empty-post bg-primary"><h1 class="text-center text-content text-white comment-message">@emojione($subpost->description)</h1></div>
	@else
		<h5 class="fz-sm fw-600 text-gray">{{ $subpost->title }}</h5>
		@if( $subpost->description )
			<p class="comment-message">@emojione($subpost->description)</p>
		@endif		
	@endif
		
	@if( $subpost->attachments->count() )
		<?
		$images = [];
		foreach( $subpost->attachments as $attachment ){
			$images[] = get_attachment_url_by_record($attachment,'subposts','large3');
		}
		$optionsimagesGrid = array('images'=>$images,'align'=>true);	
		?>
		<div class="images" data-options="<?=clean_jsonString($optionsimagesGrid)?>" data-plugin="imagesGrid"></div>
	@endif

</div>