<div class="form-group">
	<label>Rating</label>
	<?
		$optionsRating = array(
			'size'=>'md',
			'step'=>1,
			'starCaptions'=> [1=> 'Very Poor', 2=> 'Poor', 3=> 'Ok', 4=> 'Good', 5=> 'Very Good'],
			'starCaptionClasses'=> [1=> 'text-danger', 2=> 'text-warning', 3=> 'text-info', 4=> 'text-primary', 5=> 'text-success'],
			'emptyStar'=>'<i class="far fa-star"></i>',
			'filledStar'=>'<i class="fas fa-star"></i>',
		);	
	?>
	<input type="text"  name="rating" class="required" required data-plugin="rating" data-options="<?=clean_jsonString($optionsRating)?>" >
</div><!-- /.form-group -->

<div class="form-group">
	<input type="hidden" name="id" class="review-id">										
	<label>Message</label>
	<?
		$optionsEmojione = array(
			'pickerPosition'=>'bottom',
			'filtersPosition'=>'bottom',
			'tonesStyle'=> 'checkbox',
			'search'=> false,
		);	
	?>				
	<textarea class="form-control"  rows="3" data-options="<?=clean_jsonString($optionsEmojione)?>" data-plugin="emojioneArea" name="message" rows="5"></textarea>
</div><!-- /.form-group -->
@if (Auth::guest())
<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			<label>Name</label>
			<input type="text" name="name" class="form-control required" required>

		</div><!-- /.form-group -->
	</div><!-- /.col-* -->

	<div class="col-sm-6">
		<div class="form-group">
			<label>E-mail</label>
			<input type="email" name="email" required class="form-control required">
		</div><!-- /.form-group -->
	</div><!-- /.col-* -->
				
	<div class="col-sm-6">
		<div class="form-group">
			<p>
				<img src="{{ captcha_src() }}" alt="captcha" class="captcha-img" data-refresh-config="default">
				<i class="fa fa-refresh fa-2x refresh-captcha" aria-hidden="true" style="padding: 0 15px;cursor: pointer;"></i>
			</p>
			<input type="number" name="captcha" class="form-control required captcha" required>
		</div><!-- /.form-group --> 
	</div><!-- /.col-* -->

</div><!-- /.row -->
@endif