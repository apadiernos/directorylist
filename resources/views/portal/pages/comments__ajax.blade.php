@if( isset($reviews) && count($reviews) )
	@foreach($reviews as $reviewRecord)	
		<li class="<?=isset($offset) ? 'new-comments-'.$offset : null ?> parent-comment" data-count="{{ $currentreviewcount }}" data-id="{{ encode_data_short($reviewRecord['parent']->id) }}">
			<? get_comment_template($module,0,$reviewRecord['parent'],$reviewreactions); ?>
			@if( isset($reviewRecord['children']) && count($reviewRecord['children']) )	
				<ul>
					@foreach($reviewRecord['children'] as $reviewRecordChild)	
					<li class="comments-reply <?=isset($offset) ? 'new-comments' : null ?>" data-id="{{ encode_data_short($reviewRecordChild->id) }}">
					<? get_comment_template($module,1,$reviewRecordChild,$reviewreactions); ?>											
					</li>
					@endforeach
				</ul>										
			@endif	
		</li>	
	@endforeach
@endif