<div class="header-bottom">
	<div class="container">
		<div class="header-bottom-label hidden-md-down">
			I'm looking for
		</div><!-- /.header-bottom-label -->

		<ul class="nav nav-pills">
			<li class="nav-item"><a href="listings.html" class="nav-link">Properties</a></li>
			<li class="nav-item"><a href="listings.html" class="nav-link">Cars</a></li>
			<li class="nav-item"><a href="listings.html" class="nav-link">Events</a></li>
			<li class="nav-item"><a href="listings.html" class="nav-link">Food &amp; Drink</a></li>
			<li class="nav-item"><a href="listings.html" class="nav-link">Travel</a></li>
			<li class="nav-item"><a href="listings.html" class="nav-link">Hotel</a></li>
			<li class="nav-item"><a href="listings.html" class="nav-link">Restaurant</a></li>
		</ul><!-- /.nav -->

		<div class="header-bottom-more">
			<i class="md-icon">more_horiz</i>

			<ul class="header-more-menu">
				<li><a href="submit.html">Submit New</a></li>
				<li><a href="listings.html">All Listings</a></li>
				<li><a href="login.html">Login</a></li>
				<li><a href="register.html">Register</a></li>
			</ul>
		</div><!-- /.header-bottom-more -->
	</div><!-- /.container -->
</div><!-- /.header-bottom -->			