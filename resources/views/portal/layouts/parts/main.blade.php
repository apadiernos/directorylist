<div class="page-wrapper">
	<div class="header-wrapper">
		<div class="header">
			<div class="header-inner">
				<div class="header-top">
					<div class="container">
						<!-- /.logo -->
						<a href="/">
							<img src="{{ asset('assets/img/logo-k.png') }}" alt="" class="header-top-logo">
						</a>
						@include('portal.layouts.parts.headertop')
					</div><!-- /.container -->
				</div><!-- /.header-top -->				
				
			</div><!-- /.header-inner -->
		</div><!-- /.header -->
	</div><!-- /.header-wrapper -->
	
	<div class="main-wrapper">
	    <div class="main">
	        <div class="main-inner">
	            <div class="content">
					@include('portal.layouts.parts.content')
				</div><!-- /.content -->
	        </div><!-- /.main-inner -->
	    </div><!-- /.main -->
    </div><!-- /.main-wrapper -->
	@include('portal.layouts.parts.footer')
</div><!-- /.page-wrapper -->		