<div class="nav-primary-wrapper collapse navbar-toggleable-sm">
	<ul class="nav nav-pills ">
		<li class="nav-item">
			<a href="{{ url('/') }}" class="nav-link"><i class="fa fas fa-car"></i> Home</a>						
		</li>
		<li class="nav-item">
			<a href="{{ url('/lists') }}" class="nav-link "><i class="fa fas fa-list-ul"></i> Listings</a>
		</li>
	</ul><!-- /.nav -->
</div><!-- /.nav-primary-wrapper -->

<ul class="nav nav-pills secondary">
	@if (!Auth::guest())
		<li class="nav-item user-avatar-wrapper">
			<a href="#" class="nav-link circle user-avatar-image" style="background-image: url('{{ get_attachment_url_by_record(Auth::user()->profilepicture,'user','square1') }}')"></a>
			<span class="user-avatar-status"></span>
			<ul class="header-more-menu">
				<li><a href="{{ url('/profile/user') }}">Profile</a></li>
				<li><a href="{{ url('/profile/likes') }}">Saved Listings</a></li>
				<li><a href="{{ url('/logout') }}">Logout</a></li>
			</ul>							
		</li>
	@else
		<li class="nav-item">
			<a href="javascript:void(0)" class="nav-link" data-toggle="modal" data-target="#register-modal">Register</a>
		</li>
		<li class="nav-item"> <a href="javascript:void(0)" style="cursor:none" class="nav-link">|</a> </li>
		<li class="nav-item">
			<a href="javascript:void(0)" class="nav-link" data-toggle="modal" data-target="#login-modal">Login</a>
		</li>		
	@endif	
</ul>
					
<button class="navbar-toggler pull-xs-right hidden-md-up" type="button" data-toggle="collapse" data-target=".nav-primary-wrapper">
	<i class="md-icon">menu</i>
</button>	