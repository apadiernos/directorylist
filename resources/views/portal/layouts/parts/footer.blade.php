<div class="footer-wrapper">
	<div class="footer">
		<div class="footer-widgets">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-3">
						<div class="widget right-border">
							<h2 class="widgettitle"><img src="{{ asset('assets/img/logo-light-k.png') }}" alt="Materialist"></h2>

							<p>
								Mauris dictum, sem non pretium ultricies, dolor mi aliquet dolor, non facilisis arcu ligula vitae elit. Pellentesque ac varius sem. Sed pellentesque nisl vitae pellentesque consectetur.
							</p>
						</div><!-- /.widget -->
					</div><!-- /.col-* -->
					<div class="col-xs-12 col-sm-12 col-md-2 col-md-offset-1">
					</div>
					<div class="col-xs-12 col-sm-12 col-md-2 col-md-offset-1">
						<div class="widget right-border">
							<h2 class="widgettitle">Social</h2>

							<ul class="nav nav-stacked">
								<li class="nav-item"><a href="#" class="nav-link">Facebook</a></li>
								<li class="nav-item"><a href="#" class="nav-link">Twitter</a></li>
								<li class="nav-item"><a href="#" class="nav-link">LinkedIn</a></li>
								<li class="nav-item"><a href="#" class="nav-link">Google Plus</a></li>
							</ul>
						</div><!-- /.widget -->
					</div>

					<div class="col-xs-12 col-sm-12 col-md-2 col-md-offset-1">
						<div class="widget right-border">
							<h2 class="widgettitle">Pages</h2>

							<ul class="nav nav-stacked">
								<li class="nav-item"><a href="{{ url('/') }}" class="nav-link">Home</a></li>
								<li class="nav-item"><a href="{{ url('/lists') }}" class="nav-link">Listings</a></li>
							</ul>
						</div><!-- /.widget -->
					</div><!-- /.col-* -->						
		
				</div><!-- /.row -->
			</div><!-- /.container -->
		</div><!-- /.footer-widgets -->

		<div class="footer-top">
			<div class="container">
				<div class="footer-top-left">
					<strong class="hidden-xs-down">Sponsored By:</strong>
					<i class="fab fa-fedex fa"></i>
					<i class="fa fas fa-car"></i>
				</div><!-- /.footer-top-left -->

				<div class="footer-top-right">
					<!--
					<a class="footer-top-action">
						<i class="md-icon">vertical_align_top</i>
					</a><!-- /.footer-top-action -->
					
					<ul class="nav nav-pills">
						<li class="nav-item"><a class="nav-link" href="{{ url('/') }}">Home</a></li>
						<li class="nav-item"><a class="nav-link" href="{{ url('/lists') }}">Listings</a></li>
					</ul>
				</div><!-- /.footer-top-right -->
			</div><!-- /.container -->
		</div><!-- /.footer-top -->

		<div class="footer-bottom">
			<div class="container">
				<div class="footer-bottom-left">
					Created for <a href="#">Directory Listings</a>
				</div><!-- /.footer-bottom-left -->

				<div class="footer-bottom-right">
					Copyright &copy; 2019 - All Rights Reserved
				</div><!-- /.footer-bottom-right -->			
			</div><!-- /.container -->
		</div><!-- /.footer-bottom -->
	</div><!-- /.footer -->
</div><!-- /.footer-wrapper -->


<div id="register-modal" class="modal fade" tabindex="-1"  data-unique="registerform" role="dialog" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title">Register</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<form method="post" data-plugin="validate">
						<div class="col-sm-6">
							<div class="form-group">
								<label>First Name</label>
								<input type="text" name="first_name" class="form-control required" required>
							</div><!-- /.form-group -->
						</div><!-- /.col-* -->
						<div class="col-sm-6">
							<div class="form-group">
								<label>Last Name</label>
								<input type="text" name="last_name" class="form-control required" required>
							</div><!-- /.form-group -->
						</div><!-- /.col-* -->		
						<div class="col-sm-6">
							<div class="form-group">
								<label>Username</label>
								<input type="text" name="username" class="form-control required" required>
							</div><!-- /.form-group -->
						</div><!-- /.col-* -->						
						<div class="col-sm-6">
							<div class="form-group">
								<label>E-mail</label>
								<input type="email" name="email" class="form-control required" required>
							</div><!-- /.form-group -->
						</div><!-- /.col-* -->
						<div class="col-sm-12">
							<div class="form-group">
								<label>Password</label>
								<input type="password" name="password" class="form-control required" required>
							</div><!-- /.form-group -->
						</div><!-- /.col-* -->			
						<div class="col-sm-12">
							<div class="form-group">
								<label>Confirm Password</label>
								<input type="password" name="password_confirmation" class="form-control required" required>
							</div><!-- /.form-group -->
						</div><!-- /.col-* -->								
						<div class="col-sm-12">
							<div class="form-group">
								<p>
									<img src="{{ captcha_src() }}" alt="captcha" class="captcha-img" data-refresh-config="default">
									<i class="fa fa-refresh fa-2x refresh-captcha" aria-hidden="true" style="padding: 0 15px;cursor: pointer;"></i>
								</p>
								<input id="captcha-comment" type="number" name="captcha" class="form-control required captcha" required>
							</div><!-- /.form-group --> 
						</div><!-- /.col-* -->
					</form>
				</div><!-- /.row -->
				<!--
				<a href="{{url('/facebookredirect')}}" class="btn btn-primary" ><i class="fab fa-facebook"></i>  Facebook</a>
				-->
				<a href="{{url('/googleredirect')}}" class="btn btn-danger" ><i class="fab fa-google"></i>  Google</a>
			</div><!-- .modal-body -->
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-success" id="user-register" >Register</button>
			</div><!-- .modal-footer -->

		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>


<div id="login-modal" class="modal fade" tabindex="-1" data-unique="loginform" role="dialog" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title">Login</h4>
			</div>
			<div class="alert alert-danger p-xs text-center alert-danger-login" style="display: none" role="alert">
				
			</div>
			<div class="modal-body">
				<div class="row">
					<form method="post" data-plugin="validate">					
						<div class="col-sm-12">
							<div class="form-group">
								<label>E-mail</label>
								<input type="email" name="email" class="form-control required" required>
							</div><!-- /.form-group -->
						</div><!-- /.col-* -->
						<div class="col-sm-12">
							<div class="form-group">
								<label>Password</label>
								<input type="password" name="password" class="form-control required" required>
							</div><!-- /.form-group -->
						</div><!-- /.col-* -->							
				</div><!-- /.row -->
				<!--
				<a href="{{url('/facebookredirect')}}" class="btn btn-primary" ><i class="fab fa-facebook"></i>  Facebook</a>
				-->
				<a href="{{url('/googleredirect')}}" class="btn btn-danger" ><i class="fab fa-google"></i>  Google</a>
			</div><!-- .modal-body -->
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-success" id="user-login" >Login</button>
			</div><!-- .modal-footer -->

		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>

<div id="error-modal" class="modal fade" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title">Something went wrong..</h4>
			</div>

			<div class="modal-body">
				Please email us at <a href="mailto:alan.ontue@gmail">alan.ontue@gmail.com</a>
			</div><!-- .modal-body -->
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal"> Cancel</button>
				<button type="button" class="btn btn-success submit-report" >Reload Page</button>
			</div><!-- .modal-footer -->

		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>