<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
	<link rel="shortcut icon" sizes="196x196" href="{{ asset('assets/img/favicon.png') }}">
	
	<link type="text/css" rel="stylesheet" href="{{ mix('css/app.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&subset=latin,latin-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">    
    <link href="{{ asset('assets/fonts/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">  
    <link href="{{ asset('css/all.css') }}" rel="stylesheet" type="text/css" id="css-primary">
	
	@if( $is_single )
	<?
	$attach = $recordcat->attachments ? get_attachment_url($recordcat->attachments,'listings','square4') : null;
	if( !count($attach) )	
		$attach[] = 'assets/img/tmp/card-1.jpg';
	?>	
	<meta property="og:title" content="{{ $record->title }}">
	<meta property="og:description" content="{{ $record->description }}">
	<meta property="og:image" content="{{ url($attach[0]) }}">
	<meta property="og:url" content="{{ url('lists/'.$recordcat->slug_term.'/'.$record->slug_term) }}">	
	@endif
	
    <title>@yield('title')</title>
</head>
<body class="@yield('body_class')" asset_url="{{ asset('assets/') }}" base_url="{{ url('/') }}" csrf="{{ Session::token() }}">