<div class="admin-header">
	<div class="admin-header-navigation">
		<div class="container-fluid">
			<ul class="nav nav-pills">
				<li class="nav-item"><a href="{{ url('/') }}" class="nav-link">{{ __('homeportal') }}</a></li>
			</ul> 

			<ul class="nav nav-pills secondary">
				<li class="nav-item user-avatar-wrapper hidden-sm-down">
					<a href="#" class="nav-link circle user-avatar-image" style="background-image: url('assets/img/tmp/profile-1.jpg')"></a>
					<span class="user-avatar-status"></span>

					<ul class="header-more-menu">
						<!--
						<li><a href="#">Profile</a></li>
						<li><a href="#">Password</a></li>
						<li><a href="#">Submissions</a></li>
						-->
						<li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('listings__category.logout') }}</a></li>
					</ul>	
				</li>
			</ul>		
			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
				@csrf
			</form>
			<button class="navbar-toggler pull-xs-right hidden-lg-up" type="button" data-toggle="collapse" data-target=".dashboard-nav-primary">
				<i class="md-icon">menu</i>
			</button>									
		</div><!-- /.container -->
	</div><!-- /.admin-header-navigation -->
</div><!-- /admin-header -->