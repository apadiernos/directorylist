<div class="admin-content">
	<div class="container-fluid">
		<div class="admin-title">
			<h1>@yield('page_title')</h1>

			<ul class="breadcrumb">
				@section('breadcrumb')
				@show
			</ul><!-- /.breadcrumb -->	
		</div><!-- /.admin-title -->
		@section('content')
		@show		
		