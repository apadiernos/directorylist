<div class="admin-sidebar">
	<a href="/" class="hidden-md-down">
		<img src="assets/img/logo-light.png" alt="" class="admin-sidebar-logo">
	</a>

	<a href="{{url('/admin/listings/add') }}" class="btn btn-primary btn-block hidden-md-down">{{ trans_choice('listings__category.get_action',1).' '. trans_choice('listings__category.listing',1) }}</a>

	<div class="dashboard-nav-primary collapse navbar-toggleable-md">
		<ul class="nav nav-pills nav-stacked">
			
			<li class="nav-item">
				<a href="{{url('/admin/dashboard') }}" class="nav-link active"><i class="md-icon">dashboard</i> Dashboard</a>
			</li>
			
			<li class="nav-item">
				<a href="{{url('/admin/listings/categories') }}" class="nav-link "><i class="md-icon">view_list</i> {{ trans_choice('listings__category.listing',1).' '.trans_choice('listings__category.category',2) }} </a>
			</li>			
			<li class="nav-item">
				<a href="{{url('/admin/listings') }}" class="nav-link "><i class="md-icon">view_list</i> {{ trans_choice('listings__category.listing',2) }} </a>
			</li>
			<!--
			<li class="nav-item">
				<a href="admin-reviews.html" class="nav-link "><i class="md-icon">rate_review</i> {{ trans_choice('listings__category.review',2) }} </a>
			</li>

			<li class="nav-item">
				<a href="admin-users.html" class="nav-link "><i class="md-icon">people</i> {{ trans_choice('listings__category.user',2) }} </a>
			</li>
			-->
			<li class="nav-item">
				<a href="{{ route('logout') }}" class="nav-link" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="md-icon">power_settings_new</i> {{ __('listings__category.logout') }}</a>
			</li>
		</ul>
	</div>


</div><!-- /.admin-sidebar -->