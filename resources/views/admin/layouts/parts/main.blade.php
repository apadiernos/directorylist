<div class="admin-wrapper">
	@include('admin.layouts.parts.sidebar')
	<div class="admin-main">		
		@include('admin.layouts.parts.header')
		@include('admin.layouts.parts.content')
		@include('admin.layouts.parts.footer')
	</div><!-- /.admin-main -->
</div><!-- /.admin-wrapper -->		