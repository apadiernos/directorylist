<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
	<link rel="shortcut icon" sizes="196x196" href="{{ asset('assets/img/favicon.png') }}">
	
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&subset=latin,latin-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">         
    <link href="{{ mix('css/all.css') }}" rel="stylesheet" type="text/css" id="css-primary">
	
    <title>@yield('title')</title>
</head>
<body class="@yield('body_class')" base_url="{{ url('/') }}" csrf="{{ Session::token() }}">