@extends('admin.layouts.admin')

@section('body_class','layout-empty')
@section('page_title',trans_choice('listings__category.get_action',$get_action).' | '.trans_choice('listings__category.listing',2))
@section('title',trans_choice('listings__category.get_action',$get_action).' | '.trans_choice('listings__category.listing',2))

@section('breadcrumb')
	<li><a href="{{ url('admin/listings') }}">{{ trans_choice('listings__category.listing',1) }}</a> <i class="md-icon">keyboard_arrow_right</i></li>
	<li><a href="{{ url('admin/listings') }}">{{ trans_choice('listings__category.listing',2) }}</a> <i class="md-icon">keyboard_arrow_right</i></li>
	<li class="active">{{ trans_choice('listings__category.get_action',$get_action) }}</li>
@endsection

@section('js')
	@parent
	<script type="text/javascript" src="{{ asset('assets/js/custom/locations.js') }}"></script> 
	<script type="text/javascript" src="{{ asset('assets/js/custom/admin/listing.js') }}"></script> 
@endsection

@section('content')
<div class="admin-box" data-fa="<?=clean_jsonString($fa) ?>">
		<form method="post" action="{{ url('admin/listings') }}" data-plugin="validate" enctype="multipart/form-data">		
			<fieldset>
				<legend>{{ __('listings__category.basic_information') }}</legend>		
				<div class="form-group">
					<label>{{ __('listings__category.title') }}</label>
					@csrf
					<input type="hidden" name="id" class="form-control id" value="{{ $record->id }}">
					<input type="text" name="title" class="form-control  required" value="{{ $record->title }}" required>
				</div><!-- /.form-group -->

				<div class="form-group">
					<label>{{ __('listings__category.description') }}</label>
					<textarea name="description" class="form-control" rows="6">{{ $record->description }}</textarea>
				</div><!-- /.form-group -->
				
				<div class="form-group">
					<label>{{ trans_choice('listings__category.category',1) }}</label>
					<select name="category_id" class="form-control">
						@foreach ($listingCategory as $key => $category)
							<?
							$selected = $record->category_id ==  $category->id ? 'selected="selected"' : null;
							?>
							<option value="{{ $category->id }}" {{ $selected }}>{{ $category->category }}</option>
						@endforeach
					</select>
				</div><!-- /.form-group -->				
			</fieldset>
			
			<fieldset>
				<legend>{{ trans_choice('listings__category.location',1) }}</legend>
				<div class="form-group">
					<label>{{ __('listings__category.address') }}</label>
					<input name="address" value="{{ $record->address }}" type="text" name="title" class="form-control required" required>
					
				</div><!-- /.form-group -->
				
		
					<?
					$data = array(
						'country'=>$record->country,
						'state'=>$record->state,
						'city'=>$record->city,
						'brgy'=>$record->brgy,
					);
					?>
					{{ getCountriesCitiesHtm($data) }}
				
			</fieldset>

			<fieldset>
				<legend>Lat/Long</legend>

				<div class="row">
					<div id="map-leaflet" data-plugin="leaflet" data-options=""  style="height: 500px">
				</div><!-- /.row -->
				<div class="row">
				    <div class="col-sm-4">
				        <div class="form-group">
				            <label>{{ __('listings__category.lattitude') }}</label>
				            <input name="lat" id="lat" value="{{ $record->lat }}" type="text" class="form-control required" required>
				        </div><!-- /.form-group -->
				    </div><!-- /.col-* -->

				    <div class="col-sm-4">
				        <div class="form-group">
				            <label>{{ __('listings__category.loghitude') }}</label>
				            <input name="long" id="long" value="{{ $record->long }}" type="text" class="form-control required" required>
				        </div><!-- /.form-group -->
				    </div><!-- /.col-* -->
					<div class="col-sm-12">
					<p class="m-b-0">
					Use this link to find lat/long :<a href="https://www.latlong.net/" target="_blank">https://www.latlong.net/</a>
					</p class="m-b-0">
					Paste this address: <label id="label-str">{{ getAddress($record) }}</label>
					</p>
					</div>
				</div>				
			</fieldset>
			
			
			<fieldset>
				<legend>{{ __('listings__category.contact_information') }}</legend>

				<div class="row">
				    <div class="col-sm-4">
				        <div class="form-group">
				            <label>{{ trans_choice('listings__category.email',1) }}</label>
				            <input name="email" value="{{ $record->email }}" type="text" class="form-control required" required>
				        </div><!-- /.form-group -->
				    </div><!-- /.col-* -->

				    <div class="col-sm-4">
				        <div class="form-group">
				            <label>{{ trans_choice('listings__category.website',1) }}</label>
				            <input name="website" value="{{ $record->website }}" type="text" class="form-control">
				        </div><!-- /.form-group -->
				    </div><!-- /.col-* -->
					
				</div>
			</fieldset>
			
			<fieldset>
				<legend>{{ __('listings__category.social_media') }}</legend>
					@if( $record->socialmedia )	
						@foreach( $record->socialmedia[0]['content'] as $key => $content  )
							<?
							$data = array(
								'idx'=>0,
								'key'=>'socialmedia',
								'content'=>$content,
								'icon'=>$record->socialmedia[0]['icon'][$key],
								'price'=>$record->socialmedia[0]['price'][$key],
							);
							echo '<div class="row section-row-template">'.row_templates($fa,$data).'</div>';
							?>
						@endforeach	
					@else
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label>{{ __('listings__category.content') }}</label>
								<input type="text" name="socialmedia[0][content][]" prop="content" class="form-control">
							</div><!-- /.form-group -->
						</div><!-- /.col-* -->							
						<div class="col-sm-4">
							<div class="form-group">
								<label>{{ trans_choice('listings__category.icon',1) }}</label>
								<?
								$optionsFa = array(
									'manual_init'=>1,
								);			
								?>								
								<select class="form-control faSelect2" name="socialmedia[0][icon][]" data-plugin="select2" style="width:100%" prop="icon" data-options="<?=clean_jsonString($optionsFa)?>">	
									<option value="">{{ __('listings__category.no') }} {{ trans_choice('listings__category.icon',1) }}</option>
									@foreach ($fa as $ico => $icon)
										@if (strpos($ico,'fab') !== false)
											<option value="{{ $ico }}" data-icon="{{ $ico }}">{{ $ico }}</option>
										@endif	
									@endforeach
								</select>
							</div><!-- /.form-group -->
						</div><!-- /.col-* -->
			
						<div class="col-sm-2">
							<div class="center">
								<button class="btn btn-primary btn-xs btn-add-row" obj="socialmedia" data-idx="0"><i class="md-icon m-0">add</i></button>
								<button class="btn btn-danger btn-xs btn-remove-row"><i class="md-icon m-0">remove</i></button>
							</div><!-- /.form-group -->
						</div><!-- /.col-* -->	
					</div>	
					@endif		
					
			</fieldset>
			@if( $record->id )
			<fieldset>
				<legend>{{ __('listings__category.gallery') }}</legend>			
				<?
				$options = array(
					'url'=>url('attachments'),
					'geturl'=>url('attachments/get'),
					'removeurl'=>url('attachments/delete'),
					'module'=>'listings',
					'module_id'=>$record->id,
					'addeddata'=>['user_id'=>Auth::id(),'filedirectory'=>'listings'],
					'maxFiles'=>10,
				);			
				?>		
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-12">
							<div class="dropzone dz-clickable" data-plugin="dropzone" data-options="<?=clean_jsonString($options)?>">
								<div class="dz-message">
									<div class="fallback">
										<input name="gallery[]" type="file" multiple />
									</div>
									<h3 class="m-h-lg">{{ __('listings__category.drop_upload') }}</h3>
									<p class="m-b-lg text-muted">({{ __('listings__category.gallery') }})</p>
								</div>
							</div>	
							<label>image size at least 825x408</label>	
						</div>
					</div>
				</div>
			</fieldset>	
			@endif
			<div class="section-template" style="display:none;">
				<? section_templates($fa) ?>
			</div>
			<div class="sections">
				@if( $record->sections )	
					@foreach( $record->sections as $key => $section  )
						<?
						$section['key'] = $key;
						?>
						<fieldset class="sections" style="background-color: rgba(0, 0, 0, 0.10); position: relative">{{ section_templates($fa,$section) }}</fieldset>
					@endforeach	
				@endif	
			</div>

			<div class="center">
				<button class="btn btn-info btn-large btn-add-section"><i class="md-icon">add</i> {{ trans_choice('listings__category.get_action',1) }} {{ __('listings__category.section') }}</button>
				<button class="btn btn-primary btn-large">{{ __('listings__category.submit') }} {{ trans_choice('listings__category.listing',1) }}</button>
			</div><!-- /.center -->
			
		</form>
</div><!-- /.admin-box -->
@endsection

<?
function section_templates($fa,$params=null)
{
	$title = isset($params['title'][0]) ? $params['title'][0] : null;
	$title_name = isset($params['key']) ? 'name="sections['.$params['key'].'][title][0]"' : 'name="sections[0][title][0]"';
	echo '<h4>'.__('listings__category.title').': <input type="text" prop="title" value="'.$title.'" '.$title_name.' class="form-control">';
		echo '<button style="position: absolute; top: 0; right: 0" class="btn btn-danger btn-md btn-remove-section"><i class="md-icon m-0">clear</i></button>';
	echo '</h4>';
	if( $params && isset($params['content']) && count($params['content']) ){
		foreach($params['content'] as $key => $content){
			$data = array(
				'idx'=>$params['key'],
				'key'=>'sections',
				'content'=>$content,
				'icon'=>$params['icon'][$key],
				'price'=>$params['price'][$key],
			);
			echo '<div class="row section-row-template">'.row_templates($fa,$data).'</div>';
		}
	}else{
		if( $title ){
			$data = array(
				'idx'=>0,
				'key'=>'sections',
				'content'=>'',
				'price'=>'',
				'icon'=>'',
			);
		}else{
			$data = array();
		}
		echo row_templates($fa,$data);	
	}	
}
function row_templates($fa,$params=array())
{
	$icon['name'] = $params ? 'name="'.$params['key'].'['.$params['idx'].'][icon][]"' : null;
	$icon['value'] = $params ? $params['icon'] : null;
	$ico = str_replace('fas fa-','',$icon['value']);
	$ico = str_replace('fab fa-','',$ico);
	
	$content['name'] = $params ? 'name="'.$params['key'].'['.$params['idx'].'][content][]"' : null;
	$content['value'] = $params ? 'value="'.$params['content'].'"' : null;
	
	$price['name'] = $params ? 'name="'.$params['key'].'['.$params['idx'].'][price][]"' : null;
	$price['value'] = $params ? 'value="'.$params['price'].'"' : null;

	$data_idx = $params ? 'data-idx="'.$params['idx'].'"' : null;
	$fa_init = 'fa-init';
	$optionsFa = array(
		'manual_init'=>1,
		'selected'=>$icon['value'],
	);	
	$content ='
		<div class="row section-row-template">
			<div class="col-sm-4">
				<div class="form-group">
					<label>'.__('listings__category.content').'</label>
					<input type="text" prop="content" class="form-control" '.$content['name'].' '.$content['value'].'>
				</div><!-- /.form-group -->
			</div><!-- /.col-* -->		
			<div class="col-sm-2">
				<div class="form-group">
					<label>'.__('listings__category.price').'</label>
					<input type="number" prop="price" class="form-control" '.$price['name'].' '.$price['value'].'>
				</div><!-- /.form-group -->
			</div><!-- /.col-* -->				
			<div class="col-sm-4">
				<div class="form-group">
					<label>'.trans_choice('listings__category.icon',1).'</label>
	';	
	$content .='			
					<select class="form-control '.$fa_init.'" '.$icon['name'].' style="width:100%" prop="icon" data-options="'.clean_jsonString($optionsFa).'">	
						<option value="">'.__('listings__category.no').' '.trans_choice('listings__category.icon',1).'</option>		
	';
	$content .= $icon['value'] ? '<option value="'.$icon['value'].'" selected="selected">'.$ico.'</option>' : null;			
	$content .='					
					</select>
				</div><!-- /.form-group -->
			</div><!-- /.col-* -->

			<div class="col-sm-2">
				<div class="center">
					<button class="btn btn-primary btn-xs btn-add-row" obj="sections" '.$data_idx.'><i class="md-icon m-0">add</i></button>
					<button class="btn btn-danger btn-xs btn-remove-row"><i class="md-icon m-0">remove</i></button>
				</div><!-- /.form-group -->
			</div><!-- /.col-* -->							
		</div>	
	';
	return $content;		
}
?>