@extends('admin.layouts.admin')

@section('body_class','layout-empty')
@section('page_title',trans_choice('listings__category.listing',2))
@section('title',trans_choice('listings__category.listing',2))

@section('breadcrumb')
	<li><a href="{{ url('admin/listings') }}">{{ trans_choice('listings__category.listing',1) }}</a> <i class="md-icon">keyboard_arrow_right</i></li>
	<li class="active">{{ trans_choice('listings__category.listing',2) }}</li>
@endsection

@section('js')
	@parent
	<script type="text/javascript" src="{{ asset('assets/js/custom/admin/listing_list.js') }}"></script> 
@endsection

@section('content')
<?
$ajax_options = array(
	'ajax'=>array(
			'url'=>url('admin/listings/table'),
			),
	'responsive'=>'true',
	'serverSide'=>'true',
);
?>
<div class="admin-box">
	<table additional_search="<?=$additional_dttables_search?>"  id="responsive-datatable" data-unique="listings" data-plugin="DataTable" data-options="<?=clean_jsonString($ajax_options)?>" class="table small-header table-striped" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>{{ trans_choice('listings__category.listing',1)  }}</th>
				<th>{{ trans_choice('listings__category.website',1)  }}</th>
				<th>{{ trans_choice('listings__category.category',1) }}</th>
				<th>{{ __('listings__category.created_by') }}</th>
				<th>{{ trans_choice('listings__category.action',1) }}</th>
			</tr>
		</thead>
		
	</table>
</div><!-- /.admin-box -->
@endsection
