@extends('admin.layouts.admin')

@section('body_class','layout-empty')
@section('page_title',trans_choice('listings__category.category',2))
@section('title',trans_choice('listings__category.listing',1).' '.trans_choice('listings__category.category',2))

@section('breadcrumb')
	<li><a href="{{ url('admin/listings') }}">{{ trans_choice('listings__category.listing',1) }}</a> <i class="md-icon">keyboard_arrow_right</i></li>
	<li class="active">{{ trans_choice('listings__category.category',2) }}</li>
@endsection

@section('js')
	@parent
	<script type="text/javascript" src="{{ asset('assets/js/custom/admin/category_listing.js') }}"></script> 
@endsection

@section('content')
<?
$ajax_options = array(
	'ajax'=>array(
			'url'=>url('admin/listings/categories/table'),
			),
	'responsive'=>'true',
	'serverSide'=>'true',
);
?>
<div class="admin-box">
	<table additional_search="<?=$additional_dttables_search?>"  id="responsive-datatable" data-unique="category_listing" data-plugin="DataTable" data-options="<?=clean_jsonString($ajax_options)?>" class="table small-header table-striped" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>{{ trans_choice('listings__category.category',1) }}</th>
				<th>{{ __('listings__category.created_by') }}</th>
				<th>{{ trans_choice('listings__category.action',1) }}</th>
			</tr>
		</thead>
		
	</table>
</div><!-- /.admin-box -->

<div id="modal" class="modal fade" role="dialog" style="overflow:hidden;" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title">{{ __('listings__category.add_edit_category') }}</h4>
			</div>
			<div class="modal-body">
				<fieldset>
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<form id="ajax-form">
								<label>{{ trans_choice('listings__category.category',1) }}</label>
								<input type="text" name="category" class="form-control category" value="">			
								<input type="hidden" name="id" class="form-control id" value="">
								</form>
							</div><!-- /.form-group -->
						</div><!-- /.col-* -->

						<div class="col-sm-12">
							<div class="form-group">
								<form id="ajax-form">
								<label>{{ trans_choice('listings__category.icon',1) }}</label>
								<?
								$optionsFa = array(
									'manual_init'=>1,
								);	
								?>
								<select class="form-control fa-init icon" data-plugin="select2" name="icon" style="width:100%" data-options="<?=clean_jsonString($optionsFa) ?>">			
									@foreach ($fa as $ico => $icons)
										<option value="{{$ico}}" data-icon="{{$ico}}">{{$ico}}</option>
									@endforeach
								</select>
								</form>
							</div><!-- /.form-group -->
						</div><!-- /.col-* -->

					</div><!-- /.row -->
				</fieldset>
			</div><!-- .modal-body -->
			<div class="modal-footer">
				<button type="button" class="save btn btn-primary"> {{ __('listings__category.save') }} </button>
				<button type="button" class="btn btn-danger" data-dismiss="modal"> {{ __('listings__category.close') }} </button>

			</div><!-- .modal-footer -->

		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>
@endsection
