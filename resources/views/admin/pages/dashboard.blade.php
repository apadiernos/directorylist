@extends('admin.layouts.admin')

@section('title','Admin Dashboard')
@section('page_title','Admin Dashboard')

@section('breadcrumb')
	<li><a href="index.html">Admin</a> <i class="md-icon">keyboard_arrow_right</i></li>
	<li class="active">Dashboard</li>
@endsection

@section('content')
<div class="admin-stats">
	<div class="row">
		<div class="col-xs-12 col-lg-6 col-xl-6">
			<div class="admin-stat lime">
				<div class="admin-stat-icon">
					<i class="md-icon">people</i>
				</div><!-- /.admin-stat-icon -->

				<div class="admin-stat-content">
					<h2>{{ trans_choice('listings__category.category',2)  }}</h2>
					<h3>{{ $categories }}</h3>
				</div><!-- /.admin-stat-content -->
			</div><!-- /.admin-stat -->
		</div><!-- /.col-* -->

		<div class="col-xs-12 col-lg-6 col-xl-6">
			<div class="admin-stat blue">
				<div class="admin-stat-icon">
					<i class="md-icon">search</i>
				</div><!-- /.admin-stat-icon -->

				<div class="admin-stat-content">
					<h2>{{ trans_choice('listings__category.listing',2)  }}</h2>
					<h3>{{ $listings }}</h3>
				</div><!-- /.admin-stat-content -->
			</div><!-- /.admin-stat -->
		</div><!-- /.col-* -->
	</div><!-- /.row -->
</div><!-- /.admin-stats -->

<h1 class="text-center">Work In Progress</h1>
<?
$ajax_options = array(
	'ajax'=>array(
			'url'=>url('admin/listings/table'),
			),
	'responsive'=>'true',
	'serverSide'=>'true',
);
?>
<div class="admin-box">
	<table additional_search="<?=$additional_dttables_search?>"  id="responsive-datatable" data-unique="listings" data-plugin="DataTable" data-options="<?=clean_jsonString($ajax_options)?>" class="table small-header table-striped" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>{{ trans_choice('listings__category.listing',1)  }}</th>
				<th>{{ trans_choice('listings__category.website',1)  }}</th>
				<th>{{ trans_choice('listings__category.category',1) }}</th>
				<th>{{ __('listings__category.created_by') }}</th>
				<th>{{ trans_choice('listings__category.action',1) }}</th>
			</tr>
		</thead>
		
	</table>
</div><!-- /.admin-box -->

@endsection
