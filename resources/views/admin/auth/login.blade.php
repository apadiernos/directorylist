@extends('admin.layouts.guest')

@section('body_class','layout-empty')
@section('title','Admin Login')

@section('content')
<div class="container login-form">
	<div class="row">
		<div class="col-lg-5 col-lg-offset-4">
			<h2>Log In Into Account</h2>

			<form method="post" action="/login/admin">
				@csrf
				<div class="form-group">
					<label for="">Email</label>
					<input type="email" name="email" class="form-control">
				</div><!-- /.form-group -->

				<div class="form-group">
					<label for="">Password</label>
					<input type="password" name="password" class="form-control">
				</div><!-- /.form-group -->

				<div class="form-group-btn">
					<button type="submit" class="btn btn-primary pull-right">Sign In</button>
				</div><!-- /.form-group-btn -->
				<!--
				<div class="form-group-bottom-link">
					<a href="reset-password.html">I forgot my password <i class="fa fa-long-arrow-right"></i></a>
				</div><!-- /.form-group-bottom-link -->
				
			</form>
		</div><!-- /.col-* -->
	</div><!-- /.row -->
</div><!-- /.container -->
@endsection
