@extends('portal.layouts.portal')

@section('title','404 | Not Found')
@section('page_title','404 | Not Found')

@section('content')	
	<div class="row">
		<div class="warning">
			<h1>System Error</h1>
			<p>
				Please return <a href="{{ url('/') }}"> Home</a>
				or email us at <a href="mailto:alan.ontue@gmail.com">alan.ontue@gmail.com</a> if you are seeing this error.
			</p>
			<hr>
			
			<a href="{{ url('/') }}" class="btn btn-secondary btn-large"><i class="fa fas fa-car"></i> Return Home</a>
		</div><!-- /.warning -->		
	</div><!-- /.row -->
@endsection	