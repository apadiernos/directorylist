@extends('portal.layouts.portal')

@section('title','405 | Not Allowed')
@section('page_title','405 | Not Allowed')

@section('content')	
	<div class="row">
		<div class="warning">
			<h1>405</h1>
			<p>Not Allowed to Access Page!</p>
			<hr>
			
			<a href="{{ url('/') }}" class="btn btn-secondary btn-large"><i class="fa fas fa-car"></i> Return Home</a>
		</div><!-- /.warning -->		
	</div><!-- /.row -->
@endsection	