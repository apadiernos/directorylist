@extends('portal.layouts.portal')

@section('title','404 | Not Found')
@section('page_title','404 | Not Found')

@section('content')	
	<div class="row">
		<div class="warning">
			<h1>419</h1>
			<p>Page Expired</p>
			<hr>
			
			<a href="{{ url('/') }}" class="btn btn-secondary btn-large"><i class="fa fas fa-car"></i> Return Home</a>
		</div><!-- /.warning -->		
	</div><!-- /.row -->
@endsection	