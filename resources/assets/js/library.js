var base_url = $('body').attr("base_url");
var csrf = $('body').attr("csrf");
$.extend({
	xResponse: function(url, data, type) {
		// local var
		var theResponse = null;
		// jQuery ajax
		$.ajax({
			url: url,
			type: type ? type : 'POST',
			data: data,
			dataType: "html",
			async: false,
			beforeSend: function (xhr, settings) {
				//////////// Only for your domain
				//if (settings.url.indexOf(document.domain) >= 0) {
					xhr.setRequestHeader("X-CSRF-Token", csrf);
				//}
			},			
			success: function(respText) {
				theResponse = respText;
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 	
				//showErrorMessage(global_error_notif);
				console.log(XMLHttpRequest);
				console.log(textStatus);
				console.log(errorThrown);
				console.log('Error in form_validations.js');					
			}
		});
		// Return the response text
		return theResponse;
	}
});	
$.fn.waitingBlock = function(messageBlock=null){
	if( !messageBlock ) messageBlock = 'Processing. Please Wait..';
	
	var msgContainer = '<div class="group">'
		  +'<div class="bigSqr">'
			+'<div class="square first"></div>'
			+'<div class="square second"></div>'
			+'<div class="square third"></div>'
			+'<div class="square fourth"></div>'
		  +'</div>'
		  +'<div class="text">'+messageBlock+'</div>'
		+'</div>';
	$(this).css({'height':'100%','width':'100%'});
	$(this).block({ 
		message: msgContainer,
		css: {
			border: 'none',
			padding: '2px',
			backgroundColor: 'none',
		},
		overlayCSS: {
			backgroundColor: '#fff',
			opacity: 0.3,
			cursor: 'wait'
		},
		centerX: true, // <-- only effects element blocking (page block controlled via css above) 
		centerY: true,		
	});	
	$('.blockUI.blockMsg',this).css({'top':'30%','left':'30%'}); 	
};	

$.fn.scrollableBlock = function(){
	$('.scrollInner',this).slimScroll({
		height: 'auto',
		position: 'right',
		size: "5px",
		color: '#98a6ad',
		wheelStep: 10,
		touchScrollStep: 50
	});	
	$scrollContainer = $(this);
	if( $scrollContainer.height() < 400 ){
		$scrollContainer.css('overflow', 'inherit').parent().css('overflow', 'inherit');
		$scrollContainer.siblings('.slimScrollBar').css('visibility', 'hidden');
	} else{
		$scrollContainer.css('overflow', 'hidden').parent().css('overflow', 'hidden');
		$scrollContainer.siblings('.slimScrollBar').css('visibility', 'visible');
	}
};	

$.fn.calculateWeight = function(grade){
	var passing_weight = $(this).attr('passing_weight');
	try{
		var arrayStr = passing_weight.split(',');
		for (x = 0; x < arrayStr.length; x++) {
			try{
				if( isInt(arrayStr[x]) && parseInt(arrayStr[x]) == parseInt(grade) ){
					return 1;
				}
				else if( arrayStr[x].search("-") ){
					var array = arrayStr[x].split('-');
					var comp = 0;
					var passed = [];
					for (index = 0; index < array.length; index++) {
						var iden = index+1;
						if(iden < array.length ){
							comp = parseInt(array[iden]);
							if( comp > array[index] ){
								for(y = comp; y >= array[index]; y-- ){					
									if( parseInt(grade) == y )
										return 1;
								}
							}
						}	


					}
				}	
			}catch(err){
				console.log('error dash');
			}
		}	
	}catch(err){
		console.log(err);
		console.log('error comma');
	}

	return 0;
};
function animate_progress(progress){	
	$({ Counter: 0 }).animate({ Counter: 100 }, {
		duration: 4000,
		easing: 'swing',
		step: function () {
		  progress.text(Math.ceil(this.Counter)+'%');
		  progress.css({ 'width': Math.ceil(this.Counter)+'%' });
		}
	});	
};
function isInt(value) {
  var x;
  //var value = this.value;
  return isNaN(value) ? !1 : (x = parseFloat(value), (0 | x) === x);
};

function refresh_captcha()
{
	var captcha = $('img.captcha-img');	
	var config = captcha.data('refresh-config');
	$.ajax({
		method: 'GET',
		url: '/get_captcha/' + config,
	}).done(function (response) {
		captcha.prop('src', response);
	});		
}

function show_error_modal(){
	$('#error-modal').modal('show');
}
var LIBS = {
	// Chart libraries
	plot: [
		base_url+"/assets/js/libs/misc/flot/jquery.flot.min.js",
		base_url+"/assets/js/libs/misc/flot/jquery.flot.pie.min.js",
		base_url+"/assets/js/libs/misc/flot/jquery.flot.stack.min.js",
		base_url+"/assets/js/libs/misc/flot/jquery.flot.resize.min.js",
		base_url+"/assets/js/libs/misc/flot/jquery.flot.curvedLines.js",
		base_url+"/assets/js/libs/misc/flot/jquery.flot.tooltip.min.js",
		base_url+"/assets/js/libs/misc/flot/jquery.flot.categories.min.js"
	],
	chart: [
		base_url+"/assets/js/libs/misc/echarts/build/dist/echarts-all.js",
		base_url+"/assets/js/libs/misc/echarts/build/dist/theme.js",
		base_url+"/assets/js/libs/misc/echarts/build/dist/jquery.echarts.js",
		//base_url+"/assets/js/custom/echarts.js",
	],
	circleProgress: [
		base_url+"/assets/js/libs/bower/jquery-circle-progress/dist/circle-progress.js"
	],
	sparkline: [
		base_url+"/assets/js/libs/misc/jquery.sparkline.min.js"
	],
	leaflet: [
		base_url+"/assets/js/libs/misc/leafletjs/leaflet.js",
		base_url+"/assets/js/libs/misc/leafletjs/leaflet.markercluster.js",
		base_url+"/assets/js/libs/misc/leafletjs/leaflet.css",
		base_url+"/assets/js/libs/misc/leafletjs/MarkerCluster.css",
		base_url+"/assets/js/libs/misc/leafletjs/MarkerCluster.Default.css",
		base_url+"/assets/js/libs/misc/leafletjs/control-search/leaflet-search.min.css",
		base_url+"/assets/js/libs/misc/leafletjs/control-search/leaflet-search.mobile.min.css",
		base_url+"/assets/js/libs/misc/leafletjs/control-search/leaflet-search.src.js",
		base_url+"/assets/js/custom/leaflet.js",
	],	
	maxlength: [
		base_url+"/assets/js/libs/bower/bootstrap-maxlength/src/bootstrap-maxlength.js"
	],
	tagsinput: [
		base_url+"/assets/js/libs/bower/bootstrap-tagsinput/dist/bootstrap-tagsinput.css",
		base_url+"/assets/js/libs/bower/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js",
	],
	rating: [
		base_url+"/assets/js/libs/bower/bootstrap-star-rating/css/star-rating.min.css",
		base_url+"/assets/js/libs/bower/bootstrap-star-rating/js/star-rating.min.js",
	],
	facebookReactions: [
		base_url+"/assets/js/libs/misc/fbreactions/stylesheet.css",
		base_url+"/assets/js/libs/misc/fbreactions/jquery.easing.1.3.min.js",
		base_url+"/assets/js/libs/misc/fbreactions/facebook-reactions.js",	
	],
	emojioneArea: [
		base_url+"/assets/js/libs/misc/emojione/emojionearea.min.css",
		base_url+"/assets/js/libs/misc/emojione/emojionearea.min.js",	
	],		
	TouchSpin: [
		base_url+"/assets/js/libs/bower/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css",
		base_url+"/assets/js/libs/bower/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"
	],
	selectpicker: [
		base_url+"/assets/js/libs/bower/bootstrap-select/dist/css/bootstrap-select.min.css",
		base_url+"/assets/js/libs/bower/bootstrap-select/dist/js/bootstrap-select.min.js"
	],
	filestyle: [
		base_url+"/assets/js/libs/bower/bootstrap-filestyle/src/bootstrap-filestyle.min.js"
	],
	timepicker: [
		base_url+"/assets/js/libs/bower/bootstrap-timepicker/assets/js/bootstrap-timepicker.js"
	],
	datetimepicker: [
		base_url+"/assets/js/libs/bower/moment/moment.js",
		base_url+"/assets/js/libs/bower/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css",
		base_url+"/assets/js/libs/bower/eonasdan-bootstrap-datetimepicker/build/assets/js/bootstrap-datetimepicker.min.js"
	],
	select2: [
		base_url+"/assets/js/libs/bower/select2/dist/css/select2.min.css",
		base_url+"/assets/js/libs/bower/select2/dist/js/select2.full.min.js",
		base_url+"/assets/js/custom/select2.js",
	],
	vectorMap: [
		base_url+"/assets/js/libs/misc/jvectormap/jquery-jvectormap.css",
		base_url+"/assets/js/libs/misc/jvectormap/jquery-jvectormap.min.js",
		base_url+"/assets/js/libs/misc/jvectormap/maps/jquery-jvectormap-us-mill.js",
		base_url+"/assets/js/libs/misc/jvectormap/maps/jquery-jvectormap-world-mill.js",
		base_url+"/assets/js/libs/misc/jvectormap/maps/jquery-jvectormap-africa-mill.js",
		base_url+"/assets/js/custom/vectorMap.js",
	],
	summernote: [
		base_url+"/assets/js/libs/bower/summernote/dist/summernote.css",
		base_url+"/assets/js/libs/bower/summernote/dist/summernote.min.js"
	],
	DataTable: [
		base_url+"/assets/js/libs/misc/datatables/datatables.min.css",
		base_url+"/assets/js/libs/misc/datatables/datatables.min.js",
		base_url+"/assets/js/custom/datatables.js",
	],
	fullCalendar: [
		base_url+"/assets/js/libs/bower/moment/moment.js",
		base_url+"/assets/js/libs/bower/fullcalendar/dist/fullcalendar.min.css",
		base_url+"/assets/js/libs/bower/fullcalendar/dist/fullcalendar.min.js"
	],
	dropzone: [
		base_url+"/assets/js/libs/bower/dropzone/dist/min/dropzone.min.css",
		base_url+"/assets/js/libs/bower/dropzone/dist/min/dropzone.min.js",
		base_url+"/assets/js/custom/dropzone.js",
	],
	counterUp: [
		base_url+"/assets/js/libs/bower/waypoints/lib/jquery.waypoints.min.js",
		base_url+"/assets/js/libs/bower/counterup/jquery.counterup.min.js"
	],
	counterUp: [
		base_url+"/assets/js/libs/bower/waypoints/lib/jquery.waypoints.min.js",
		base_url+"/assets/js/libs/bower/counterup/jquery.counterup.min.js"
	],	
	owlCarousel: [
		base_url+"/assets/js/libs/misc/owl-carousel/owl.carousel.css",
		base_url+"/assets/js/libs/misc/owl-carousel/owl.theme.default.min.css",
		base_url+"/assets/js/libs/misc/owl-carousel/owl.carousel.min.js",
	],
	imagesGrid: [
		base_url+"/assets/js/libs/misc/imagesgrid/images-grid.css",
		base_url+"/assets/js/libs/misc/imagesgrid/images-grid.js",
	],	
	validate: [
		base_url+"/assets/js/libs/misc/jquery-validation/js/jquery.validate.min.js",
		base_url+"/assets/js/custom/form_validation.js",
	]
};
