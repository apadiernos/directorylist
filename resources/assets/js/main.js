$(document).ready(function() {	
	$('i.refresh-captcha').click(function(){
		refresh_captcha();
	});

	$('div#register-modal,div#login-modal').on('hidden.bs.modal', function () {
		$('div.popover-validate-error').remove();
	})
	
	
	$('button#user-register').click(function(){
		var url =  base_url+'/register';
		var form = $(this).parents('div#register-modal').find('form');
		var request = form.serialize();
		if( !form.valid() )
			return false;
			
		$.ajax({
			method: 'POST',
			data: request,
			url: url,
			beforeSend: function (xhr, settings) {
				xhr.setRequestHeader("X-CSRF-Token", csrf);
			},	
			error: function(XMLHttpRequest, textStatus, errorThrown) { 	
				try{
					var res = $.parseJSON(XMLHttpRequest.responseText);				
					var validator = form.validate();
					var errors = {};
					$.each(res.errors,function(key,val){
						errors[key] = val[0];			
					});
					if( Object.keys(errors).length ){
						refresh_captcha();
						validator.showErrors(errors);
						return;	
					}
				}catch(err){
					
				}
			}		
		}).done(function (response) {
			$('div#register-modal').modal('hide');
			window.location.reload();
		});	
	});
	$('button#user-login').click(function(){
		var url =  base_url+'/login';
		var form = $(this).parents('div#login-modal').find('form');
		var request = form.serialize()+'&is_ajax=1';
		if( !form.valid() )
			return false;
			
		$.ajax({
			method: 'POST',
			data: request,
			url: url,
			beforeSend: function (xhr, settings) {
				xhr.setRequestHeader("X-CSRF-Token", csrf);
			},	
			error: function(XMLHttpRequest, textStatus, errorThrown) { 	
				try{
					var res = $.parseJSON(XMLHttpRequest.responseText);				
					var validator = form.validate();
					var errors = {};
					$.each(res.errors,function(key,val){
						errors[key] = val[0];			
					});
					if( Object.keys(errors).length ){
						refresh_captcha();
						validator.showErrors(errors);
						return;	
					}
				}catch(err){
					
				}
			}		
		}).done(function (response) {
			if( parseInt(response) == 1 ){
				$('div#login-modal').modal('hide');
				window.location.reload();
			}else
				$('div.alert-danger-login').show().html(response);
		});	
	});	
});	
