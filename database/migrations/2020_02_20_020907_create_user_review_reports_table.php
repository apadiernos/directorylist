<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserReviewReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_review_reports', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->bigInteger('user_id')->nullable(false)->default(0)->index();
			$table->bigInteger('listing_id')->nullable(false)->default(0)->index();
			$table->bigInteger('review_id')->nullable(false)->default(0)->index();
			$table->text('message');			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_review_reports');
    }
}
