<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listings', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->string('title',128)->nullable(false);
			$table->string('email');
			$table->string('website')->nullable(true);
			$table->longText('description')->nullable(true);
			$table->text('sections')->nullable(true);
			$table->text('socialmedia')->nullable(true);
			$table->string('country',3)->nullable(true)->index();
			$table->string('state',6)->nullable(true)->index();
			$table->string('address',256)->nullable(true);
			$table->decimal('long', 11, 8)->nullable(true)->default(0);
			$table->decimal('lat', 10, 8)->nullable(true)->default(0);		
			$table->bigInteger('created_by')->nullable(false)->default(0)->index();
			$table->bigInteger('updated_by')->nullable(false)->default(0)->index();
            $table->timestamps();

            $table->index(['long', 'lat']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listings');
    }
}
