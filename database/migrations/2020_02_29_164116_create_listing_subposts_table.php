<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListingSubpostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listing_subposts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->nullable(false)->default(0)->index();
            $table->bigInteger('listing_id')->nullable(false)->default(0)->index();
            $table->bigInteger('listing_post_id')->nullable(false)->default(0)->index();
            $table->longText('description')->nullable(true);
            $table->string('title')->nullable(false)->index(); 
            $table->string('unique_id')->nullable(false);   
            $table->string('unique_post_id')->nullable(false);             
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listing_subposts');
    }
}
