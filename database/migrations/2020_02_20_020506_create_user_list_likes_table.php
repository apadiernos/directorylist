<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserListLikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_list_likes', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->bigInteger('user_id')->nullable(false)->default(0)->index();
			$table->bigInteger('listing_id')->nullable(false)->default(0)->index();
			$table->bigInteger('created_by')->nullable(true)->default(0)->index();
			$table->bigInteger('updated_by')->nullable(true)->default(0)->index();				
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_list_likes');
    }
}
