<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminusersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adminusers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name',64)->nullable();
            $table->string('last_name',128)->nullable();
            $table->string('middle_name',16)->nullable();
            $table->boolean('gender')->default(true);
            $table->string('username')->unique()->nullable(false);
            $table->string('email')->unique();
            $table->string('password');
			$table->bigInteger('created_by')->nullable(false)->default(0);
			$table->bigInteger('updated_by')->nullable(false)->default(0);			
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adminusers');
    }
}
