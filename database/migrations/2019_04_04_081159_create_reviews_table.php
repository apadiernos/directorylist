<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->string('email')->nullable(true)->index();
			$table->string('name')->nullable(true);
            $table->string('module')->nullable(false)->index();
			$table->text('message');
			$table->bigInteger('rating');
			$table->bigInteger('parent_id')->nullable(false)->default(0)->index();
			$table->bigInteger('listing_id')->nullable(false)->default(0)->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
