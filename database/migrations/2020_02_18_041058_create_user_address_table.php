<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_addresses', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->bigInteger('user_id')->nullable(true)->default(0)->index();
			$table->string('country',3)->nullable(true)->index();
			$table->string('state',6)->nullable(true)->index();
			$table->string('address',256)->nullable(true);
			$table->decimal('long', 11, 8)->nullable(true);
			$table->decimal('lat', 10, 8)->nullable(true);
			$table->string('region',16)->nullable(true)->index();
            $table->string('region_code',6)->nullable(true)->index();  
            $table->string('city')->nullable(true)->index();
			$table->string('brgy')->nullable(true)->index();			
			$table->bigInteger('created_by')->nullable(true)->default(0)->index();
			$table->bigInteger('updated_by')->nullable(true)->default(0)->index();			
            $table->timestamps();

            $table->index(['long','lat']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_addresses');
    }
}
