<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListingTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listing_tags', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->nullable(false)->default(0)->index();
            $table->bigInteger('listing_id')->nullable(false)->default(0)->index();
            $table->bigInteger('listing_post_id')->nullable(false)->default(0)->index();   
            $table->bigInteger('listing_subpost_id')->nullable(false)->default(0)->index();           
            $table->bigInteger('tag_id')->nullable(false)->default(0)->index();           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listing_tags');
    }
}
