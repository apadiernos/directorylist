<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attachments', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->string('file_name')->nullable(false);
			$table->string('file_type')->nullable(false);
			$table->decimal('file_size',18,2)->nullable(false)->default(0);
			$table->string('file_ext',6)->nullable(false);
			$table->string('file_raw_name')->nullable(false);
			$table->string('module')->nullable(false)->index();
			$table->string('module_id')->nullable(false)->index();
			$table->bigInteger('created_by')->nullable(false)->default(0)->index();
			$table->bigInteger('updated_by')->nullable(false)->default(0)->index();
            $table->timestamps();

            $table->index(['module','module_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attachments');
    }
}
