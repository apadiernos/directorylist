<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateListingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('listings', function (Blueprint $table) {
			$table->string('region',16)->after('state')->nullable(true);
            $table->string('region_code',6)->after('region')->nullable(true);            
			$table->bigInteger('category_id')->nullable(false)->after('id')->default(0)->index();
			$table->bigInteger('ratings')->nullable(true)->after('id')->default(0)->index();
			$table->string('slug_term')->nullable(true)->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('listings', function (Blueprint $table) {
            $table->dropColumn('website');
            $table->dropColumn('region');
            $table->dropColumn('region_code');
            $table->dropColumn('category_id');
            $table->dropColumn('ratings');
            $table->dropColumn('slug_term');
        });    
    }
}
