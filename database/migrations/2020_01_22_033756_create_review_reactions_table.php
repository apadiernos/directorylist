<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewReactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('review_reactions', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->bigInteger('review_id')->nullable(false)->default(0)->index();
			$table->bigInteger('listing_id')->nullable(false)->default(0)->index();
			$table->bigInteger('timecreated')->nullable(false)->default(0);
			$table->ipAddress('visitor');
            $table->string('reaction',64)->nullable(false)->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('review_reactions', function (Blueprint $table) {
            Schema::dropIfExists('review_reactions');
        });
    }
}
